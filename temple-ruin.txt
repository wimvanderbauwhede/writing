The Temple Ruin

On a planet called Earth, orbiting an unremarkable main-series star called the Sun, there is a temple ruin. There are in fact many temples and temple ruins on that planet, the inhabitants having frequently felt the need to pray and create organised religions.

This particular ruin is located in the barren Northern wastes of a large island off the West coast of the largest continent. (The Northen region is not really barren nor a wasteland, but it might as well be so from the perspective of the largest ethno-political group on the island, especially those living in the capital, a thousand kilometers to the South of what was after all only a low-yield colony.)

Long, long ago, the leaders of a minority faith in that North-Western region -- which was one of the largest religions globally, but not on the island, where the dominant group had instituted a state religion -- had come to believe, against all evidence to the contrary, that their faith would spread across the entire North. As this imminent expansion would require many priests, they conceived of a temple that would hold a hundred novices. And their will was carried out, and the new temple was magnificent. 

Unfortunately, very shortly after its completion, the global leadership of the religion decreed that priests would henceforth be embedded in the community; and the growth in adherents in the North never materialised, on the contrary, numbers kept on dwindling. 
The temple therefore never operated at full capacity, and worse, became anathema almost as soon as it opened; and thus a decade later it was abandoned, and left to fall into gentle delapidation. Not that its demise was overly mourned, least of all by the ex-novices: although it was generally acclaimed as an architectural masterpiece, from the start, it had been a cold, wet, and drafty place, and prone to mould.

Another decade later, a politician of the state that ruled over the Northern colony decided, for reasons lost in time, that the temple, at that point not quite yet a ruin, should get protected status, presumably as a monument to changing fortunes. Which meant that it could no longer be demolished, but the faith was very reluctant to put further funds into the upkeep of what had turned out to be an almost bottomless pit. And so the building was simply left to crumble, until only the bare bones were left. It is now largely forgotten, lost in the dense vegetation of the surrounding forest.

A curious custom has arisen amongst local young adults: they will climb the structure, aided by climbing ropes and harnesses, to spray messages on the remaining stonework. These messages have nothing at all to do with the original purpose or nature of the building, but because the activity is not without its risks, it is thought that there must be a spiritual motivation behind this activity.

From the capital, the ruin can be reach by overland transport in a day. A local guide needs to be contracted for exploration.  


