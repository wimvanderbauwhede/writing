The K computer in Kobe was for a long time the world's most powerful supercomputer. In Japanese the “K” is the same kanji as for Kyoto, it also means 10**16, the speed of its computations. But what I really like about it is that it has its own railway station.

