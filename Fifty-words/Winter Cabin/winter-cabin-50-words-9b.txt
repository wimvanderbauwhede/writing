All around me there was nothing but dry reeds rustling in the East wind. 
If I hadn't done such a good job with the insulation of the cabin, that wind would have blown right through it. But I had, and it was quite a cosy place to wait out winter.

