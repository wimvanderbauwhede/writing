#!/usr/bin/perl
use warnings;
use strict;
use v5.20;

open my $IN, '<', $ARGV[0] or die $!;


my @lines = ();
while (my $line = <$IN> ) {
    push @lines, $line;
}
close $IN;

my @preproc_lines = ();
my $summary=0;
my $ch_heading=0;
my $skip=0;
for my $line (@lines) {
    if ($line=~/\#\#/) { $ch_heading=1;}
    if ($line=~/\>/ and $ch_heading==1) {
        $ch_heading=0;
        chomp $line;
        # Weak, allows only one occurrence per lin
        if ($line=~/\`(\w+)\`/) {
            my $name=$1;
            $line =~s/\`\w+\`/<span class="code-summary">$name<\/span>/; 
        }
        $line = '<p class="summary"' . $line . '</p>'."\n";
    } 
    # if ($line=~/\s+\@\w+:/) {$line.= "\n"}
    if ($line=~/\s+\@\w+:/) {
        chomp $line;
        $line =~s/^\s+//;
        $line = '> '.$line.'<br';
        }


    push @preproc_lines, $line;
}

open my $OUT, '>', 'tmp.md' or die $!;
for my $line (@preproc_lines) {
    print $OUT $line;
}
close $OUT;

system("cp ~/Other/Writing/Writing-course/tmp.md ~/Git/selfedge-devel/_posts/fiction/2023-12-20-standing-wave.md");
