# Notebook "Standing Wave"

## Story idea in a nutshell 

>    Minsk
>
>    The holy grail was scaling climate models to resolve clouds. She found the solution in an old FORTRAN CFD code for the Soviet Minsk mainframe. Autotranslated to GPU, the ancient core worked its magic.
>
>
>    A still-unresolved issue with climate modeling [1] is that the required resolution to accurately model clouds with current techniques is so high that it requires too much compute power. I speculate here that a researcher found a piece of code that could do this in a program originally written in the 1960s for the Minsk-32 mainframe [2], a Soviet computer which supported the FORTRAN programming language (developed in the 1950s by an American, John Backus). 
>    As part of my research, I have developed a compiler [3] which can take such old FORTRAN code and convert it into code for modern Graphics Processing Units, which are extremely fast for this type of computations.
>
>    [1] https://eos.org/research-spotlights/a-super-solution-for-modeling-clouds
>    [2] https://computer-museum.ru/english/minsk_32.php
>    [3] https://www.sciencedirect.com/science/article/pii/S0045793018302950

In more detail, this would involve a link with a trip of a US climate scientist to Novosibirsk, and the idea is that another scientist, who was a programming wizard, put a magic kernel in the code for climate simulation on the BESM-6.

I have this idea that this was done remote via the kind of teletype created by Robotron, e.g. T51 but more likely T163 which supported Cyrillic as well as Latin, like the BESM-6 punch cards.
https://www.robotrontechnik.de/index.htm?/html/sonstiges/fernschreiber.htm
Could this be over Akademset? It originated 1978, but it is plausible that there was a precursor.
Or maybe even RTTY? http://ftp.funet.fi/pub/dx/text/utility/Soviet.rtty
In reality, the BESM-6 had a Consul-254 teletype, Czechoslovakian, but that is where we have fiction.

A good candidate is Valentin Pavlovich Dymnikov (he was born in the [Mari ASSR](https://en.wikipedia.org/wiki/Mari_Autonomous_Soviet_Socialist_Republic) so he was a [Mari](https://en.wikipedia.org/wiki/Mari_people))

But unfortunately for my teletype idea, he did work in Novosibirsk. He did really work on a global ocean and atmospheric circulation model with [Marchuk](https://mathshistory.st-andrews.ac.uk/Biographies/Marchuk/). Maybe I can speculate that he went on a sabbatical.
He would have been likely to be e.g. at the [USSR Institute of High-Energy Physics](https://en.wikipedia.org/wiki/Institute_for_High_Energy_Physics) in Protvino near Moscow, because they made [a "translator" for Fortran to the Minsk-22]( https://www.sciencedirect.com/science/article/abs/pii/0041555370901552); or the Joint Inst. for Nuclear Research at Dubna (also near Moscow), who did ["Automatization of physical experiments on-line with the MINSK-32 computer" ](https://inis.iaea.org/search/search.aspx?orig_q=RN:10492348). 
The first FORTRAN compiler for the Soviet BESM-6 mainframe was written by means of manually retargeting the assembly code of the CDC 1604 FORTRAN compiler by programmers at JINR.

In any case, they idea is that all that remains is a roll of tape encoded in Fernschreib-Alphabet 2 for the T163, which has somehow ended up at the The National Museum of Computing (Bletchley Park). And how it got there is because the stuff from Novosibirsk was moved to Moscow when the USSR fell apart, and from there bought by TNoC. And forgotten, I want to say something like:

"And yet there lie in his hoards many records that few now can read, even of the lore-masters, for their scripts and tongues have become dark to later men. And Boromir, there lies in Minas Tirith still, unread, I guess, by any save Saruman and myself since the kings failed, a scroll that Isildur made himself. For Isildur did not march away straight from the war in Mordor, as some have told the tale."

Maybe, refering to the archivist:

"And yet there lie in their archives many records that few now can read, even of the experts, for their formats and encodings have become incompatible to later systems. And, kagetsuko, there lies in Bletchley Park still, undeciphered, I guess, by any save myself since the USSR failed, a roll of tape that Valentin Dymnikov punched himself."ge


The story is told by a first-person narrator in 2030 but deals with events from 2027.

## Character notes

### Protagonist

* kagetsuko
    * @kagetsuko カゲツコ real name 涼宮夏子 SUZUMIYA Natsuko 
    * female, Japanese
    * aged say 32 at the time of the events, 35 at the time of narration
    * Post-doc at Kyoto University DPRI but working full time remote at ECMWF
    * I think she should have short hair, even shaved sides, and look like Yoko Tani
* from aggretsuko but also ka-getsu-ko, full moon child; and kage　tsuukou, 影通航, shadow sailing
* fedi
	* Main account on absturzmö.ve, ("Absturzmöve" meaning "crash gull" in German); also runs her own server, so has to be geeky; and it's misskey, so a pointing towards Japan.
	* Avatar a minor character from the Yuyushiki ( ゆゆ式) manga
	* Bio says science/bread/chocolate/coffee; languages: Japanese (native), English (fluent)
	* Main oddity is keeping European hours
* Maybe her groundbreaking work will be known as the Suzumiya-Dymnikov method

I think I need her partner, Chiharu, 36 in 2027 and  Chihar's mom, 66 in 2027, so 69 

### Protagonist's aunt

* Mieko 
    * 大島美恵子　OOSHIMA Mieko, the sister-in-law of Natsuko's mother
    * female, father Japanese, mother Singapore Chinese (hafu)
    * aged 79 in 2027 so 24 in 1972, 27 in 1975
    * lives in Hawaii with her husband (小松茂 KOMATSU Shigeru) , who used to be an avid surfer. Maybe her husband died recently (or else he has dementia?).
    * when younger, looked like Rei Kawakubo
    * her parents died in 1960 in the tsunami caused by the Chile earthquake
    
* Shigeru
	* 小松茂 KOMATSU Shigeru
	* male, Japanese
	* 26 in 1972, 29 in  1975, died in 2023? Or more like 2003?
	* worked for Sumitomo Realty in Hawaii, then opened a bicycle shop (after 4 years)
	* avid surfer
	* looks like the protagonist in "A scene by the sea"
	* inspired by a surfer from Hokkaido he knew at university
	* his mother also has relatives who died in the tsunami	    
	* his parents live in Kamakura
	

### Scientist at the Siberian SuperComputer Center of SB RAS in 2027

* klimagalka
    * Venera Galina Вене́ра
    * female, Russian but maybe ethnically Uzbek or Kyrghyz, it is plausible
    * age unknown
    * @klimagalka Галина Венера
    * works as a climate researcher at the Siberian Branch of the Russian Academy of Sciences in Novosibirsk, but on a different topic from @kagetsuko
    * I think she can look like Anastasia Pak, but without mentioning the name
    
### Librarian at Hawaii University in 2027

* silverstacks
    * Anonymous, they/them. Has an online indie second-hand bookshop.
    * @silverstacks SilverStacksBooks 
    * looks like a more androgynous version of Dr. Isabella Aiona Abbott
### Narrator: anonymous

* Scandinavian (Danish, Swedish or Icelandic)
* probably male but female would work just as well
* aged 74 at the time of the narration, 2030
* a retired academic, computing scientist, maybe at Essex
* lived in the UK at the time of the events
* lives in Kyoto at the time of narration

### NOAA Scientist visiting the Computer Center in 1975

* Dr. Nathan Lightman
    * male, about thirty
    * I imagine him to look a bit like John Garfield
    * A climate scientist working for NOAA but at University of Hawaii
    * I have a whole character description and back story for him but the only point other relevant to the plot is that he is gay.

### Scientists at the Computing Center in 1975

Computing Center, Siberian Department, Academy of Sciences of the USSR

The following are historic characters. I attribute the key innovation to Dymnikov, and for that invent some part of his life.

* Dr. Anatolii Semenovich Alekseev, Deputy Director, 
    - he looks like Karl Malden. 
    - Born 1928, so 47 in 1975
* Dr. Gury I. Marchuk, first Director of the center and expert in computational science. 
    - In 1974 he initiated work on modelling the global ocean
and atmospheric circulation at the Computing Center of the Siberian Branch of the Academy of Sciences of the USSR. 
    - Looks a bit like Lee J Cobb. 
    - Born 1928, so 47 in 1975
* Dr. Valentin P. Dymnikov, Валенти́н Па́влович Ды́мников headed this research, has been linked to the construction of physically complete and numerically effective models of the global ocean and atmospheric circulation aimed at the development of a global climate model. He introduced a new approachwhich allows one to solve several new problems of nonlinear atmospheric dynamics. A great contribution was made by Dymnikov to the development of a strategy of numerical experiments with a climate model. 
    - Very few pictures, none as a young man. 
    - Born 1938 so 37 in 1975, 87 in 2027. 
    - Ethnic Mari
    - Looks vaguely like Yuri Gagarin in terms of overall facial built but hairline, nose and chin are different.

* Dr. Alexey Ider Chinchuluun, the key contact 
    - male, about thirty
    - Ethnic Mongolian but grew up in the Soviet Union
    - For some reason, speaks Japanese
    - Looks like Amarkhuu Borkhuu
    - Also called Lyosha; Nathan calls him Lexy 
    
## Narratives

### 1975 events

Nathan travels to Novosibirsk on an exchange program to work with the team of Dymnikov on climate simulations. Dymnikov is on sabbatical near Moscow but still contributes to the research, although Nathan does not know this. Alexey is in charge in his absence. They work together on the simulations but the work is slow as explained in Bernard's report. Nathan and Alexey fall in love.
Alexey continues the work after Nathan has left, and manages to run the simulations, and Nathan is surprised at the resolution they have managed, but expresses this in careful terms.
On his return visit in Hawaii (which was planned to be soon to have continuity), Alexey defects and moves in with Nathan. He is not allowed to work on the simulations anymore so he starts a surf shop. His business quickly grows so Nathan quits his job at NOAA to help Alexey run it.

The problem in general is: if Dymnikov wrote this magic kernel, how come it is not in his article nor in his book? A possible explanation is politics: Dymnikov's publications are from after the end of Détente, in 1979. It was not acceptable to publish work that was a collaboration with US scientists. This would mean that Dymnikov's kernel was the core of an early US convection model for simulating cloud feedbacks. 

Another, minor political point is that, after the Kudirka Incident, there were new  guidelines for handling defections, to ensure that proper defectors were not sent back. 

> One of the principles embodied in these guidelines is that all cases of would‐be defection be reported at once to policy making officials, who could then take steps to make sure that the person involved was a genuine defector seeking political asylum.


### Scientific background

### Mieko's story

Mieko remembers when they moved to Hawaii, her husband bought his first surf board in a newly opened surf shop opened near Ala Moana beach in 1976. It was called Standing Wave and run by a gay couple, one of whom was a striking and funny Mongolian who spoke Japanese with a Russian accent.
She (or her husband) know that the shop still exists and is now run by the granddaughter of Nathan's brother.
She goes and talks to her and finds out their names, and indeed they were Alex and Nathan, and she also learns that Alex defected and that this is why they started the shop.

### 2027 events

@kagetsuko posts a request for help to find some old code
she clarifies the background

The narrator is intrigued and digs into the old documents. He finds out that there is something very peculiar with the simulations: they are much to detailed considering the capabilities of the system. 

@klimagalka replies, and a few weeks later finds out the material was moved to Moscow in the early 80s. She also finds out that Ider left the Center in 1976.

@silverstacks replies, and a few weeks later finds out that there is no material left at Hawaii University, and that records show that Dr Lightman left in 1976. He did not publish anything after that date.
They found an internal report that helps the narrator to make sense of the enigma, and makes it even more worthwhile to find the materials.

@klimagalka asks a friend. Unfortunately, he no longer works there, but he promises to ask one of his ex-colleagues.

More time elapses. @kagetsuko gets in touch with her aunt. 

Finally, @klimagalka reports back that the material was moved to the Computer Museum in {Moscow, St-Petersburg}
@kagetsuko contacts them with @klimagalka's help, but learns that the material was sold to Bletchley Park

@kagetsuko's aunt went to talk to the granddaughter.

@kagetsuko contacts Bletchley Park and they confirm they have some materials in the Archive. They arrange that the narrator can go and study them.

The narrator finds the punch cards used for the August 1975 run and scans them. However, the code is not complete. A crucial kernel is missing. 

The narrator returns to Bletchley Park for a more in-depth search, and finds materials on tape. One of the tapes is labeled "тт. Дубна́ В. Ды́мников 3 сен 1975"  The narrator recognises it as a teletype tape from a Robotron T163, and manages to decode it. 

With the help of another researcher he finally manages to convert the now complete code to modern Fortran.
@kagetsuko integrates it with her code, runs the simulations, and "the rest is history".

## Structure

I think I will tell the story through fedi posts and old documents as encountered by the narrator. 
But it starts with the narrator in Kyoto in 2030, and ends there too, and I think it would be neat to end with a tight bond between the narrator and Natsuko and her partner.
I could even speculate that they develop an interest in surfing and go watch the surf contests.

### A possible story structure 

could be:

- Prologue
Where we learn about the narrator
- Chapter 1: the setup "Models of Circulation"
Where kagetsuko posts her request, and the narrator asks for some details.
- Chapter 2: the history "Détente"
Where the narrator learns about the 1975 events and notices something odd.
- Chapter 3: the chase "Threads"
Where kagetsuko, with the help of klimagalka, silverstacks and her aunt Mieko, tries to trace the missing materials, and they learn a lot in the process.
- Chapter 4: "Bletchley Park"
Where the narrator goes to to the computer museum to look at the archived materials.
- Chapter 5: the denouement "Minsk"
Where the puzzle is solved and the world made a slightly better place.
- Epilogue
Where we learn more about the narrator

### More detail

- Prologue
Where we learn about the narrator: 
[[He is old and living in Kyoto, and talking about the events that brought him there (kind of).
Key introduction is something like "you might have read or heard in the media about the breakthrough in climate modelling that led to a dramatic and much needed improvement in forecasting of severe weather events, and so has helped save countless lives." I had a small role in that story, one that has not been mentioned in the official press releases and interviews. I had insisted on that. Nevertheless, I have a desire to tell this story, because I feel I am not long for this world. 
Other key points: the events date from 2027, but the past three years have been chaotic, with his wife's illness and death. So no time or space in the mind to tell the story. Now he is lonely, despite not being alone.]]

- Chapter 1: "Models of Circulation"
Where kagetsuko posts her request, and the narrator asks for some details.
[[
kagetsuko posts a request about some old FORTRAN code developed in Siberia/Hawaii in 1975, but without much detail (I need to work this out better)
The narrator asks about the context, and she explains about climate modelling, how the Soviets were first, what the problems are with resolution and scale, and that she hopes that this code will give her some ideas. 
So most of this chapter is the original post and the science. I can maybe already introduce the fedizens who may be able to help, klimagalka and silverstacks.
]]
- Chapter 2: Détente"
Where the narrator learns about the 1975 events and notices something odd.
- Chapter 3: "Threads"
Where kagetsuko, with the help of klimagalka, silverstacks and her aunt Mieko, tries to trace the missing materials, and they learn a lot in the process.
- Chapter 4: "Bletchley Park"
Where the narrator goes to to the computer museum to look at the archived materials.
- Chapter 5: "Minsk"
Where the puzzle is solved and the world made a slightly better place.
- Epilogue
Where we learn more about the narrator

# References

## Surfing
[](http://nakaimo.com/kitano/2018/09/14/235/)
[](https://www.nsa-surf.org/match/30th_jropen2022/)
[Hokkaido dissidents](https://www.huckmag.com/article/japan-surf-pioneers)
[](https://www.hawaiiansouthshore.com/blogs/hawaiian-south-shore-surfing-blog/the-history-and-science-of-surf-reporting-forecasting-on-oahu)
[](https://www.ncei.noaa.gov/access/metadata/landing-page/bin/iso?id=gov.noaa.nodc:0001754)
[](https://www.undergroundsurf.com.au/pages/local-motion)

## Bletchley Park National Museum of Computers
[](https://www.tnmoc.org/)

## SB RAS
[](https://icmmg.nsc.ru/en/content/pages/siberian-supercomputer-center-siberian-branch-russian-academy-sciences-general-purpose)

## BESM-6
[](https://usermanual.wiki/Pdf/FORTRANCDC1604RefManVol1Mar63.818451915/view)
[](http://mailcom.com/besm6/instset.shtml)
[](https://github.com/besm6/simh)
[](https://www.besm6.org/)
[](https://collection.sciencemuseumgroup.org.uk/objects/co8400208/besm-6-supercomputer-1968-1987-mainframe)
[](https://www.rbth.com/science_and_tech/2014/09/24/computers_in_the_ussr_a_story_of_missed_opportunities_40073.html)
[](https://en.topwar.ru/189960-rozhdenie-sovetskoj-pro-bjesm-saga-chast-iii.html)
[](https://computer-museum.ru/english/besm6.htm)

## Minsk-32
[](https://ajovomultja.hu/minsk-32?language=en)
[](https://computer-museum.ru/english/minsk_32.php)
[](https://inis.iaea.org/search/search.aspx?orig_q=RN:9384141)

## Robotron 
[](https://www.robotrontechnik.de/index.htm?/html/sonstiges/fernschreiber.htm)
[](https://www.robotrontechnik.de/index.htm?/html/sonstiges/fernschreiber.htm)

## USSR Climate modelling
[](https://link.springer.com/article/10.1007/s10584-022-03315-0)

## Marchuk
[](https://mathshistory.st-andrews.ac.uk/Biographies/Marchuk/)

## Dymnikov
[](https://www.degruyter.com/document/doi/10.1515/RJNAMM.2008.018a/pdf)

##  Hawaii

[Bike shop](https://www.bikeshophawaii.com/about/our-history-pg56.htm)
[Sumitomo](https://www.referenceforbusiness.com/history2/62/SUMITOMO-REALTY-DEVELOPMENT-CO-LTD.html)

G. I. Marchuk, V. P. Dymnikov, V. N. Lykosov, et al., Hydrodynamic Model of General Circulation of the Atmosphere and Ocean (Methods of Realization) (VTs SOAN SSSR, Novosibirsk, 1975) [in Russian].
 

G. I. Marchuk, V. P. Dymnikov, V. B. Zalesnyi, et al., Mathematical Modeling of General Circulation of the Atmosphere and Ocean (Gidrometeoizdat, Leningrad, 1984) [in Russian].

Clouds, circulation and climate sensitivity
https://www.nature.com/articles/ngeo2398
https://www.researchgate.net/publication/274407347_Clouds_circulation_and_climate_sensitivity/link/56961b3308aeab58a9a53652/download


https://nsarchive2.gwu.edu/NSAEBB/NSAEBB233/12-22-70.pdf

In a Dec. 22, 1970, memorandum describing a meeting between Nixon and Soviet Ambassador Anatoly Dobrynin, Kissinger described the lingering effect of the incident in U.S.-Soviet relations. 

Dobrynin then mentioned the Soviet irritation at …  the treatment of the defecting incident of the Soviet sailor.  He said he could not understand the American performance. If we had given asylum to the Soviet sailor, he would have had to make a protest, and the matter would have been forgotten within 24 hours. But, first, to return him to the Soviet ship, and then to announce daily how profoundly concerned the president was, filled Moscow with outrage. For all these reasons, there was now profound distrust in the Soviet Union. 
