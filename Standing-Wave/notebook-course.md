# Writing course notebook

## Wk1 notes and exercises

### 1.7 Appearance of people

Superdry
People were dressed for winter. Person with carry-on.
One season bag, probably a sleeping bag
He holds the paper between index and ring finger, like a smoker; it's The Guardian. It must be cold.
The way he holds his phone; serious dark loden overcoat
From the camouflage pattern you can tell the year of the jacket (I can't)
The flecked nail varnish; the tap-tap of the pen against her teeth
Wondering about the watch. Not interested in the phone
Woman with vaio. Her black bangs hide her eyes; there is a tension in the mouse hand.
Two women. Lots of rings, rabbit ring
Strange keyring, like a monogram; dog-eared notebook

### 1.8 Pay attention to all senses

### 1.9 Why writers write

* How did these writers come to write?
* Why did they start to write?
* Where there any similarities in their respective journeys towards writing?


### 1.10 Why writers write 

* What elements of your life experience and personal circumstance do you think might influence your writing.

- work
- being an immigrant
- things I love: Japanese, pictures, drawing, cooking, old movies
- people, places, situations I know, to steal from
- climate change

### 1.11 Develop a character from your notebook

TBD

### 1.12 Reading characters

We could do this with two characters from other novels.

TBD

### 1.13 Investigating characters

Quiz

### 1.14 Comparing your characters

TBD, refining of 1.11




