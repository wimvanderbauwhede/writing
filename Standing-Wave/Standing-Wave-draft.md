# Standing Wave

## Prologue
<!--[284]-->

Kyōto, April 2030

The vagaries of fate are like the waves on the ocean. They all have a distinct cause and origin, but when they break on the beach, it is no longer possible to disentangle them.

You might have read or heard in the media about the breakthrough in climate modelling that led to a dramatic and much needed improvement in forecasting of severe weather events, and thereby has helped save countless lives. I had a small part in that story, one that has not been mentioned in the official press releases and interviews, or even in the paper. I had insisted on that at the time. 

The events date from 2027, but the past three years of my life have been chaotic. That summer, my wife fell ill and I looked after her until she died, seven months later. Then I had to tie up all the loose ends that arise when someone so close to you dies. I don't remember much of it, or maybe it is more accurate to say that I don't want to remember it. It was like walking through a world of fog. When everything was finally settled, I felt a great emptiness. I considered going back to my homeland, but it held not attractions for me, and I had no longer any ties there. My wife and I had both loved Kyōto, and I thought I'd feel closer to her memory there. So I settled my affairs in the UK and moved to Japan. A simple sentence, but it proved to be  anything but simple, and I could not have done it without help. 
But I am in Kyōto, and it is the season of the cherry blossoms. I know _sakura_ in Japan is a huge cliché, but I can't deny that the warm wind blowing a storm of pink and white petals deeply moves me. But I'm digressing. I now finally have time and space in my mind to tell my story.

## Chapter 1: Models of Circulation
<!--[807]-->

* In which a request for help is posted, clarifications are asked and a promise is made.

On an otherwise unremarkable, ordinary dull day in the late winter of 2027, `kagetsuko` posted a message on her fediverse account that particularly intrigued me. 

She was one of those people I followed to have a nice balanced timeline, not one of my close circle but we were mutuals, regularly boosted and liked one another's posts and occasionally interacted. 
I had long forgotten why I'd started to follow her. I liked her handle because it referenced Aggretsuko, the irritable red panda from the eponymous anime; but it also could be read as _ka-getsu-ko_, meaning "full moon child"; and as _kage tsūkō_, which would mean "shadow sailing". I suspected this was entirely intentional. Her posts were often funny, sometimes flippant, frequently absurd and only occasionally serious.
Her profile said "science/chocolate/coffee; languages: Japanese (native), English (fluent)". No pronouns. Avatar a minor character from the Yuyushiki manga, quite obscure outside of Japan. Ran her own single-user server so she had to be somewhat geeky. I gathered she was an atmospheric scientist working on climate models. She'd done her PhD in the UK and then returned to Japan, but I had noticed a while ago she kept European hours. That was nothing remarkable, a lot of fedi people had non-standard wake/sleep cycles. And yet it was a little odd, one of those small mysteries that are the spice of life.

I was checking my timeline over coffee and I immediately noticed the post:
    
    @kagetsuko:
> Hi fedi, I am looking for some old FORTRAN codes that were developed in 1975 for the BESM-6 computer by a collaboration of scientists from the US and the USSR. They are simulation codes for a global circulation model. The scientists involved were Nathan Lightman and Bernard Miller at the University of Hawaii/NOAA Joint Climate Research Effort in Honolulu and Alexey Chinchuluun and Gury Marchuk and maybe also Valentin Dymnikov at the Computer Center in Novosibirsk. I've gone through all the declassified documents from the CIA  Electronic Reading Room, but I haven't been able to find the codes or any papers or reports about this research. I have emailed both institutions but I don't have any contacts there so I don't think I'll get anywhere. Any pointers welcome; please boost!

Now that was very interesting. The BESM-6! The near-mythical, highly influential Soviet super computer of the 1970s. It was what you got when you let an electronic engineering wizard design a computer with inputs from scientists, but cut out programmers and compiler experts from the development. It could solve differential equations at an amazing speed, but simple data movement was embarrassingly slow, because you had to do the pointer arithmetic in floating-point format. 

For all the antagonism between the Eastern and Western blocs in those days, the Soviets had quickly adopted the US-designed FORTRAN programming language for their super computers. FORTRAN was a marvel, the closest thing to immortality in the computing world: designed before there even were proper computers, and still going strong today; much maligned by "real programmers" but beloved by scientists, and still healthy and under active development. (Only we now called it Fortran.)

I wanted to know more. I replied:

    @lores:
> @kagetsuko I might be able to help a little, I'm quite familiar with the BESM-6 and ye olde FORTRAN and there are a few code repositories that I'm aware of. What is the context for your question?

Her answer came almost instantaneously:

    @kagetsuko:
> @lores Thank you so much! I'm doing a postdoc at ECMWF on novel general circulation models to improve prediction of extreme weather events. The Soviets were the first to develop such simulation models, there is a seminal book by Marchuk and Valentin Dymnikov from 1984 but the work had already started in 1974. The problem is that to predict extreme events we need very high resolution, but we also need a global scale. On those old computers, they had to be very efficient, in particular with memory, so I thought I might get inspiration from those old codes. From the letters exchanged between Lightman and Chinchuluun, they were getting very promising results, but somehow they never published.

The European Centre for Medium-range Weather Forecasts, in Reading. I knew it well. That explained her strange hours. I smiled to myself: a small mystery solved.
I reflected that it was a good time to do this kind of historical research: the war between Ukraine and Russia was finally over, and international relations had been normalised quickly. A few years ago, it would have been impossible. But 1975 was a long time ago and pre-internet, so it wasn't going to be easy.

I stared out of the living room window towards the rainclouds over the slate grey surface of the North Sea, less than a kilometre down the gentle slope on which the house stood. A year ago, we were hit by a hurricane-level storm, and unfortunately the MetOffice's predictions had underestimated it. It had ripped the roof off and scattered the contents, and what remained had been drenched by the relentless torrential downpour. We'd been homeless for months, and we were amongst the lucky ones: we hadn't been flooded, and we could rent temporary accommodation while the house was being repaired. With a better forecast, people would have had more time to prepare. It would have made a huge difference.  

I was retired, I was intrigued, and it was right up my street, so I rose to the challenge:

    @lores:
> @kagetsuko I'll have a look at the CIA docs and the BESM-6 repos

## Chapter 2: Détente

<!--[842]-->

* In which we learn about a cold war era scientific collaboration, and an odd result.

The CIA FOIA Electronic Reading Room is a veritable treasure trove of documents. It's amazing to see what those spooks had been squirreling. The communication between the University of Hawaii/NOAA Joint Climate Research Effort and Computer Center of the Siberian Department of the Academy of Sciences of the USSR comprised only about a dozen documents. There was a telegram from Henry Kissinger himself, showing that this programme was sanctioned at the highest levels. The introductory letters from Miller and his counterpart Alekseev were uninteresting, but the exchange between Lightman and Chinchuluun was full of fascinating technical details. It was clear that Lt. Dr. Lightman had done his homework: he must have read all the relevant papers by the Soviet scientists involved. As these were published only in Russian, the project must have gotten support from translators. More evidence that Kissinger considered it important.

Ahead of the visit, Lightman and Chinchuluun had quicky identified a potentially fruitful area of collaboration: the integration of  a NOAA convection model for simulating cloud feedbacks into the Computer Center's global circulation model. There was some discussion of initial test runs, but the most enlightening (and entertaining) document was a report by Lightman on his two-month visit. He complained that the work advanced slowly because of  the system's technical disadvantages and the total lack of punched card debugging tools. ("To verify the card punched, one had to punch a second card as a check. If the two cards matched, one assumed they were correct; if not, one assumed one card was mispunched.") 

But he also praised the staff for being extremely helpful: "A. I. Chinchuluun and V. K. Gusiakov assisted me in learning the use of the BESM-6 and in utilizing the other facilities. A. I. Chinchuluun was instrumental in helping me overcome the program changes. Without his dedication, I probably would not have been able to make the models work."

It was clear that in a short time, Lightman, Chinchuluun and Gusiakov had became close friends:  "The people I worked with were very hospitable. I was invited into the homes of A. I. Chinchuluun and V. K. Gusiakov. We went on some excursions  together, notably several days of camping and hillwalking near Luzhba in the mountains of the Abakan Range, and I enjoyed their friendship immensely. They took me to film festivals, art shows, ballets, and several banquets. A Russian picnic on an island in the Ob Sea was an unforgetable experience. A. I. Chinchuluun and V. K. Gusiakov helped me with problems of living in Novosibirsk.  Their patience and efforts made my stay so much more enjoyable."

Somehow the name of Gusiakov was familiar to me. Viacheslav K. Gusiakov, know as "Slava" to his friends, was an eminent expert on tsunami modelling and in 2012 he had been the co-author of a damning report by the Bulletin of the Atomic Scientists, "Fukushima: The myth of safety, the reality of geoscience". I remembered it well as I had been planning a visit to a colleague in Fukushima prefecture in 2011 when the disaster occurred, and I had therefore taken a more than casual interest in it. 

In 1975, Gusiakov must have been in his late twenties, fresh from his PhD defense. His assigned role in the project was that of the Center's offical host -- the report even mentions that he created a short film about Lightman's visit -- but clearly he had been a lot more hands-on than that. 

Apparently, Lightman and Chinchuluun had managed the integration of the models in the FORTRAN code, but had not managed to run the similations by the end of the visit. Follow-on letters from Chinchuluun in preparation for his return visit to Hawaii showed eventual progress, and the final letter even had some very preliminary results. 

Looking at those results and the assumptions on the models, I felt there was something very odd: with the resolution given in the final letter by Chinchuluun, and the approach described in the famous 1980 article by Marchuk and Dymnikov, "A mathematical model of the general circulation of the atmosphere and ocean", it would seem that on the BESM-6, the simulation would have taken unpractically long, much longer than the two months between the visits. Both scientists appeared to be meticulous and embedded in professional teams, so I had to assume the results were correct. There had to be some deep magic going on here. Either the model, and in particular the convection kernel, was different from  the published approach, or they had found some totally radical compiler optimisations. Both were unlikely: a novel model with that performance should surely have been published; and the Soviet BESM-6 compiler was an instruction-by-instruction translation of the binary of the compiler for the CDC 1604 computer,  totally sub-optimal.  But the facts were there: by my estimate, the model allowed an eight times higher resolution in every dimension, and even then was twice as fast as it should have been. So it was a thousand times faster than could be explained by the existing assumptions. 

As I had seen incredible speed-ups before, in my own work and that of others, I didn't get too excited: such results were usually not real, but the result of unoptimised baselines or unjustified assumptions, or just plain wrong. So I carefully double-checked everything I knew. I studied the BESM-6 architecture and instruction set and looked for ways a programmer might have handwritten the assembly code to improve the performance that much. There was definitely scope, easily for ten times speed-up, but nowhere near a thousand. It had to be to model itself, some radically new algorithm. Buf it so, how come it had never been published, not even in the internal reports of the Computer Center (which of course the CIA had managed to acquire)?

    @lores:
> @kagetsuko There's something strange about those results in Chinchuluun's last letter. It's much too fast compared to the state of the art at the time. I am assuming they were using a model similar to what Marchuk et al. describe in their paper (link). Do you know of any algorithms or schemes that would make a dramatic difference and that were known at the time?

    @kagetsuko:
> @lores No, I had assumed they used an early version of the published model by Marchuk et al. I'm not familiar at all with the BESM-6 computer, all I know is that this is the machine they used for the simulations and that they could program it in FORTRAN. So you say their results were too fast? What do yo mean?

    @lores:
> @kagetsuko If you look at the memory requirements for the model, and compare against how fast the BESM-6 could access that memory and compute on it, there is a huge performance gap. Even if they had hand-optimised the machine code, it is still more than a hundred times faster than what could be expected based on those published models. The only plausible explanation is some radically new scheme or algorithm in the cloud simulation part of the model. So if you can find that code, there could be something genuinely novel in it.

    @kagetsuko:
> @lores  That's really exciting! Let's hope we find it. お互いに頑張りましょう！

`kagetsuko` and I often had exchanges in a mix of English and Japanese. _otagaini gambarimashō_ is one of those quintessentialy Japanese phrases laden with cultural subtext, but here it basically means let's work hard on this together, and persevere until the job is done. Japanese is pithy.

## Chapter 3: The actors, Part I

<!--[1230]-->

* In which fedi offers help, and we get to know the players in the drama.

A few days later, there were two responses to `kagetsuko`'s post. One was from `klimagalka`, who by her bio was Verena Galina, a climate researcher at the Siberian Branch of the Russian Academy of Sciences in Novosibirsk. Stylised female-looking avatar, like something by Möbius.

    @klimagalka:
> @kagetsuko I work at the Siberian SuperComputer Center of SB-RAS. I will try and find out what happened to the archives of that project.

Another was from `silverstacks`, whose profile only said SilverStacksBooks, they/them. They ran an indie second hand bookshop in Honolulu.

    @silverstacks:
> Hi @kagetsuko, I am a librarian at Hawaii University in Honolulu. I'll be happy to ask around to see if there's anything left from that time. The JCRE was closed down when Bernard Miller died in 1979, so the archives might be scattered all around the University and NOAA. 

    @kagetsuko:
> @klimagalka @silverstacks @lores That's wonderful, thank you all so very much for wanting to help me! 

A few slow weeks followed. Interminable dark February ended after all, March came in like a lion. I trailed through the BESM-6 code repositories but could not unearth anything of interest. I also researched the scientists involved, in particular Marchuk, Dymnikov, Chinchuluun, Lightman and Miller. 

Dr. Alexey Ilun Chinchuluun was a bit of an enigma. There were no pictures of him. His name was clearly Mongolian, with a Russian first name which was not uncommon at the time. He got his PhD from Tomsk University, in Siberia, in 1973. I found his PhD dissertation, and it was brilliant. He was part of the hand-picked team led by the renowned Valentin Dymnikov, so he seemed destined for a great career. He was already co-author on several papers and technical reports of the Center. But after his 1976 visit to Hawaii, nothing. No trace of him to be found anywhere, neither in publications nor in the CIA's documents. 

Gury Marchuk, the _Direktor_ of the Computer Center in Novosibirsk, was no mystery at all. He was by all accounts a great scientist and leader, Hero of Socialist Labour, recipient of no less than four Orders  of Lenin, the Chebyshev Gold Medal and much more besides, and his career was an open book, if a very voluminous one. A very giant amongst men, and yet by all accounts an affable person. He looked like a less brutal, better groomed version and more thoughtful of Lee J. Cobb. He had practically single-handedly built up climate research in the USSR, and was instrumental in starting the general circulation model project at the Siberian Computer Center. At the time of the events, he was no longer involved in the technical side of the project. 

That part was left to Dr. Valentin P. Dymnikov, who also had an illustrious and public career and was, as far as I could make it, something of a genius, both in modelling and programming. Results obtained with the climate model he developed were included in the 4th IPCC report. He looked a bit like the young Yuri Gagarin, but was ethnically Mari, a Finnic people who had an Autonomous Soviet Socialist Republic. He was credited with one of the great breakthroughs in numerical simulation of nonlinear atmospheric dynamics. That sounded promising. And yet, he had not seemed to be involved in the project a the time. According to Lightman's report of his visit, Dymnikov was notionally the project lead but he was on sabbatical at the time, and Chinchuluun was the acting lead. Based on some documents I found, Dymnikov was at the time at the Joint Institute for Nuclear Research at Dubna, near Moscow, working on a compiler for the Minsk-32 machine, another one of those Soviet behemoths.

Lt. (JG) Dr. Nathan Lightman was Chinchuluun's US counterpart. He was a Lieutenant Junior Grade in NOAA's Commissioned Officer Corps and I found a picture of him looking a bit like John Garfield, dapper and charming in his uniform. His career before 1976 was very easy to follow, and was that of a model atmospheric scientist who excelled at computer simulation. The visit to the USSR, which he got on the strength of those skills, should have given it a considerable boost as well. But just like Chinchuluun, after 1976, nothing. It seemed like they had both gone up in smoke. I found an interview with him organised  by NOAA in 2020 as part of their ongoing oral history project. He talks about his Jewish family background, how his family emigrated from Vilnius and ended up in Cincinnati, about his education, and how he became an atmospheric scientist, but of his visit to Siberia he only tells a brief anecdote, how he found out that the appartment he was staying in was bugged. He says nothing about why he left NOAA, and the interviewer does not broach the subject either.

Finally, there was Dr. Bernard Miller. He got his PhD in 1965 from the Institute of Geophysics and Planetary Physics in San Diego on the topic of climate physics and he joined the newly created Joint Climate Research Effort right away. In 1972, he became its Director. In pictures from that time he looks like I imagine Robert Redford would have looked if he had been French. During the interview, Lightman talked a lot about Miller. I got the clear impression that he was an exceptional and rather flamboyant person with a knack for bringing out the best in people, and a brilliant organiser with a win-win style of management, but no longer an active scientist. It is also clear from the interview that Miller was not only Lightman's boss but also his mentor,  and that they were very close. Sadly, Miller died young, in 1979.

The picture was clear: it was with Lightman and Chinchuluun that our quest lay. They were clearly the key authors of the model code. And very inconveniently, they seemd to both have disappeared into thin air.

I shared these thumbnail sketches with my co-conspirators. To my surprise, `silverstacks` replied 

    @silverstacks:
> @lores Here's how I imagine Lightman getting his assignment from Miller @kagetsuko @klimagalka

> One fine morning — but of course in Hawaii, most mornings are fine — the director of the Joint Climate Research Effort breezed into my office. Bernard was an easygoing guy and never stood on protocol, even though he was one of the key decision makers at NOAA. 
> "Nathan," he said with a mischievous smile, "how would you like to go to Siberia?". Talk about a bolt from the blue, but I wasn't going to let it show. I shrugged, "What gives?" 
> "Well," he said, as always getting straight to the point, "as part of Kissinger's 'détente', we've been having talks with our Soviet counterparts to set up a joint program on simulation of ocean-atmosphere circulation models."
> Now he really had my attention. This was right up my street, and the Soviets had the best theoretical models, even if we had the better computers. "So what's the deal?"
> "For the first phase, we've agreed to sending someone to their new Computing Centre at Novosibirsk this summer. I haven't given them a name yet, but I'm sure I could find somebody if — "
"No way," I interrupted. "I'm definitely in!"
> Bernard beamed. "Wonderful! Let's start planning." And without further ado, he sketched out a travel plan and itinerary. It was uncanny, so perfectly was it tailored to me, to the last detail. He clearly knew me much better than I had assumed.  
> He was a handsome guy and I liked him a lot, but I didn't fancy him. I wondered if maybe he was more interested in me than I had realised. I hoped not. He was married, and a big shot, and it would definitely complicate our working relationship.
> "Don't look so surprised," he said, "I took the itinerary of my trip to the USSR of five years ago. I figured it would work for you too."
How absurdly simple. I was relieved. Overthinking is one my faults.

So apart from being a librarian and bookshop owner, `silverstacks` was also a writer. 

        @klimagalka: 
> @silverstacks Amazing! You'll have to do one on Chinchuluun and Marchuk as well!

        @kagetsuko:
> @silverstacks Yes please!!! @klimagalka

`klimagalka`, `silverstacks` and `kagetsuko` really hit it off. `klimagalka` has a wicked sense of humour, `kagetsuko` loves leaning towards the absurd and `silverstacks` was just rolling with it. They sprinkled my timeline with brief, witty exchanges peppered with obscure emoji like ​:neocatangelpleading:​ , :nekowave: or :blobmeltsoblove: . I joined in the fun occasionally but was mostly happy to sit back and enjoy the fireworks.

March had nearly gone, if not like a lamb, its weather having been more volatile than ever. The days had finally started to lengthen when `klimagalka` got some news.

        @klimagalka:
> @kagetsuko Unfortunately the material was moved to the Hydrometeorological Center in Moscow in the early 90s, when the USSR broke up. According to the personel records, Alexey Chinchuluun left the Center in 1976. I couldn't find anything else about him. I have a friend at the Center in Moscow, I will ask them. @silverstacks @lores 

[[ maybe here say something about how kagetsuko is so good at rallying the troops ]]

Shortly after, `silverstacks` managed to unearth a very interesting historical document.

    @silverstacks:
> @kagetsuko I found an internal JCRE technical report from 1976 about the work by Lightman and Chinchiluun. I will DM you a link. Apart from that there is no material left at Hawaii University as far as I can find, and I dug really deep. Records show that Nathan Lightman left in that year. He did not publish anything after that date. @klimagalka @lores

The report confirmed the performance and resolution of the model. The authors note that their old CDC 3600 could not match that performance but they did apparently not consider it exceptional. That was not all that surpising as they did not have the expertise to judge the capabilities of the BESM-16. And the much-publicised performance of that machine on the Apollo–Soyuz Test Project, where it was said to have beaten NASA's computers by a large margin, probably added to the myth of its speed.

The report further mentioned that, because the code was completely written in FORTRAN, it would be possible to run it on JCRE's soon-to-be-installed CDC Cyber 175. But it did not describe the model kernels in any detail, and there was no code attached. 

Enlightening as it was, it didn't get us much further. But at least we knew the results were not a fluke, and that we could rule out hand-written assembly, which would have been very hard to port to any modern machine. 

A further update came from `klimagalka`, she'd hit another obstacle:

        @klimagalka:
> @kagetsuko Just to update you, my friend no longer works there but they have promised to ask one of their ex-colleagues. @silverstacks @lores

Meanwhile, `kagetsuko` had tried another angle. She told me about it later, when I had moved to Japan. All she posted at the time was:

        @kagetsuko:
> @silverstacks @klimagalka @lores I have an aunt in Hawaii and she just solved a part of the puzzle. In 1976, Chinchuluun and Lightman started a surf shop in Honolulu! Apparently it still exists. The both must have quit their jobs. Chinchuluun  must have defected so he could be with Lightman. That's why they never published.

This is the story  as `kagetsuko` told it to me one hot summer afternoon in Kyoto, while we were drinking codl barley tea and eating watermelon:

## Chapter 4. Mieko's story
<!--[1707]-->

* In which `kagetsuko` gets answers from an unexpected corner

It had been puzzling me that there were no publications resulting from the 1975 collaboration between the teams at University of Hawaii-NOAA and the USSR Computing Center in Novosibirsk. From the archived official communication between Lt. Dr. Nathan Lightman of NOAA and Dr. Alexey Chinchuluun of the Computing Center, the work  during the initial visit of the former to Siberia had been very promising, and the return visit of the latter to Hawaii should have led to publishable results. I had  enquired with the official institutions, but their replies  were bound to be slow, if they came at all.
Another way was to try and find someone who had known Nathan Lightman. Aunt Mieko had been living in Hawaii from before the time of the events, and she was well-connected. It was a long shot, but I'd made up my mind to ask her. We chatted regularly anyway so I'd arranged a call. 

She was looking good as usual. The Hawaiian sun was streaming through the window and lit up her features. For as long as I'd known her, she'd had her hair in a kind of short frizzled bob with bangs just skirting her eyebrows, and not a grey hair in sight. Maybe that was the effect of the cosmetic software, but I didn't think so. Superficially she seemed very severe, but if you knew her better you couldn't miss the twinkle in her eye and the slightly amused turn of her mouth. She really was my favourite aunt.<br>
I didn't want to get straight to the point with my enquiry, as that seemed a bit rude. Instead, I asked her how she had ended up living in Hawaii. 

"It's not a very interesting story," she said, "and it was a long time ago. It all started in 1972. Your uncle and I had both started our final year at Waseda. He did economics, I did maths. He got lots of job offers because he was a good student. 
One afternoon he came back from a Sumitomo recruitment session brimming with enthusiasm. 'Mieko,' he said, 'how would you like to move to Hawaii?' <br>
As you know, my parents had died in the Chile earthquake tsunami, so nothing tied me to Japan. I felt a thrill. Hawaii had always seemed a distant, unattainable paradise to me. 'But what about your parents?' I replied.<br>
'Mum has a brother who lives there, so I think it will be OK. The offer is really good too, I think dad will be proud.' 
Shigeru could be very convincing. Not only did his parents agree to the move, they even proposed to pay for our wedding.  I think his mother felt sorry for me because she had also lost relatives in the tsunami.<br>
We got married at the Hachimangū shrine in their hometown Kamakura. I was crying all the way while we walked in procession along the dankazura, I was totally overwhelmed that they would do this for a poor hāfu orphan like me.<br>
"Hāfu?" I said, startled. I'd never known that. <br>
Aunt Mieko made a brushing motion with here hand. "My mother was Singapore Chinese. My father met her when he was posted there by his company, at the end of the war. It's not important. Shigeru's aunt is Hawaiian, so they were already used to the idea." <br>
"Sumitomo had just opened an office in Honolulu and they had a lot of openings there. But they must have thought very higly of Shigeru: he was being fast-tracked for management. They arranged everything: the travel, a house in East Honolulu, a car. I still remember how it felt in those weeks. The house was huge by Tokyo standards; the weather was fabulous; everything was new and exiting. It really was like paradise."<br>
I didn't want the story to take a sad turn, so I changed the subject to what I knew was one of her favourite topics. 
"I bet the beaches were amazing too! Didn't uncle Shigeru love surfing?" <br>
"And so did I, I loved it. But Shige, he lived for nothing else. I only realised much later, but surfing had been his real motivation for the move. He had this friend, Tōru, who was one of the famous Hokkaidō surfing pioneers, you know?"<br>
I'd never even heard of surfing in Hokkaidō, so I just made some non-commital noise.<br> 
"Well, I think it was Tōru's stories of surfing at Waimea Bay that sold him on Hawaii. He never told me, I think he was afraid to jinx it. We learned to surf in our spare time, but we were both very busy so it was mostly something for the holidays."<br>
With a look of admiration, she continued "But Shigeru was patient: he worked for the company for several years, until he felt he'd paid them pack for their trust in him. Then he quit. I had taken on maths tutoring and thanks to aunt Haunani I got a lot of students so we managed to save quite a bit  of money in those short years. With our savings, we bought a small house with a workshop, not far from the Ala Moana Center, and we started a bicycle shop."<br>
She rolled her eyes for a moment "It seemed a crazy idea to me but it quickly became a success. I think Shigeru had been planning this for a long time, and used his work contacts to secure good supply contracts. We sold Japanese bicycles, the new Kuwahara frames with Shimano parts, and they were really popular. We soon got  really good at repairs too.  A year later we were able to employ somebody full time, and finally Shigeru could begin to surf in earnest. There was a new surf board shop that had opened up a few blocks towards the beach, and that's were he got his first custom made board."<br>
A faint smile played on her lips then, and she had the look of someone replaying a fond memory "I remember the first time we went into that shop. I had expected some tall blond American, but the owner was a Mongolian-looking guy, only a few years older than me. He greeted us in Japanese and introduced himself as Alex. He spoke in a peculiar way, not with the usual English-inflected drawl, but with a strong Russian accent."<br>
"Russian?" I pricked up my ears. This was unbelievable: my aunt knew a Mongolian-looking Russian called Alex in the seventies. What where the odds?<br>
She nodded "I never asked him, of course, but both his English and his Japanese had a strong Slavic influence. He was a genuinely funny guy as well, always made me smile, and very smart too."<br>
It could hardly be a coincidence, but I had to make sure. So I asked "Did he run the shop all by himself?" I guess that was a bit of an odd question, but aunt Mieko barely raised her eyebrows. "Funny you should ask that. When the shop opened, it was just him, but not long after, he was joined by his partner, a goodlooking America guy called Nathan."<br>
"His partner?" <br>
Aunt Mieko gave me a look as if I was a bit slow "Well yes, they were a lovely gay couple. After it became legal, I think that was shortly after we arrived, a lot of them came out."

So, totally unexpectedly, I had the answer to my riddle. There could be no doubt about it:  Dr. Alexey Ider Chinchuluun of the Computing Center in Novosibirsk had come to Hawaii and stayed there, and had opened a surf shop with Lt. Dr. Nathan Lightman of NOAA. What an amazing twist. <br>
I tried to imagine what had happened. Probably, when Nathan visited Novosibirsk, Alex had fallen in love with him. Then, on his return visit to Hawaii, he must have defected so they could be together. But the US authorities wouldn't have allowed him to continue his scientific work, for fear of spying or for political reasons, not to upset the Soviets. So he set up a surf shop, and Nathan joined him to run it. And so none of their research work as ever finished. That would explain the lack of publications. <br>
"He was charming," aunt Mieko went on. "Shigeru and I both liked them a lot, and we often hung out on the beach together. Nathan was a very good surfer, he had been doing it for years, but Shigeru soon got even better than him. Alex never got the hang of it, he was even worse than me. 
Their shop still exists, it's run by a granddaughter of Nathan's brother. It now has a coffee shop now as well, and I sometimes go there when I feel lonely."<br>
Uncle Shigeru had died in a typhoon more than twenty years ago, and aunt Mieko had never remarried. She had a very active social life, but you can be lonely even while you're with other people. <br>
"Such an amazing story," I sighed happily. "But what a risk you took! Giving up a respectable, secure job, just to go surfing."<br>
"We were both much the happier for it" said aunt Mieko. "We had a lot less money, that's true, but Shigeru's life had been very stressful. It was really a relief. The surfer community was full of lovely people too. It was  great time."<br>
She looked at me with that severe twinkle "You're still young, Natsuko-chan. Don't throw away your youth for security!"<br>
Ever the firebrand, my dear aunt Mieko. We said our goodbyes and I closed the call.

## Chapter 5. The Actors, Part II

* In which we learn a little more, some of it true, some of it made up

    @silverstacks:
> @kagetsuko What's the name of that shop?  @klimagalka @lores 

    @kagetsuko:
> @silverstacks My aunt didn't say, but it's near Ala Moana beach, and it's run by a lesbian couple. @klimagalka @lores 

    @silverstacks:
> @kagetsuko Oh, I know it! That must be Standing Wave. It's a real institution! @klimagalka @lores 

    @silverstacks:
> @lores @klimagalka @kagetsuko By the way, here's how I imagine Chinchuluun got roped into this:

> I remember the day the _Direktor_ of the Centre came into my office —— the great Gury Marchuk himself, Hero of Socialist Labour. It was the day that changed my life.
> I was at that time working with the genius Valentin Dymnikov on a global ocean and atmospheric circulation model, a project initiated by Marchuk. Despite being so high and mighty, he was a friendly man who knew even junior staff like myself by their first name. "Lyosha," he said, "you are the polyglot of the team, how would you feel about working with an American?" 
> Apart from Russian and my native Mongolian, I knew Japanese and English. I claimed it was useful for reading scientific papers, and of course it was, but in reality I was fascinated by the cultures of Japan and the US. "An American?" I asked, suprised. "Yes. To further the bond between our two great nations, Comrade Molotov has set up a scientist exchange programme, and the American agency NOAA is very interested in our work on the global circulation model. One of their scientists would visit us for a few months this summer." 
> I was delighted and could hardly contain my enthusiasm, but I hesitated. Surely this should be Valentin's job? He was the project lead, and spoke English as well as I did. As if guessing my thoughts, Marchuk said "Comrade Dymnikov is going on a sabbatical for a year in the spring, so he won't be here. And don't be too humble, Sasha. He told me that besides himself there's nobody better acquainted with the work than you." 
> That was high praise, especially coming from Valentin Dymnikov. "I'll be honoured," I stammered, and I felt myself blushing. Marchuk smiled broadly. "Wonderful!", he said. "I will tell Comrade Alekseev to forward you the correspondence we have received so that you can make the preparations. There are quite a few technical issues to be discussed." He turned to go. At the door, he looked back with a twinkle in his eye. "Maybe you'll get to go to Hawaii on a return visit." 
> Dr Alekseev was Marchuk's deputy, and offically responsible for any international communications. In practice he was happy to delegate with minimal oversight. The documents he forwarded me started at the highest level, with a telegram from Molotov himself. But the last letter in the trail was from an actual scientist at NOAA, a Lt Dr Nathan Lightman, with a list of technical questions. We wrote back an forth several times. From his letters, Nathan was a kind and considerate person, and I looked forward to his visit.
> The day Nathan's plane was due, I went to the airport as part of the welcome committee. The man in the alien uniform who climbed down the disembarcation staircase reminded me vaguely of an actor in an old US war movie.
> When we met and shook hands, for an instant that seemed to last for eternity, a spark jumped between us, bright as the lightning between the terminals of a Van de Graaff generator. It painted the entire arrival hall in an electric blue light, it must have been obvious to everyone. 
> Then he spoke "Hi, I'm Nathan Lightman," he said and reality snapped back. His voice was mellow, with a lovely American accent." Alexey Chinchuluun. Delighted to meet you, Dr Lightman." 

    @kagetsuko: 
> @silverstacks sugoiiiii! @klimagalka @lores 

    @lores: 
> @silverstacks amazing, you might have been there! @klimagalka @kagetsuko

    @klimagalka: 
> @silverstacks :blob_love_melt: so sweet! @lores @kagetsuko

Bizarre though it was, the story made total sense to me. While after the Kudirka incident guidelines had been put in place for handling defections, to ensure that "proper" defectors were no longer sent back to the USSR, no defector would have been allowed to work in such a sensitive position. So now at least we knew what had happened to the scientists and why they had stopped publishing. It also explained indirectly why the BESM-6 code was never published either. If one of the key members of the team defected to the US, it would not have been politically acceptable to use the NOAA code;  so they must have removed that part, despite the superior performance. That would explain the 1980 paper. But we were no closer to solving the central mystery. On the contrary, it was now clear we would not find any publications. Finding the code was our only chance.

## Chapter 6. Bletchley Park
<!--[432]-->

* In which we get close to the heart of the mystery

It was well into April, the weather had turned lovely, the air started to have that earthy spring smell and new green appeared everywhere, when `klimagalka` finally discovered what had become of the materials.

        @klimagalka:
> @kagetsuko My friend's colleague says the material was moved to the Computer Museum in Ekaterinburg. I have contacted them, will keep you posted. @silverstacks @lores

And a few days later:

        @klimagalka:
> @kagetsuko You're not going to believe this, but they sold the material to National Museum of Computers in the UK. I'm afraid I can't help you any further. @silverstacks @lores 

        @lores:
> @kagetsuko @klimagalka That's in Bletchley Park, it's not very far from where I live. I'll be very glad to go and have a look at it if you can make the introductions.

`kagetsuko` contacted the museum and they confirmed they had some materials in the archive that originally came from the Siberian Computer Center and were dated between 1970 and 1980. They agreed that I could come and study them.

And so, one fine morning in the early May of 2027, with every bird in the dawn chorus competing for its space in the audible spectrum, I set out for Bletchley Park, which was about 200 km as the osprey flies, but as the railway network in the UK is so London-centric, it was a three-hour hour journey. 

The archivist showed me the materials, a few storage boxes with paper documents, 12-inch floppy disks, various kinds of tape and, lo and behold! several thick stacks of punched cards. They were well indexed and I had no trouble with the Russian captions. I found the bricks of cards used for the 1975 runs, scanned them with my phone, and returned home. I had a decoder for the BESM-6 punched card format on my laptop, so I spent the return journey in decoding the scans into FORTRAN source code. Success at last! It was night when I returned, so all I did was post a quick message and go to bed.

        @lores:
> @kagetsuko @klimagalka @silverstacks I've got the source files! Will check them tomorrow.

Next morning after breakfast, I started checking the source code. It was not a very large, so it was easy to work out the structure, but it was rather convoluted. Not that it was poorly written, but such coupled models have a complex data flow and very large numbers of parameters. It was easy to separate the NOAA cloud feedback code from the Soviet code, purely on coding style. However,  on closer inspection, the code turned out to  be incomplete. The crucial kernel was missing. 

        @lores:
> @kagetsuko I have the code for the whole simulator, except for that crucial convection kernel. I will go back and see how I could have missed it.
@klimagalka @silverstacks 

## Chapter 7. VEB Kombinat Robotron
<!--[861]-->

* In which the puzzle is solved and the world made a slightly better place.

Back to Bletchley Park, for a more in-depth search. The archivist was not encouraging: she said they'd carefully inspected, categorised and index the whole lot, and none of the other materials contained source codes.

        @lores:
> And yet there lie in their archives many records that few now can read, even of the experts, for their formats and encodings have become incompatible to later systems. And, @kagetsuko, there lies in Bletchley Park still, undeciphered, I guess, by any save myself since the USSR failed, a roll of tape that Valentin Dymnikov punched himself."

Trusting to the museum's experts, I had scanned the tapes quickly on my previous trip, looking for backup tapes only, but there had been nothing of interest. Now I looked closer. The BESM-6 had had a teletype connected to it, so it was not impossible that someone had connected to the machine over the teletype network and contributed code. It was a long shot, but it was soon rewarded: one of the tapes was labeled "тт. Дубна́ В. Ды́мников 3 дек 1975". By now, I had learned enough Cyrillic to read this as "TT. Dubna V. Dymnikov 3 Dec 1975". A tape of a teletype communication by Valentin Dymnikov, from Dubna! I inspected the yellowed, perforated paper ribon closely, and to my delight I recognised the format: it was from a Robotron T163, a really obscure teletype machine that had supported both Cyrillic and Latin. I felt like Gandalf discovering the scroll of Isildur.

Robotron had been a conglomerate of electronics companiesin the DDR (called a "Volkseigener Betrieb Kombinat" in German), and their "Fernschreiber" (teletypes) had been highly regarded. For some reason I had at some point studied the encoding format, a variant of their "Fernschreib-Alphabet 2", and wrote a decoder for it. Although it was a 5-bit tape, it supported 6-bit words with a switch character, and could therefore encode the GOST 10859 character set as used by the BESM-6. I discussed with the archivist the best way to digitise the tape. Luckily, they had a compatible teletype to run the tape, and we simply filmed the run with a stationary camera. I spent the return journey hacking a little script that split the video in static frames, and the next day I dusted off my old decoder decoder code and processed the stills from the video. The result of all that work was a few pages of FORTRAN-IV code: Dymnikov's missing kernel. 

I posted an update, but we were still not done yet: this old code could not easily be used with today's compilers. 

        @lores:
> @kagetsuko I've found it, Dymnikov's missing convection kernel. It's in FORTRAN-IV, I still need to translate the whole thing to modern Fortran, but we are really close now! @klimagalka @silverstacks 

        @kagetsuko:
> @lores 頑張ってね！

Luckily I had a friend at the University of Glasgow who had build a dedicated compiler specifically to convert old FORTRAN into modern Fortran, suitable for accelerators like GPUs and FPGAs. Our code was older than what he usually targeted, but he agreed to help -- of course, he simply couldn't resist the challenge -- and we finally managed to convert the now complete simulation code to modern, accelerated Fortran. I checked that it compiled correctly and sent it off to kagetsuko.

        @lores:
> @kagetsuko It's done! I've DM'ed you the repo link. I am on tenterhooks about the performance and results! I hope you have a decent GPU to test it on @klimagalka @silverstacks 

        @kagetsuko:
> @lores Amazing! Your friend must be a magician. Thank him from me and don't worry, I have access to state-of-the-art hardware. Onwards!

 `kagetsuko` lost no time in trialing the code. Hardly a day had passed when she replied:

        @kagetsuko:
> @lores I integrated it with my own code own and ran some benchmark simulations on our GPU cluster. All very preliminary of course, but it's literally incredible: it allows an 8x higher resolution in every dimension, for the same speed and accuracy, so a 256x speed-up! It's nothing short of miraculous. @klimagalka @silverstacks

## Chapter 8. Details of attribution

* In which `kagetsuko` ensures that credit will be given where it is due

[[ I guess this might have to become a separate chapter, maybe entitled "Details of attribution" or "The politics of science"
kagetsuko realises the implications right away and asks to have a video chat to discuss the approach. This gives me a chance to describe her, klimagalka and silverstacks; the latter I'm not sure, it would make sense to have her be native Hawaiian but that would of course be guessing on the part of the narrator. ]]

`kagetsuko` had organised a video chat to celebrate the success of the adventure, and to discuss the next steps. 

Three faces appeared on my screen. 

`silverstacks` was an enby in her late forties or maybe early fifties, who looked a Hawaiian native to me, maybe with some East-Asian ancestry as well, but that was of course a guess. Their hair was short, shot with grey but still quite dark. With a perpetual semi-frown and large glasses with squarish, translucent white frames they looked distinctly intellectual. 

`klimagalka` was in her late twenties or early thirties, with a strong-boned face, deep-set dark eyes, shoulder length dark reddish-brown hair and a hint of a mischievous smile.  Because of her Russian name and her avatar, I'd had a mental picture of her as blond and blue-eyed, but she wouldn't have looked out of place in one of those Siberian ethno-pop bands. Later I learned that her family came from Kyrgyzstan. 

`kagetsuko` reminded me instantly of a character in an old East German/Polish scifi movie, Dr Sumiko Ogimura in "Der schweigende Stern". That same inquisitive look and stern eyebrows. Her hair was much shorter and shaved at the sides, making her look at the same older  and more boyish than the actress in that movie, Yoko Tani. 

As a late-middle-aged white male I felt somewhat out of place in their company. I wished I was less male, less pale and with better cheekbones.
<!--We did a real names introduction.  `kagetsuko` said we could call her  Natsuko; `klimagalka` prefered Verenka and `silverstacks` said just call me Silver.-->
But we had all brought our favourite drinks and nibbles, and the usual awkwardness amongst geeks proved an effectice icebreaker. After a toast, `kagetsuko` got straight to the point. I mused how peculiar this was for a Japanese person who had lived in the UK. 

"I can never thank you enough for what you have done for me. This paper will cause a storm when I publish it! I have no doubt that it will be the most important work in my career, and it will lead to huge improvements in simulation of extreme weather events. Therefore I want to make sure that I don't mess it up, and I would really like your advice on a few points. First of all, this is not just my work. To be fair, my contribution is quite minor. I want to make sure that everyone who contributed gets credit, even if they are no longer alive. We also need agreements to open source the code, and we need to work out acknowledgements. Especially the three of you should get an acknowledgement because without you I could never have done this."

I explained that I'd rather not be acknowledged in the paper, but that I suggested she acknowledge my friend from Glasgow instead, or maybe even considered co-authorship for him as it was his compiler that had created the modernised code. 

`kagetsuko` was dismayed: "But then you won't get any credit at all!" 

`silverstacks` suggested it was OK for them to be acknowledged, as long as it was done anonymously. I agreed with that and proposed that maybe there could be a general acknowledgement of everyone who helped track down the code.

`kagetsuko` continued "There is no rush. ECMWF will need to make very sure about these results, so it will take time. I will need to get support and compute time, and we'll have to create an super-detailed stress test bench for that code. Just doing that will probably take a few months. 

I would  also like to involve both University of Hawaii/NOAA and the Siberian SuperComputer Center in the validation of the model. This paper is going to cause a storm so it has to be absolutely watertight. We'll have to be super thorough. If the code is independently verified by groups of experts in the US, Russia and Europe, it will make the paper much more robust. I also think it is the right thing to do because it's fundamentally the work of Lightman, Chinchuluun and Dymnikov. But for that I need your help. I will need contacts at NOAA and SB RAS that can help organise this and also decide on who should get credit and in what way.
I'll also have to discuss this at home because technically I'm an employee of Kyoto University so they will want credit too. Only when all that is sorted can I start thinking about writing the paper." 

`klimagalka` was more than happy to arrange the contacts at SB RAS: "This is super exciting. Maybe I can even find one of the old guys who were involved at the time and who remembers Nathan and Alexey, like Viacheslav Gusiakov, or maybe even Valentin Dymnikov himself."

"That would be amazing!", `kagetsuko` replied, "I will definitely push for Dymnikov to get authorship, even posthumous. But if  we could just ask him, that'd be fantastic."

`silverstacks` said they knew who to approach at NOAA. And they went further: "I think I should get in touch with Nathan's grandniece. This work concerns Nathan and Alexey very closely, so I think she should have a say in this." Of course we all agreed on this. 

[[
More about the paper ("this will cause a storm when I publish it!")
This is a good occasion to demonstrate that kagetsuko is not only a top-class scientist but also understands the politics
- ECMWF needs to make very sure about this, so it will take time, and be a many-author 
- the fedi ppl will get an ack
- the scheme needs to be open sourced 
- The Siberian SuperComputer Center has a historical stake in this
- but so has NOAA, and so it will probably have to be the named Suzumiya-Lightman-Dymnikov, SLD. Unless this was really Bernard's kernel, a nice touch, then it would be Suzumiya-Miller-Dymnikov
So there will be a long period of nothing happening. 

I also still need to tie up e.g. why Dymnikov never published his kernel => I think I did that 

So there will be three sites, say with 2 people on each site, and their boss, and then there's the 3 real authors.
]]

A month later, there was a long message from `silverstacks`. She had gone to the surf shop and met with Nathan's grandniece Alice. It turned out that Nathan was still alive and well, living in a nice serviced apartment in Makiki. They went to visit him together.  [[ 

    @silverstacks:
> @kagetsuko While we were on our way, Alice told me that your aunt had come to the shop to ask about Nathan, and had gone to visit him. And when we met with Nathan, one of the first things he told Alice was, "Guess what? Mieko came to see me last week. You know, her husband Shigeru was one of our first customers? They ran a bike shop and we went there for our bikes. Shigeru had this really cool trailer for surfboards, but they didn't sell them. Thought there would be no demand for them. In those days everyone was still driving everywhere. But the moment we saw it, Lexey and I both wanted one, and Shigeru arranged it. We turned a lot of heads when we cycled to the beach with our boards in tow." His eyes shone with the memory.

[[
    @silverstacks:
> @kagetsuko @lores @klimagalka 
]]
We sat under the pergola on the terrace, with a magnificent view towards the ocean. I asked him what happened when Alexey visited. This is the story he told me. I've only embellished it a litte bit `^_^`:

[[   @silverstacks:
> @kagetsuko Like we discussed, I also asked him if he wanted authorship on the paper, and if he would agree to have the scheme named after him. He was so moved he actually cried. He insisted that Chinchuluun and Miller had to be co-authors, and he will provide the justifications. @lores @klimagalka
]]


## Chapter 9. Standing Wave
<!--[1157]-->

In which we learn of the events in Hawaii in the winter of 1976

When I returned to Hawaii from Siberia in early October, I was at the same time deliriously happy and desperately lonely. Love is terrible. To think that only two months ago, I had no idea Lexey even existed, and now I couldn't imagine living without him. 
January had come and we'd had our first Kona storm of winter when finally the wait was over: he was here, we were back together!

We had made lots of plans, during that hot summer in Novosibirsk, and we were eager to get on with them. But there was still work to be done, and appearances had to be kept up. It wouldn't do if the project got wind of our real intentions before we had realised them. 

Or so I thought. But once again, I had underestimated Bernard. I should have known.

Lexey had been here a week, and we were making really good progress with the research work. To the rest of the team, we were just two scientists with a professional and productive working relationship. Bernard got on great with Alexey, and I was glad for that, although truth be told, I would feel a slight pang of jealousy whenever Lexey laughed at one of his jokes, or Bernard payed him some perfectly innocuous compliment. 
That morning, when I walked past the open door of his office, Bernard hailed me to come in. 

"Shut the door, please, Nathan," he said. "Have a seat". He motioned me towards one of the easy chairs he had in his office, and he sat down in the other one. He looked at me thoughtfully.
Whether it was because of his Dutch mother or his Belgian upbringing I don't know, but Bernard had a habit of coming straight to the point. 
He looked me straight in the eye and said "I think Alexey will want to stay here after his visit, don't you?" 
I was dumbfounded. How could he have found out? Did he have us watched? The game was up!
"Don't look so shocked!" he laughed. "If you had wanted to keep it a secret, you should have started with being less glowingly happy when you returned from Novosibirsk." 
Once again I realised, belatedly, that this man knew me almost better than I knew myself.
He continued "And when Alexey got of the plane, you flew straight into his arms!" 
I started to protest, but he held up a placatory hand. "All right, all right, you didn't. But it was plain to see that you would have wanted to. And the way you look at one another when you think nobody's watching."
"Everybody knows, then?" My voiced betrayed me, but Bernard smiled sympatethically.  
"Of course not. Nobody but me. The others tend to be a lot less observant, it simply wouldn't occur to them." 
I was somewhat relieved, but not for the first time, I wondered about Bernard.
"Now," he said, once again with alarming directness, "what we need is a convincing story for Alexey. With the new guidance, he can't simply defect. Somebody has to check that he has cause to seek political asylum. And we need some way to keep his colleagues in the Computer Center out of it, as they are all decent people as well as top scientists."
"Won't it cause a huge stink when he defects in any case?" I interjected. "Surely the Soviets will be livid?"
"The ambassador will protest in the strongest terms, but that will be all. It's a little game they play. Nobody really cares about defections, but there are niceties to be observed. No, I'm more worried about repercussions on his team in Novosibirsk. They've been so nice to us, it wouldn't do at all if they got punished as a result. So we must give the Soviets a convicing reason. But it's easy. We'll say he is a Shamanist and wants the freedom to practice his religion in the open."
"But," I protested, "Lexey is not religious at all!" 
Bernard showed me a wicked smile "That just proves how adept he had to be at hiding it." 
I gave in. This was a man not to be argued with; and he was after all my boss.
"But Nathan," he went on, uncharacteristically hesitant, "there is another issue. You and Alexey will want a life together, and I'm afraid that, even with the current relaxation of the regulations, the Corps would have to take a dim view of that."
How tactful of him. I was touched by his consideration, but it was my time to smile "You know me only too well, Bernard, but this time you've missed something. Have you ever wondered why I moved to Hawaii?"
To my great satisfaction, he was suitably nonplussed. "You will do me a great favour by accepting my voluntary resignation, as soon as Lexey has jumped."
Bernard stared at me with an unreadable expression. Then he smiled wistfully "I'll miss you both." 
With that he rose, signalling that the interview was over, and he perked up "We still have a lot to do. Let's find Alexey and get some coffee."

A few weeks later, the new CDC Cyber 175 super computer was delivered. It was a very powerful machine, but it had to go through a long process of testing and verification before it could be used for JCRE's simulations, so in the meanwhile, we had to make to with the old CDC 3600. Somewhat to my surprise, the same code that ran like lightning on the BESM-6 was slow as molasses on that machine. When we looked into it, it was clear that it was our very own convection kernel that was the problem. Lexey shrugged and said "It must have been that genius Dymnikov, he'll have have invented some radically new scheme again." Apparently, Dymnikov did this kind of thing all the time. We left it at that as we had more pressing concerns. The girls operating the Cyber had assured us that it really was working just fine, and they'd be happy to slip in some unofficial jobs if we wanted. They loved Lexey, charmer that he was.
So we devised what was ostensibly a standing-wave problem, a common way of testing the ability of numerical schemes to model wave dispersion.

In reality, we were designing surfboards. We were opening a surf shop and we'd have boards designed on a state-of-the-art super computer!
 
I had already bought a property in Ala Moana. I had talked to my friend Rob Burns, and we'd made a deal that he'd teach Lexey to shape boards, in return for us selling his boards alongside our own. Rob wanted to open his own shop soon too, in Kailua, so that way he'd have a presence on both sides of Oahu. 

As an in-joke, we'd decided to call our shop "Standing Wave". 
<!-- We liked the sound of it. -->

[[ This bit is logical but poor as ending ]]
[[    @silverstacks:
> @kagetsuko Like we discussed, I also asked him if he wanted authorship on the paper, and if he would agree to have the scheme named after him. He was so moved he actually cried. He insisted that Chinchuluun and Miller had to be co-authors, and he will provide the justifications. @lores @klimagalka
]]

## Epilogue

<!--[512]-->
As expected, the paper caused a storm, and not only in the climate modelling community: a carefully planned concerted news campaign of the five organisations involved made sure it made headlines in the international press for quite a while. `kagetsuko` went from anonymous postdoc to superstar scientist overnight. Even Lightman and Dymnikov gave interviews. It was after all an uplifting story, about an effort that was not only global but also spanned several generations. And the potential was very real: already, the improved predictions of extreme weather events have saved lives. In addition, the scheme improved the accuracy and resolution of global climate models as well, which helped to reduce the uncertainty on the predictions of global warming. This might seem rather futile, but it removed another arrow from the quiver of the deniers.

The published paper, "A novel scheme to improve the resolution of global circulation models with cloud feedback", had twelve authors, one of them posthumous. The first author was Natsuko Suzumiya, our `kagetsuko`. The final three authors were Lightman, Chinchuluun and Dymnikov. I was pleased to see my friend got full authorship as well. `klimagalka` got an acknowledgement by name, and `kagetsuko` had added "a heartfelt thank-you to her dear fedi friends, who made this paper possible." The algorithm had been officially named the "Suzumiya-Lightman-Dymnikov cloud feedback scheme".

While I'm writing this, reflecting on the paper and its story makes me very happy. But at the time of its release, I was too weighed down by my own problems to  share much in the joy of my friends. Looking back, my posts must have looked stilted and remote.  And yet they were incredibly understanding and supportive, those strangers I had encountered by chance and never met in person. They helped me through the hard days and nights, and asked nothing in return. In particular Natsuko and I would talk often, and it was the help of her and her partner Chihar that made my move possible. I ended up living close to Natsuko's place of work in Uji, and they visit often. 

Natsuko  never mentions her family but Chiharu introduced me to her mom. She's only a little younger than me but looks about sixty. She's an uncomplicated person with a sunny disposition and a delicious Kansa accent. I have a real struggle to keep up when she's talking but it's always a lot of fun. At our first encounter she cheerfully informed me that she'd divorced her abusive, _rokudenashiyarou_  husband twenty years ago and that she'd been very happy on her own. When she heard our story, she immediately suggested that the four of us should go to a surfing contest together. Chiharu and Natsuko chimed in with "yes, let's!" and -- somewhat to my surprise -- I found myself agreeing to join them. 

So we went all the way to the Junior Open Surfing Championshop at South Chikura Beach in Minamiboso, in Chiba prefecture. It was a fabulous weekend, gloriously sunny and warm, and sitting on that magnificent beach in their company, watching the waves rolling in from the Pacific and the exploits of the young surfers riding them, I felt profoundly at peace.
