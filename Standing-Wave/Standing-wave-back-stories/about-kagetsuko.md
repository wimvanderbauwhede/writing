On an otherwise unremarkable, average dull day in the late winter of 2027, kagetsuko posted something on her fediverse account that particularly intrigued me. 

She was one of those people I followed to have a nice balanced timeline, not one of my close circle but we were mutuals, regularly boosted and liked one another's posts and occasionally interacted. 
I had long forgotten why I'd started to follow her. I liked her handle because it referenced Aggretsuko, the irritable red panda from the eponymous anime; but it also could be read as ka-getsu-ko, meaning "full moon child"; and as kage tsūkō, which meant "shadow sailing". I suspected this was entirely intentional.
Her profile said "science/chocolate/coffee; languages: Japanese (native), English (fluent)". No pronouns. Avatar a minor character from the Yuyushiki manga. Ran her own single-user server so she had to be geeky. I gathered she was some kind of atmospheric scientist working on climate models. She'd done here PhD in Germany and then returned to Japan, but I had noticed a while ago she kept European hours. That was nothing remarkable, a lot of fedi people had non-standard wake/sleep cycles.

I was checking my timeline over coffee and I immediately noticed the post:

> Hi fedi, I am looking for some old FORTRAN codes that were developed in 1975 for the BESM-6 computer by a collaboration of scientists from the US and the USSR. It is a simulation code for a global circulation model. The scientists involved were Nathan Lightman and Bernard Miller at the University of Hawaii/NOAA Joint Climate Research Effort and Alexey Chinchuluun and Gury Marchuk and maybe also Valentin Dymnikov at the Computer Center in Novosibirsk. I've gone through all the declassified documents from the CIA  Electronic Reading Room, but I haven't been able to find the codes or any papers or reports about this research. I have emailed both institutions but I don't have any contacts there so I don't think I'll get anywhere. Any pointers welcome; please boost!

Now that was very interesting. I wanted to know more. I replied:

> @kagetsuko I might be able to help a little, I'm quite familiar with the BESM-6 and there are a few code repositories that I'm aware of. What is the context for your question?

Her answer came almost instantaneously:

> @lores I'm doing a postdoc on novel general circulation models to improve prediction of extreme weather events. The Soviets were the first to develop such simulation models, there is a seminal book by Marchuk and Valentin Dymnikov from 1984 but the work had already started in 1974. The problem is that to predict extreme events we need very high resolution, but we also need a global scale. On those old computers, they had to be very efficient, in particular with memory, so I thought I might get inspiration from those old codes. From the letters exchanged between Lightman and Chinchuluun, they were getting very promising results, but somehow they never published.

<!-- [[ need to work in the Ukraine-Russia peace treaty at some point, and the normalisation of external relations]]-->
I reflected that it was a good time to do this kind of historical research: the war between Ukraine and Russia was finally over, and international relations had been normalised quickly. A few years ago, it would have been impossible. But 1975 was a long time ago and pre-internet, so it wasn't going to be easy.

I stared out of the living room window towards the rainclouds over the grey North Sea, less than a kilometre down the gentle slope on which the house stood. A year ago, we were his by a hurricane-level storm, and unfortunately the MetOffice's predictions had underestimated it. It had ripped the roof off and scattered the contents, and what remained had been drenched by the relentless torrential downpour. We'd been homeless for months, and we were amongst the lucky ones: we hadn't been flooded, and we could rent temporary accommodation while the house was being repaired. With a better forecast, people would have had more time to prepare. It would have made a huge difference.  

> @kagetsuko I'll have a look at the CIA docs and the BESM-6 repos


<!-- [[Make the narrator muse on the need for better predictions: 
Maybe also contrast with a typhoon in Japan? ]] -->

[[ he narrator reads the docs. 
- Nathan had done his homework. He had read a lot of the Russian papers, I guess Kissinger must have insisted that the programme got support from translators.
He concludes that there is something very odd: with the resolution given in the final letter by Chinchuluun, and the approach described in the 1980 article by Marchuk and Dymnikov, it would seem that on the BESM-6, the simulation should take unpractically long. So there had to be some deep magic.

The problem in general is: if Dymnikov wrote this magic kernel, how come it is not in his article nor in his book? A possible explanation is politics: Dymnikov's publications are from after the end of Détente, in 1979. It was not acceptable to publish work that was a collaboration with US scientists. This would mean that Dymnikov's kernel was the core of an early US convection model for simulating cloud feedbacks. 

Another, minor political point is that, after the Kudirka Incident, there were new  guidelines for handling defections, to ensure that proper defectors were not sent back. 
"""
One of the principles embod ied in these guidelines is that all cases of would‐be defection be reported at once to policy making officials, who could then take steps to make sure that the person involved was a genuine defector seeking political asylum.
"""

One of the principles embod ied in these guidelines is that all cases of would‐be defection be reported at once to policy making officials, who could then take steps to make sure that the person involved was a gen uine defector seeking political asylum.
]]











