The character in this story is based on someone I know; the experiences are based on something similar that happened to me once; the shop assistant is a stereotype of the worse kind of British shop assistants.
----

Hanif was waiting patiently in the queue. He was happy with the suit he'd picked. Pal Zileri, beautifully cut, even though, by necessity, a bit conservative. He was a tall man in his early forties, with luxurious curly hair, a neatly trimmed beard and pair of large, stylish glasses. His skin was somewhere between olive and caffe latte.
When he came to the till, he recognised the look in the shop assistant's face: slight dislike, born from fear. He sighed internally but smiled, put the suit on the desk and presented his credit card. 

Judy had seen the man move along the queue. He looked pleasant enough but he clearly was a foreigner, some kind of Arab. His appearance was very casual, totally not like the expensive suit he was carrying. She decided to be watchful.
When he handed her his credit card, she looked closely. A bank she didn't know; issued to "H. Anderson". No way his name was Anderson. He must have stolen it, maybe even mugged somebody. And now he was trying to buy this expensive suit! 
She handed the card back with her best apologetic face and said "I'm sorry sir, this card does not work in this country."

Even if he wasn't surprised -- this was Brexit England after all -- Hanif felt insulted. However, he answered mildly "It was issued by a British bank and is current, what seems to be the problem?"  He could see the attendant was taken aback, but she rallied. "Can I see proof of ID please?"

Hanif smiled more broadly now, but those who knew him would have warned Judy to run. He took out his phone. "Proof of ID is not a requirement for card payments," he said levelly, "but here is my card". With a sleigt of hand he flipped a business card onto the suit and took a picture of the attendant's badge, making sure the card was in the frame. "I will take my custom elsewhere. You can expect my letter of complaint." And without further ado, leaving the suit on the desk, with the card on top, he walked away. 

Glancing at the  business card, Judy had the sinking feeling of someone who realised they had thorougly blown it. "Hanif Anderson, Senior Partner, Anderson & Quadri, Barristers and Solicitors -- 'law with a human face'". She felt the urge to run after him, but he had already disappeared, and there was a long queue.


