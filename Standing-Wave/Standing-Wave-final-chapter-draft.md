

There was a long message from silverstacks. She had gone to the surf shop and met with Nathan's grandniece. It turned out that Nathan was still alive and well, living in a nice serviced apartment in Makiki. They went to visit him together.  

@kagetsuko @lores @klimagalka We sat under the pergola on the terrace, with a magnificent view towards the ocean. I asked him what happened when Alexey visited. This is the story he told me. I've only embellished it a litte bit ^_^.

Apart from being a librarian and bookshop owner, silverstacks was clearly also a writer. This is Nathan' story.

## Chapter 6. Standing Wave

When I returned to Hawaii from Siberia in early October, I was at the same time deliriously happy and desperately lonely. Love is terrible. To think that only two months ago, I had no idea Lexey even existed, and now I couldn't imagine living without him. 
January had come and we'd had our first Kona storm of winter when finally the wait was over: he was here, we were back together!

We had made lots of plans, during that hot summer in Novosibirsk, and we were eager to get on with them. But there was still work to be done, and appearances had to be kept up. It wouldn't do if the project got wind of our real intentions before we had realised them. 

Or so I thought. But once again, I had underestimated Bernard. I should have known.

Lexey had been here a week, and we were making really good progress with the research work. To the rest of the team, we were just two scientists with a professional and productive working relationship. Bernard got on great with Alexey, and I was glad for that, although truth be told, I would feel a slight pang of jealousy whenever Lexey laughed at one of his jokes, or Bernard payed him some perfectly innocuous compliment. 
That morning, when I walked past the open door of his office, Bernard hailed me to come in. 

"Shut the door, please, Nathan," he said. "Have a seat". He motioned me towards one of the easy chairs he had in his office, and he sat down in the other one. He looked at me thoughtfully.
Whether it was because of his Dutch mother or his Belgian upbringing I don't know, but Bernard had a habit of coming straight to the point. 
He looked me straight in the eye and said "I think Alexey will want to stay here after his visit, don't you?" 
I was dumbfounded. How could he have found out? Did he have us watched? The game was up!
"Don't look so shocked!" he laughed. "If you had wanted to keep it a secret, you should have started with being less glowingly happy when you returned from Novosibirsk." 
Once again I realised, belatedly, that this man knew me almost better than I knew myself.
He continued "And when Alexey got of the plane, you flew straight into his arms!" 
I started to protest, but he held up a placatory hand. "All right, all right, you didn't. But it was plain to see that you would have wanted to. And the way you look at one another when you think nobody's watching."
"Everybody knows, then?" My voiced betrayed me, but Bernard smiled sympatethically. 
"Of course not. Nobody but me. The others tend to be a lot less observant, it simply wouldn't occur to them." 
I was somewhat relieved, but not for the first time, I wondered about Bernard.
"Now," he said, once again with alarming directness, "what we need is a convincing story for Alexey. With the new guidance, he can't simply defect. Somebody has to check that he has cause to seek political asylum. And we need some way to keep his colleagues in the Computer Center out of it, as they are all decent people as well as top scientists."
"Won't it cause a huge stink when he defects in any case?" I interjected. "Surely the Soviets will be livid?"
"The ambassador will protest in the strongest terms, but that will be all. It's a little game they play. Nobody really cares about defections, but there are niceties to be observed. No, I'm more worried about repercussions on his team in Novosibirsk. So we must give them a convicing reason. But it's easy. We'll say he is a Shamanist and wants the freedom to practice his religion in the open."
"But," I protested, "Lexey is not religious at all!" 
Bernard showed a wicked smile "That just proves how adept he had to be at hiding it." 
I gave in. This man was not to be argued with; and he was after all my boss.
"But Nathan," he went on, uncharacteristically hesitant, "there is another issue. You and Alexey will want a life together, and I'm afraid that, even with the current relaxation of the regulations, the Corps would have to take a dim view of that."
I was touched by his consideration, but it was my time to smile "You know me only too well, Bernard, but this time you've missed something. Have you ever wondered why I moved to Hawaii?"
To my great satisfaction, he was suitably nonplussed. "You will do me a great favour by accepting my voluntary resignation, as soon as Lexey has jumped."
Bernard stared at me with an unreadable expression. Then he smiled wistfully "I'll miss you both." 
With that he rose, signalling that the interview was over, and he perked up "We still have a lot to do. Let's find Alexey and get some coffee."

A few weeks later, the new CDC Cyber 175 super computer was delivered. It was a very powerful machine, but it had to go through a long process of testing and verification before it could be used for JCRE's simulations, so in the meanwhile, we had to make to with the old CDC 3600. Somewhat to my surprise, the same code that was so fast on the BESM-6 was slow as molasses. When we looked into it, it was clear that it was our very own convection kernel that was the problem. Lexey shrugged and said "It must have been that genius Dymnikov, he'll have have invented some radically new scheme again." Apparently, Dymnikov did this kind of thing all the time. We left it at that as we had more pressing concerns. The girls operating the Cyber had assured us that it really was working just fine, and that we could run some unofficial jobs if we wanted. So we devised what was ostensibly a standing-wave problem, a common way of testing the ability of numerical schemes to model wave dispersion.

In reality, we were designing surfboards. We were opening a surf shop and we'd have boards designed on a state-of-the-art super computer!

I had already bought a property in Ala Moana. I had talked to my friend Rob Burns, and we'd made a deal that he'd teach Lexey to shape boards, in return for us selling his boards alongside our own. Rob wanted to open his own shop soon too, in Kailua, so that way he'd have a presence on both sides of Oahu. 

As an in-joke, we'd decided to call our shop "Standing Wave". It sounded good.






