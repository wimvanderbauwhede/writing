# Possible titles:

"Journey to Schwarzkollm"

"The Krabat illustrations"

# Structure

## Introduction

This is the story of how long ago, a book and  a drawing project led to  a journey to a faraway land.

## The Book

The book at the centre of this story is in Dutch called "Meester van de zwarte molen (Krabat)", which means "Master of the black mill". It was written in 1971 by the  German author Otfried Preußler. The book is based on an old  Sorbian legend
about the sorcerer Krabat. It is set in the Lausitz area of Germany,  in particular the ancient village of Schwarzkollm where Krabat is said to have learned his magic powers.  The book is set in an around Schwarzkollm during the  Great Northern War (Große Nordische Krieg, 1700-1721) and focuses on Krabat's youth as an apprentice (Mühlknappe) in the Black Mill in the Kosel moor (Koselbruch), where he learns the arts of both miller and sorcerer. It is a story about magic, good and evil, power and love. What appealed to me in particular is the realism of the setting, and the importance put on rituals, both magical and religious.

## The Project

I don't remember when I first read the book, it must have been some time in the second half of the 1980s. But it made a strong impression on me, and I re-read it many, many times. In the meanwhile I had taken up drawing, and I thought it would be an interesting project to create a series of illustrations of my favourite novel.

Then one day I discovered Schwarzkollm in an atlas, and realised it was a real place, and that I maybe could even go there. So the plan was born for a journey to Schwarzkollm, to do research for my illustrations.

## The Reality

However, I had to postpone this plan a bit for various reasons, the most important one being that Schwarzkollm was in Eastern Germany, which was behind the Iron Curtain until 1990.

This is now old history, but a brief summary will help with the rest of the story. After the Second World War, Germany was partitioned in West Germany (Bundesrepublik Deutschland) and East Germany (Deutsche Demokratische Republik, DDR). As East Germany was part of the Eastern Bloc, a fortified frontier (with actual fences, walls, minefields, and watchtowers) was erected between the two countries, called the "Iron Curtain". Travel across this border was severely restricted, it was unlikely that I would have been allowed to travel to Schwarzkollm on my own.  The DDR was a socialist state under strong influence of the Soviet Union, and a poor country.  

But in 1990 the Iron Curtain disappeared and East and West Germany were reunited. At that time, I was too busy studying to pursue my plan. It wasn't until 1994 that I was in a position to realise my plan.

But 1994 was very different from now.  In 1994, the Internet was the next big thing in technology.  TIME magazine did a cover story on it, but it was so unfamiliar that they had to begin by explaining what it was: “the world’s largest computer network and the nearest thing to a working prototype of the information superhighway”. Effectively, this meant I could not use the internet to establish contact with anyone in Schwarzkollm. And as I did not know any names, I could not telephone either.  That would have been difficult anyway as I didn't speak German.

But I took a postal distance learning course in German, and managed to write a letter to the mayor of Schwarzkollm, explaining that I would like to visit the village for a week to do research with an illustration project about Krabat. This was in November of 1994.
In January 1995, against my expectations (if not my hopes), a reply arrived! The Bürgermeisterin of Schwarzkollm wrote to say that she welcomed me to the village and would arrange for me to stay with someone on the village.
Some time later, another letter arrived from  a  certain Herr Blazejczyk, who explained that his family lived in a large new-built house and that it would be a pleasure to let me and my partner stay with them for a week in May 1995, free of charge. They would also meet us at the railway station, if I could write them back once I new the arrival time of the train.

## The Journey

Because naturally, the way to travel to Schwarzkollm from my home in Belgium, about a thousand kilometres, was by train. Air travel was much less common in those days, and I we were already ecologically aware. But again, there was no internet, so booking tickets was done at a dedicated ticket window for international travel. Where they explained to us that yes, they could sell us a ticket, but unfortunately not all the way: it was not possible to get a ticket for the part of the journey that was in ex-East Germany. But instead they provided us with a letter of transit signed and stamped by a high-ranking official of the National Railway Company of Belgium.

And it worked! Whenever we showed this letter, the train guard read it, gave it back to us with a mystified look, and left us alone. I don't remember the details, but we must have left our departure station of Gent Sint-Pieters very early, because when we finally arrived, it was still light. We changed onto the Germain railways at Aachen, and then a few times more. I remember we changed onto the final, local train in a virtually deserted, almost derelict station, which might have been Falkenberg/Elster. I also remember that train: a Spartan, brown-liveried ex-DDR train.

When we arrived at Schwarzkollm station, Herr Blazejczyk and Frau Winzer were both waiting for us on the platform. The first thing they asked was "Was the train punctual?"

As indeed it was, every leg of the journey had been perfectly on time.  

## The Stay

Herr Blazejczyk took us to his house where we met his wife, Frau Blazejczyk. He was the headmaster of the high school in Hoyerswerda and she was an artist and art teacher. They had two small children.  

Both Herr and Frau Blazejczyk spoke German and English, which was a relief as our German was still very poor. It was also at that time a rare thing: the second language that most people in East Germany learned at school was of course Russian. English or French were optional third languages.

We had very interesting discussion with them. I remember specifically a discussion about  Documenta IX. Documenta is an exhibition of contemporary art which takes place every five years in Kassel. The most recent one at that time, Documenta IX, took place in 1992 and was curated by a Flemish artistic director, Jan Hoet, who was the founder and director of  the Municipal Museum for Contemporary Art in my home town Ghent.  We also talked about the how it was under Communism and about the effects of the reunification. And with Herr Blazejczyk in particular, I talked about beer. We had brought some particularly strong Belgian beers as a gift, and they were in awe at their strength ("Aber das kan man doch nicht trinken!").

Schwarzkollm is a small village, about five hundred people at that time, essentially a single main street. There was a watermill, which has in the meanwhile been rebuilt an is now a major tourist attraction. In 2005 the book was turned into a successful film and that made has led to a huge increase to the village. But when we were there, it was old and not well maintained, and very much what I was looking for as inspiration.

It was amazing to be in this very place where to book was set. Because it was such a small place, it was easy to get around. We also had the use of the bicycles to explore the surrounding area, in particular the Kosel moor. One of those visits took us to a pottery where the potter (Töpfer) showed us his art and we bought two simple mugs with the traditional peacock's eye decoration which is typical for that region. We still call them the "Töpferpotjes" (potje is small mug in Dutch).  
http://hegewald-toepferei.de/

My main purpose was of course to collect materials for my illustrations. By coincidence, the year before Schwarzkollm had celebrated its 600th anniversary.  As a result, a lot of promotion material and a special commemorative book had been published, so I got a wealth of printed material, historical information and pictures.

Still, the most effective way is to make sketches so I walked around with my drawing materials and made lots of sketches. Luckily the weather was perfect.

[[
I suppose this will be the longest part, but make sure it is interest to the reader
The people,
Frau Winzer, die Bürgermeisterin
Herr  und Frau Blazejczyk, our hosts
Frau Unger, our guide
Andreas Hegewald, the Topfer
Sometimes Frau Winzer or her friend Frau Unger would guide us.

the village,
What it looks like, the houses, the church, the landscape

collecting information and sketching
Anecdotes:
- Documenta IX https://www.documenta.de/en/retrospective/documenta_ix
- Bier, Kopfschmerzen, das kan man doch nicht trinken
- Visits to Hoyerswerda and Dresden
- Wie eine Rüssenkazerne
- How it was when it was the DDR
- Russian as a second language
]]
[[
The question is how to include the sketches: as part of the Stay or as part of the Work?

]]
## The Work

I had already made a selection of scenes of the book I wanted to illustrate, so most of the sketches were made with these in mind. On my return I made a final selection of nine illustrations. In the end it probably does not seem like a lot of research went into each drawing, but  I know I could never have made the illustrations the way they are without the visit.

* The first illustration is of the scene when Krabat arrives at the Black Mill, to which he is called by a voice in his dreams. It is in the days between New Year and Epiphany.

  Like a blind man in a fog, for a while Krabat groped his way through the forest by touch. Then he suddenly found himself in a clearing. At the same time, the moon broke through and suddenly cast everything in a cold light.  
  There stood the mill. Dark and threatening it lay in the snow, like a strong, dangerous animal eyeing its prey.

* The second one is the scene where Krabat has gone to sleep, that same night, after the miller has accepted him as an apprentice. He is woken up in the middle of the night, and the apprentices are standing all around him in their nightshirts:

  The attic reverberated to the stomping and rumbling of the mill. Just as well that Krabat was dog tired: he fell asleep like a log as soon as he lay down -- and he slept until a ray of light woke him.
  Krabat sat up and froze with fright. Eleven white appearances stood at his bed and looked down on him; eleven white figures with white hands and white faces.
  "Who are you?" he asked fearfully.
  "We are what you will become," said one of the ghosts. "But we won't do anything to you," added a second one. "We're just miller's apprentices."

* Then there is the scene when the man with the cockerel's feather comes and the apprentices have to work all night.

  Was the mill then really on fire?
  Krabat was suddenly wide awake and pulled open the window. He leaned out and saw in the millyard a heavily loaded flat cart, with a sturdy cover, blackened by rain, with a team of six pitch black horses. Someone sat on the XXX, with his collar up and his hat drawn low over his face; he was also entirely clad in black. Only the cockerel bright red and waved in the wind; it seemed like a tongue of fire, waxing fiercely and then shrinking, as if on the verge of going out. The gleam was so strong that it bathed the whole yard in a flickering light.
  The apprentices ran back and forth between the mill and the cart; the offloaded sacks which they hauled to the mill room and returned for more. Everything happened in dead silence but with a feverish haste.

* What I considered a key scene is when Tonda gives his special knife to Krabat:

  The 'barren plane' was a square clearing, barely larger than a threshing floor, surrounded by a few stunted pines. In the gloaming the boy made out a row of low, long flat humps. They seemed like graves in an abandoned graveyard, overgrown with heather and unkempt -- no cross or stone to be seen -- whose graves could that be?
  Tonda halted. "Take it then," he said and held out the knife to Krabat; the boy understood that he could not refuse it.
  "It has a peculiar property," Tonda said, "that you should be aware of: should you ever be in danger -- serious danger -- then the blade will turn black as soon as you flick it open."
  "Black?" Krabat asked.
  "Yes," Tonda said, "Black, as if you'd held it over a candle flame."

* The wake at the cross outside the village, on Easter night, is my favourite scene in the book. It is the will of the Master that the apprentices spend the Easter night outdoors, in pairs, on a spot where someone had died a violent death. In his first year at the mill, Krabat had paired with Tonda, who had taken them to a place with a tall, rough wooden cross, some distance from the village. In his second year, Krabat goes to the same spot with Juro.

  Krabat wrapped himself in a blanket and went to sit under the cross. Just as Tonda had sat there a  year ago, he sat there now: with a straight back, his knees drawn in, leaning back against the cross.

  What I like about the scene is the description of the church bells heralding Easter (because on the day before Easter they are silent), and the girls' choir starting to sing.

* Then there is the scene when Lobosch comes running into the millroom in a panic. Krabat and Staschko are dressing the millstones, an elaborate process that requires dismantling a considerable part of the machinery.

  Just when the were about to detach the curb so they could access the stones, the door flew open and Lobosch burst in,  pale as death, his eyes wide with fear.
  He shouted and wheeled his arms around and seemed to repeat the same thing over and over. The boys only understood what he said when Hanzo stopped the millworks, the mill fell silent and Lobosch could be understood.
  "He has hung himself!" he shouted. "Merten, -- has hung himself, in the barn! Do come quickly!"

* And the scene of the village fair, where Krabat goes and dances with all the girls without choosing, including with the Kantorka, taking care not to give their relation away. Nobody suspects anything, not even the old woman with the eye patch, who he hadn't noticed before. I also liked this scene because it allowed me to draw the local architecture and the dancer's traditional dresses.

* The most dramatic scene of the book is of course when the Kantorka has to recognize Krabat blindfolded.

  From his pocket the Master took a black piece of cloth and blindfolded the girl with it. The he took her into the room.
  Krabat started, he had not counted on this. How could he help her now? That way the ring was useless!
  The girl walked up and down the row of boys, once, twice.
  Krabat could barely keep on his legs. His life was finished, he thought. And the life of the Kantorka too.  He was overwhelmed with fear -- such a strong fear as he had never felt before. "It's my fault that she must die," went through his head, "My fault ..."
  Then it happened.
  The Kantorka had walk the row up and down for the third time and now she pointed at Krabat.
  "It is he," she said.
  "Are you sure?"
  "Yes!"
  It was over. She took of the blindfold and went to Krabat: "You are free."

* Finally, Krabat and the Kantorka leave the mill, which will burn down that night

  "Let's go, Krabat."
  He let her lead him along, out of the mill and throug the Kosel moor to Schwarzkollm.
  "How dit you manage to recognise me between the other boys?" he asked when the firsts lights of the village appeared between the trees.
  "I felt your fear," she said. "You were afraid for me, -- and that's how I recognised you."
  While they walked towards the houses it started to snow, with light, fine flakes, like meal from a large seeve descending on them.
