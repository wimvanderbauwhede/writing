# The Dunes of Ard Dìthreabh

## Chapter 1. Winter, Muir

<!-- 1077 words

[[2070
- what happened in 2054 to 2056 
What we learn: the family; that dad is in jail; that the concrete catastrophe started in 2054; that all this was long ago and that he still misses his dad. Also, that he is in Wissant. And that he now has a daughter of 12. The beach description should probably go in the middle, and something (wind, sea, a bird, a ship, the cliffs of Dover) should trigger him to think about dad who left. 
At the end of this chapter, we know about most of the family and that dad was in jail and then left, but we don't know why]] -->

It must have been in the fourth year of dad's prison sentence that the concrete started to crumble. Yes, thought Muir, 2054, that figured: it was the year he had started at the University, studying History. He hadn't liked it much, in fact he was at that point seriously regretting it. He remembered lying awake that night, fretting over his position in the class, how everyone seemed to ignore him. In particular none of the female students would give him the time of day. Muir smiled, thinking of Hélène. How angsty he'd been then. 

The Erskine Bridge had been the first to fall. There had been this awful rending, rumbling sound, accompanied by what sounded like huge crashing waves, and the house literally shook on its foundations. Soon after, sirens started wailing through the night. The dreadful noise had woken Papa and Suna as well, and as it sounded really close, they'd quickly gotten dressed and went down the hill for a look. Even in the starless night it was clear to see what had happened: a part of the bridge deck had come down into the street and crushed several buildings. Luckily, all those buildings had long been abandoned because of the flooding risk. 

In the morning, the full extent of the disaster became clear. The entire bridge had collapsed and fallen into the Clyde. It was what is technically called a catastrophic process, which means there is a sudden transition that can't be predicted. So nobody saw it coming. It was a global catastrophe in the common sense as well: not long after, everywhere, bridges started to fall, buildings collapsed and airports turned to sand. Initially, it was written off as a freak incident: the bridge was old after all, and it must have suffered from some form of cracks or fatigue that had escaped the inspections. A detailed investigation revealed nothing that could explain it. The next bridge to fall was a few months later, somewhere in the Alps. Another few months and then two bridges fell, one in Brazil and the other in China. Some people started to wonder if it could still be coincidence. They didn't need wonder for very long, as the pace picked up and several more bridges collapsed within weeks of one another. Then tall concrete buildings started to crumble as well, and other concrete structures, until not a day went by when there wasn't any news item about the Global Concrete Collapse. Less dramatic but much more far-reaching, the concrete runways and aprons of airports all over the world turned to sand. As had the Glasgow pavements, Muir thought with a grim smile. The parallel had amused him at the time. 

He wondered if all that sand was what had made him end up here, to live within a bowshot of the great beach of Wissant, which Hélène had told him meant "white sand" in Flemish. But he knew it had really been Dad's leaving. His second betrayal.

It was a truly magnificent beach, and the rise in sea level had not affected its beauty. Even on a bleak winter day like this, with a biting easterly wind pushing the clouds over the Channel towards England, it had an austere appeal. Just being able to walk on this beach every day made living here worthwhile. The enormous expanse of sand at low tide, bracketed between the two promontories of Cap Gris Nez and Cap Blanc Nez, the huge dunes -- he never tired of it. The dunes were even taller than those of Ard Dìthreabh, and as a kid he thought those were the tallest in the world. They seemed to have gotten smaller as he got older, Muir thought wrily.

On a clear day, when the fancy took him he would climb up to the top and gaze at the cliffs of Dover. But he loved just sitting on the beach and watching the huge rigid-sailed robot freighters make their way through the Channel towards the ports of the Low Lands. 

Not today though, it was too cold to sit still for long. A rare cold day, one to savour. Also, this morning there had been only an old lonely sail boat, a square rigger, and it had made him think of Dad. That was the flipside of the beach, so many things reminded him of Dad. He walked on briskly, watching his breath form surprisingly dense puffs of mist, like small clouds, and ruminating on the past. 

Diederik Vandewoesteyne had committed a crime, confessed to it, and gone to jail. It sounded so simple. That had been in 2050, when Muir had been 15. He got out five years later, and Muir and Suna had thought that life would return to something like the old normal, but it wasn't to be. Of course, it would never be the same, too much had changed. For one thing, neither of them where teenagers anymore. For another, Papa was now Mama. That had not come as a surprise to Dad, but it had still been a big change for all of them. 

Well, for a short while, the four of them had lived together, studiously avoiding the past, and things had been cozy. But then, out of the blue, Dad had announced he was going away on a long trip. Muir had been heartbroken, and filled with resentment. He'd missed his dad so much for all those years, and now he would be away for even longer. He felt cheated. It seemed to him that Dad was being very selfish. But he could see that Mama was fully behind it, and from the way they behaved it was clear Dad wasn't doing this for fun. And that they didn't want to tell him or Suna anything meant they were better off now knowing. That was a bit sinister; but then Muir had never been able to shake off the feeling that there was a lot more to the affair. For one thing, dad simply couldn't have committed that crime. They had all known that. But he had confessed and went to jail. Too simple.

Muir tried to play the grown, to act tough and not show his feelings, but of course Dad had guessed it all anyway. He'd looked at Muir silently for a long time, then hugged him for even longer, and said he'd keep in touch. And he had. He would be back in two years now. Another eternity.

## Chapter 2. Spring, Suna

<!-- 2000 words already

[[2070
- starts with a lyrical description of the beach => OK
- the state of the world:  the consequences of the concrete collapse => OK
- the arrest => OK
-  Suna's doubts, => OK
- papa's transition, maybe this links to Morgana => later?
- Suna's job 
- Should she have a dog? A border collie of course.
-  Bàrr Fhasaidh
]]
-->

Suna paused at the foot of the embankment to take in the sight.

At low tide the beach of Ard Dìthreabh was always looking its best, even on a bad day. But today, on this glorious spring afternoon, the light pale and so fresh it was almost sparkling, it was truly stunning: an uninterrupted stretch of sand as far as the eye could see, hundreds of metres wide, tall sand dunes on one side, the surf on the other, running in a shallow curve along the bay towards distant An Truthail. Sand dervishes seemed to form out of nothing, did their whirling dance and dissolved again into the beach. The sea surface was a hardly ruffled steel blue on which Eilean Arainn sailed  at anchor, majestic with its tall snow-capped mountains. Suna had seen this sight hundreds of times but it still took her breath away. [[ This is OK but I need to make the sequence clear somehow ]]

As soon as she had gotten up she'd seen that it was one of the first truly nice mornings of spring, and on a whim she'd decided to give herself the day off. Roy had had a late shift and was still asleep. After breakfast she made a few quick _onigiri_ , left him a note and set out along the beach. Instead of her usual morning run or walk, today she would go all the way to Ard Dìthreabh. The tide looked just right.

Seals were basking on the rocks that were all that was left of the old Irbhinn harbour pier. Suna climbed halfway up the nearest dune and ate her lunch. Out here in the open food always tasted as good as anything but she still felt herself wishing for  a simple slice of homebaked bread. Maybe one day.

From her vantage point, she could see the remains of the old Inventors Bridge sticking like bleached bones out of the sand amongst a sea of sand dunes.
She remembered how once, as a child, she'd tried to get to it. She'd sunk knee-deep into the loose sand but kept wading. It was a strange sensation: the top layer of the sand was hot from the sun, but at her toes it was almost cold. With every high step it repeated: hot, then cold, hot again, cold again. Up close you could still read some of the names punched in the ironwork: Maxwell, Kelvin. She suspected that memory might have been why, much later,  she'd decided to study Physics.

Once, most of this had been water, the saltmarsh and mudflats at the confluence of the Garnock and Irvine rivers. Hundreds of years ago it had been the main port for Glasgow. Yaya had told her that in the late 2020s, the storm surge of a super-hurricane had breached the coast line at Stevenston, and the enormous amount of rain it had dropped had pushed the Garnock and Irvine rivers through that breach. With the more arid climate and rising sea level, the old river courses had first turned into sand flats and later into dunes. It looked like a small desert, and that was appropriate for a place called Ard Dìthreabh, which meant wilderness in Gaelic.

Her steps traced a cycloid on the firm wet sand along the water, towards what in her mind was "Ard Dìthreabh proper", near the river mouth. A little game, walking along an imaginary periodic curve. This was one of her favourites, she loved the twirling phase.

That place always sparked memories, and today was no exception. They had come here so often, it was Dad's favourite place. Dad had been away for a long time now, making his slow way into the East, and before that he'd been in prison. And before that … she recalled feeling a kind of unease as a teenager, beyond the usual unease of that awkward age. There had been lots of subliminal signals from Dad, Papa and even Yaya. A kind of premonition. And then, in the pit of a dark and moonless night in October of 2050, there had been the raid. Papa had been away in Inbhir Èireann caring for Yayo with Yaya, who still insisted on living off grid. 

In that colourless hour before dawn, Dad had woken her and Muir and said, "Kids, the cops are coming to arrest me. Don't worry, they won't hurt you. Sow them that you know your rights. It's me they want. I'd better go down before they break down the door." And with that he'd flipped on the lights, gone down the stairs and carefully opened the door, and from the landing they'd watched a number of heavily armoured police pouring into the hallway and pinning Dad to the floor. Suna in particular remembered how they'd shone their  ultra-bright torches in her face, forcing her to shield their eyes, even though all the lights were on. And how they'd taped her mouths shut before she'd even had a change to object.

Suna involuntarily winced at the memory, and for while focused on the beach, marveling as always at the sheer number of shells, in some places so dense they painted the dark sand a bright white. If the populations remained sustainable, the beach would get whiter and whiter. The thought appealed to her. It made her think of the _hoshizuna_, the sand on some beaches of the Ryukyu islands that was made up of countless tiny star-shaped skeletons of single-celled organisms. Star sand, what a beautiful word.

It was Yaya who had taught them their rights. She remembered the bad times and Suna knew she had been permanently scarred by them, although she didn't know what had happened. They never talked about it. Yaya had drilled it into them: "Kids, in Scotland we still have laws and the cops still mostly follow them, but only if you show you know your rights. If they arrest you, the law says they must inform your parents that you're in police custody, even if you don't ask them to. And they can't interview your without one of your parents or close family present. But you must ask them, else they will just assign you some social worker. If Seumas or Diederik can't be there, you should ask for me or Yayo. Until one of us is there and there is a solicitor, you should say nothing at all."

Suna had recalled that lesson. The cops had started asking questions as soon as they got to the police station, but all she told them was to get Papa or Yaya. She hoped Muir would do the same. They had locked her in a windowless room with a chair and a table and the kind of very bright lighting thats still manage to look dim, and left her alone. A few times someone brought some food and drink. Shortlty after the third meal, they took her to a similar, somewhat larger room where there were several cops, but also Muir and Yaya and  a woman who introduced herself as their solicitor.

They'd asked both of them some questions which had seemed purely pro forma and let them go. Back home, Yaya told them the local cops had come to her house in Inbhir Èireann early in the morning and arrested Papa. "Then they came back and told me the presence of me or Yayo was required in Glasgow because they wanted to interview both of you. I inferred that meant they'd arrested Diederik as well. I kicked a fuss because I couldn't leave Yayo alone, so they had  to send over a nurse to look after him. When I finally got to the police station, the solicitor informed me that Diederik had already made a full confession. So they only asked you a few things to check and let you go." They'd let Papa go soon after as well. 

As so often when she was absorbed in such recollections, the river embankment appeared much sooner than she'd expected. She climbed onto it and stood for while looking at the dike that kept the river mouth in place. It had been constructed nearly half a century ago by a Belgian company and was made entirely of lime-treated soil. No concrete at all, and it had proven entirely erosion resistant. This approach was now common, but this dike had been the first of its kind. She turned and started back towards Bàrr Fhasaidh.

The trial had been a strange affair. 

Dad had at the time been up to something, Suna had been sure of that. But she still had no idea what it had been. But whatever it was, it couldn't be what dad had confessed to, even though apparently there was uncontrovertible evidence, and IPv6 address trail leading right to our house. But the IT wizard in the house was Papa, not Dad. Then again, even Dad would never have been so careless to leave a trail like that. Papa would'n have let him.

But papa must have had a watertight alibi, they didn't even charge him. Apparently, this was about some cyber crime from before Muir and Suna were born. But the police had all the records, and it was beyond doubt that at the time of the events, Papa had been with Yaya and Yayo, firmly off-grid. How the police could be so sure was a mystery, but Yaya had told them that in those bad days, surveillance was at its peak. 

Still, there was something wrong about the whole thing. Dad was no elite hacker, the best he could do was some scripts for his DNA sequencer. He could never have pulled of such a thing. Papa could have done it, especially with Yaya's help. But that was apparently not even on the cards. It was all very odd. 

After a brief trial that was mostly held behind closed doors, Dad was sentenced to 10 years in prison, but the solicitor assured us that he'd have to serv e five years at most.

[[ Suna sighed and shook her head. She had been over this much too often. Today was too beautiful a day for all that. She put it out of her head and thought instead of XXX also some more along-the-beach description but keep the gannets for the summer. ]]

[[ Maybe this could come later? Not sure, it fits here but it is rather a lot ]]
That was almost twenty years ago, and a lot had happened in the meanwhile. The Concrete Rot had changed everything. Within a span of barely five years, every major concrete structure in the world and countless others had collapsed or crumbled. Any concrete construction still standing was deemed unsafe. The consequences had been devastating: long-distance transport became impossible, even shipping was severly affected. Most concrete wind towers had also collapsed. Luckily, the high-density concrete of nuclear power plants remained unaffected, so there had been no disasters, but all plants had been shut down because of safety concerns. 

The main damage had occurred in the early years, when it was not clear, or rather not widely accepted, that there was a common cause to the events. But scientists, civil engineers and actuaries had caught on really early. Even in the first year, when only a handful of bridges had collapsed, it was clear to them that the odds were enormously against it being coincidence. Soon, a fungus was identified as the root cause. The spores were windblown, but the main vector of the rapid global spread had been via the tyres of airplanes. 

The airline industry tried hard to deny this, until their runways had started to turn to sand. It had been their worst blow ever. There was no slack in global air travel, even a single runway out of action at a major airport had worldwide repercussions. And it had been infinitely worse: the majority of runways became unusable. The irony was that a lot of these had been converted from asphalt to concrete in the previous decade, mostly because concrete had become cheaper but also because of the better heat resistance. And so, one airline after another, and one airport after another, became insolvent. Nationalisation could not possible save them all, especially as they could now only operate at a loss. There were technical solutions, like polymer-glass tarmac, but the airports could not afford them. Only the few that had been saved would eventually get back in operation. 

Long-haul transport had been a similar story, mostly for the same reasons: motorways had increasingly been surfaced in concrete. At first the vested interests had tried to downplay  the damage to the bridges so the lorries could keep on running. Until too many bridges had collapsed, and too much of the motorway surface had turned to sand. The remains of countless lorries that had fallen victim to this refusal to accept reality were still there today. 

The railways had been the first to recover. In particular in Britain the damage was relatively minor because many of the bridges were so old that they didn't use modern concrete. The main damage had been concrete sleepers turning to sand, but these could be replaced by metal or even wood. As concrete was no longer an option and tarmac was too expensive, transport moved soon to rail. Initially, there had been reluctance to accept this state of affairs: people kept hoping for some kind of miracle that would restore the roads quickly and cheaply. But after a few years, common sense had won and new railways were built on the remains of the main motorways. 

[[Small-scale shipping also experienced a renaissance.]]


[[Mostly because the internet was still there, so people had quickly organised and planned and created alternative, small-scale supply chains. ]]







## Chapter 3. Summer, Morgana

[[2070
- starts with Morgana thinking about liking summer and being/feeling very old. 
]]

Morgana wondered what she'd look like to someone much younger if they saw her here. A frail, wizened but proudly upright figure, old but not bent by age. Not that she felt particularly old today. It was high summer in the sunniest part of Scotland, there wasn't a cloud in the sky and hardly any wind. The late afternoon sun was still high in the sky, but the UV was moderate for someone with her skin tone. 

Her skin tone. It seemed to have gotten darker over the years, despite living so far up north. She remembered how her Rioplatense mother called it _parda_, and she'd liked the sound of the word. Her Playero dad had promptly nicknamed her "Morgana la gatoparda", and she'd loved that. She could imagine being a leopardess. 

Diarmad had only been a shade darker than the typical Scottish pallor, but somehow Seumas had inherited more genes from her father, and for most of her life he'd been a shade darker than her. It had been tough for him, especially as a kid. Kids could be so cruel. 

Even now, after more than fifty years living in Scotland, people would occasionally ask her where she was from. For Seumas that had been worse because she was after all an immigrant, but he had only Scotland to call home. The only solace was that even Diarmid-of-the-fair-skin got asked the question as soon as he opened his mouth. His years in Corisco had left him with a slight but still pronounced Spanish accent.

[[

2020s: global rise of authoritarianism, Ecco's Ur-Fascism https://archive.org/stream/umberto-eco-ur-fascism/umberto-eco-ur-fascism_djvu.txt
Might be interesting to discuss Ecco's actual views.
My take is that it was a reaction of rooted in fear and denial. By voting for the fascists, climate change could be denied or ignored. 
https://www.psu.edu/news/health-and-human-development/story/climate-driven-extreme-heat-may-make-parts-earth-too-hot-humans
It also worked as long as the climate refugees came from outside. So say that ]]

In the early 2030s, cracks were starting to show, ironically mostly because of climate change. Here it had been the coastal flooding and the first signs of the AMOC collapse, which had happened more precipitous than the models predicted. Then almost overnight the Global Fascism Collapse had happened. [[2034: the collapse, and]] Morgana flattered herself that she had played a small role in that. In any case, there was a mostly-peaceful global revolution and  for a while things were looking up a little. More progressive governments finally took long overdue drastic climate action. But it didn't last. 
[[2050: the tide turns again.]] It hadn't lasted for long, certainly not long enough. Climate change had started to bite and the people blamed their governments. Always quick to capitalise on dissatisfaction, the fash staged a comeback. 

[[We ]] had long discussions in the movement and within our family circle. Morgana had known Diederik planned to take some drastic action, but she couldn't see what he could achieve. She didn't ask. If he didn't tell, it was because we were better off not knowing. She knew Seumas knew, but he also remained silent. And now that she knew, She'd rather not have known. [[ Not sure this is OK. To speed things up, the spores would have to be released at many airports, that would take people to do it, so maybe Morgana should have been in the know ]]

Anyway, the fascists had a rather short run of it this time: they came to power again in 2054(5?), just after Diederik was released. But they never made it to the next elections, and they were too weak to do a total power grab. The Concrete Rot was put a stop to all that, because the blame was laid squarely on their shoulders. It had all happened on their watch and they could blame nobody else. There were of course rumours that the fungus was a genetically engineered weapon, but as it affected the whole world, that was so patently absurd that no amount of propaganda or misinformation could make it stick. And so fascists regimes collapsed with the concrete, and we have peace again.

[[ Does this make sense? By 2060, the concrete has crumbled everywhere. So the fash in Britain should be out of power then. So why does Diederik have to stay away for 10 years more? ]]


It should be clear that Morgana is a very engaged person. Here we can have a pan on the beach at the end and the detail of Findhorn ( Inbhir Èireann) on stilts. And something about how it amused her that as a Playera, the best beach in the world was right here.
 
 - what happened in 2050: the arrest and trial but through her eyes. We have another chapter with Morgana. Can I somehow not reveal that it was Seumas who hacked the network state? In short:


]]
## Chapter 4. Autumn, Muir

[[2070
- Diederik's trip I. Muir talks about where dad has been, and how he stays in touch. As before, beach in the middle
What is the link between the bunkers and Diederik's trip? 
In any case, the trigger is easy: a new message 

Yesterday, a new message from Dad had come. As usual, it was a memory card with pictures of sand and a few cryptic notes. With Mama's program, it turned into a long letter.

The content? By then Diederik is almost on his way back so he's either in China, the Ryukyu or Japan. Let's make it Japan, but maybe Kyushuu


]]


Late Autumn was Muir's least favourite time of the year to be on the beach. The dark days and the interminable blanket of low grey clouds made him melancholy and for some reason it always made him think of the battle that had been fought here. Which was now a hundred and twenty five years ago, and still the beach was strewn with rusty bullets and age-blackened shells. The concrete bunkers had all turned to sand though, only their rebar remaining, like cubist whale skeletons.

## Chapter 5. Winter, Suna

[[2071
- Diederik's trip II Suna focuses on other aspects of dads trip and is more technical about the communication. Beach at the start

Yesterday, a new message from Dad had come. As usual, it was a memory card with pictures of sand and a few cryptic notes. Suna still thought it was very clever to combine one-time pads and steganography, and the sand pictures always had a certain poetry. Mama had written a small script that figured out which key to use to get the stego message, then which pad to use to decrypt it. 

The content? By then Diederik is almost on his way back so he's either in China, the Ryukyu or Japan. Because of the poetry, I think this should be about the link with Yokohama kaidashi kikou; then let Suna think back specifically about the China letter which was in fact written for her.



]]

## Chapter 6. Spring, Muir and Mama

[[2071
- A meeting with mama, in Wissant
Where she explains that she and Morgana did the cyber job but that dad of course knew of it
I think we'll have Muir's partner and the daughter walking ahead in the distance

- So what we reveal here is who did the cyber job and what it was. This could be rather short but I still want the beach in the middle

How do we lead into it? Mama asked if she could come and stay with us. We hadn't seen her in over a year and Sarah was delighted at the prospect of seeing her granny. She was still living in Glasgow so it was quite a journey. 

Muir also looked forward to seeing Mama. But he also suspected that the timing of the visit was no coincidence: in his last letter, Dad had hinted he might be coming back soon. Mama might have come to pave the way, so to speak. Muir felt a certain apprehension. 

Some more transition

Muir, I guess you know why I have come now, don't you? Dad will be back soon and we need to have a talk about that. We both owe you an apology, or at least an explanation.


- "Both of you?"

- "Yes Muir, both of us, and even Yaya. You must have figured out that things were never quite what they seemed, haven't you?

- "Well, all I know is that it was all very odd and a lot didn't add up, but I still don't see why Dad had to desert us.

- "That will be for Dad to explain to you himself when he's back. But I want to explain what led on to it. You know that until 2035, fascists were in charge everywhere, don't you? 

- "Aye, and that they came back to power around the time of Dad's trial. I kind of suspected there had to be some connection, but I could never work it out."

- "Do you know what caused the Global Fascism Collapse?

- "We learned in school that it had something to do with what they called the Network State in America and India. Apparently it was actually a chaotic system but nobody had realised that, and so it collapsed without any warning, and took the fash down with it"

- "That's what the history books in Scotland say all right, and it's mostly correct. The reality was only a little bit different. The Network State, this kind of ostensibly distributed but really highly centralised state substitute run by the tech megacorps, was unstable, but it was not chaotic in the theoretical sense. Meaning that it needed quite a push to collapse. And that was what your Dad was tried for and found guilty. The new regime didn't want to make a hero, that's why they hushed it all up. 
- "That sounds fantastic, I can't believe it. Dad could never have done something like that: he wasn't good enough a hacker, and also, wouldn't you have known?
- "I didn't say he did it. I said that's what he was tried for. But he did confess and all evidence supported his confession, right?
- "As far as that went. You know I'm no expert, Mama, but me and Suna went over this again and again at the time, and we couldn't believe he would leave a trail like that. If nothing else, _you_ would have noticed.
- "Ah, but I wasn't there at the time, was I? That was confirmed during the trial: I was with Yaya looking after Yayo, firmly off-grid." (She says with an ironic smile while stressing the last two words)
- Muir stops and looks at her. "Meaning you weren't?" 
- "Oh, I was there all right, all the time. But you know, being off-grid and being off-line is not always quite the same."
- "So you knew what Dad was up to?"
- "Dad didn't do anything."
- Muir was a picture of confusion "He didn't -- but then how -- who -- ???"
- "It was Yaya who organised it, and I and many others helped with the execution. And even if I say so myself, we did an almost perfect job, not a single shred of evidence against us. 

But that was in 2034. In the late 2040s, when we saw that the fash were staging a comeback, we all felt something had to be done. And Dad had a plan. It was dangerous and ethically questionable, but we all agreed to it. Reluctantly, and after interminable discussions, bu we agreed in the end.

He'll have to tell you what it was, but the upshot was that he needed a cover, and being sued for a crime from 15 years ago was the least of the evils. It also meant Yaya and I and all the others were officially in the clear. And so Dad went to jail to protect the plan and us. When he came out of jail, the fascists were still in charge here so it was best for him to disappear until they were gone again, just in case they trace something back to him. It took a bit longer than we'd hoped because they managed to postpone the elections by two years. 



]]


## Chapter 7. (Late) Spring, Morgana and Diederik

[[2071
- Part of the story
 - what happened in 2050: the arrest and trial but through her eyes. Here we reveal that it was Seumas and Yaya who hacked the network state. 

But which part? Maybe some background as to why Morgana is an activist and Diarmad does not come into the picture?
This could also be a family reunion, or maybe just Morgana and Suna


- Or this could be with Diederik, who's back. If we choose this, it will be mainly about him facing Muir and coming clear. 
Also about Diarmad who died in 2059. 

"I'm very sorry I couldn't be here for Diarmad"
"We knew that's how it would be. He knew as well."

Something like, one morning, Diederik calls at the house. They have breakfast and do small talk, then they go onto the beach. A sentence:

"Well, Rico, I am so glad you made it back in time." He'd always been "Rico" to her. She could see the name triggered a flood of memories. "I'm very glad for that too, Morgana. And to find you so well too. I'm sure you'll be with us for a long time yet. [[ this is with the new 2062 timing]]"

Then they go for a walk on the beach, cue some beach descriptions, and talk about the old times and what's to be done now.



- What are you going to do next? 
- I have to go and see Muir and Suna. Especially Muir. From what I gathered from Seònaid, Suna has practically worked it all out by herself. But Muir ... I feel very guilty --
- Don't be. You did what was best for all of us. I think we should all be fine now. I don't think the tide will turn again before you and Seònaid are my age, and by then none of it will matter anymore.
 Muir's all right, with Hélène and Sara he has to live in the present. I'm sure he'll be glad to see you. 


]]

## Chapter 8. (Early) Summer, Muir and Diederik

[[ Diederik arrives in the afternoon, they have something to eat and then go onto the beach the four of them.
At some point, Hélène and Sarah walk on; Muir and Diederik sit on the sand, the "we have to talk" scene. ]]
- Well, Dad, what can you possibly have done that was so bad you had to leave us all for almost twenty years? 
- twenty years ... Aye, it's been than long. And really, Muir, not that it helps you any but I am very sorry it had to be this way.
- You're right, it doesn't help. Come on, Dad, what did you do?
Rather than answering directly, Diederik looked towards a rusted tangle of rebar at the foot of a nearby dune. "That was a German bunker, wasn't it? Ever wondered what caused it to turn to sand?"
Muir was getting really annoyed. Even Dad knew very well what had happened to all the concrete. He was about the make a cutting rejoinder when he saw that Dad was not gazing at him intently, with a strange gleam in his eye. "Well, did you? Never wondered why the Erskine bridge was the first to fall? Or how it spread so quickly?"
Muir was dumbfounded. Suddenly, he understood, and at the same time he saw that he should have understood long ago. All blood left his face. 
"You -- it was you? You didn't! How could you?" 
Slowly, he recovered and now his confusion was replaced by anger. "You are a mass murderer! Thousands of people died because of what you did!" 
Diederik said nothing for a while, staring at the sea, then said quietly, "My parents were amongst those who died. I mixed the cup of sorrow for myself. I make no apology. What I did was a terrible thing."
Muir deflated a bit. Dad looked utterly miserable, and he couldn't sustain his anger. He remembered what Mama had told him. The movement had approved Dad's plan, and it had worked to defeat the fascists worldwide. He realised that on balance it had been the right thing to do, but it was still awful to think his dad had in cold blood created a bioweapon and unleashed it on the world. They sat in silence for a very long time. A way off, Hélène was helping Sarah to build a sand castle. The tide was coming in. 



## Chapter 9. (Early) Summer, Suna and Diederik

[[2071
- A meeting with dad. They meet in Ard Dìthreabh, of course, and then walk back towards Suna's home.


Where he finally explains what he did with the fungus, and why taking the rap for the cyber crime was the safer option
- Or maybe not: all that has now already explained before, and Suna had worked it all out by herself anyway.

But we can add some details, like the location of the experiments and how the old concrete wall that protected the Ardeer works had been the first to turn to sand, but somehow at the time nobody had paid attention to it. Also, about the spread, the role of Yaya. Maybe some details about the trial as well.
- I could have a variant of the fungus that, if present, stops the destructive fungus from taking hold. But that means concrete could make a return. Do we want that?

Ending with some glorious description of the dunes]]

