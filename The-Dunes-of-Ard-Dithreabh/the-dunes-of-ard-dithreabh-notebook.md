# The Dunes of Ard Dìthreabh

(which is what (I think) Ardeer really means, as it was also written Ard-dyir. It means the headland of the wilderness)

## Broad Ideas

- Third person, probably two main characters
- Every chapter starts with a character walking on the beach, with a description of the weather
- This might be a different beach over time or in different places
- One idea is to have it as a memory because the water level is now such that the beach is no longer there. OTOH it is possible to raise the beach simply through dredging. But of course that is expensive.
- Lightspeed Trilogy is 2067-2070 and he has the rise in sea level. Maybe having it in 2050 is a bit too early; but the Findhorn folks can be proactive and put everything on stilts.

## Characters

Muir and Suna , "papa" and "dad", the grandparents on both sides

### Muir Carter-Vandewoesteyne 

* A Scottish guy, born 2035, adopted. 
* Studies History but becomes a self-taught  heat pump installer and repairman and vegetable farmer.
* Looks: dark, stocky, Celtic, dark eyes. Like a younger version of Tommy Sheridan
* Partner Hélène, French; daughter Sarah born say 2058 so by 2070 she is 12

### Suna Carter-Vandewoesteyne 

* Born in China but Japanese mother, born 2035, adopted after the death of her great-grandmother, who was British Chinese and adopted her when her family died in a flood caused by a super-typhoon. 
* Studies Physics and becomes a furniture designer and maker.
* I imagine her to have long straight black hair, an a serious, intelligent look, and not exactly pretty but striking, strong, somewhat archaic features -- in fact, I know, make her look like Yuu.
* Partner Roy, Black South-African. Works remote in Malaysia so often has shifted hours

### Seumas Carter

* _Papa_
* Born 2010 in Glasgow
* Studied what?
* A Scottish computer expert with no formal training. 
* Looks rather dark and speaks Spanish
* Shortly after 2050, he transitions and becomes Seònaid, _Mama_

### Diederik Vandewoesteyne

* _Dad_  
* Born 2010 in Oostende
* Moved to Scotland in the 2030s
* A biologist working at UoG Dumfries on bacteria that convert waste.
* Skinny, blond hair (to one side in his youth)
* West-Flemish because grew up in Oostende
* So speaks Flemish, French, English and also Spanish; and maybe either Mandarin or Japanese?

### Diarmad Carter

* _Yayo_
* Father of Seumas
* Born in Aviemore in 1984
* Lives in Findhorn with Morgana
* Lived for some time either in Equatorial Guinea
* Fluent in Spanish
* Minor role but still need a credible backstory
* By 2050 he has CFS "like many people of his generation". He will have died by 2070, may say in 2063 (aged 79).
* It would be nice if he had a relevant role around 2035. 
 
### Morgana Anguesomo Meñana Nzang 

* _Yaya_
* Mother of Seumas
* Born in 1986 on Corisco in Equatorial Guinea
* Lives in Findhorn with Diarmad
* Playero father and Rioplatense mestiza mother so Black but rather parda:
https://globalurbanhistory.com/2016/12/19/pardo-is-the-new-black-the-urban-origins-of-argentinas-myth-of-black-disappearance/
 To differentiate a small, white, elite population from the rest the authorities increasingly used the label pardo or parda 
 a term used to describe a tanned-toned ambiguous person who did not fit the commonly used categories of  mestizo (the child of white and Indian parents),  mulato (the child of black and white parents),  zambo (the child of black and Indian parents),  negro (black), or  indio (Indian). 
In Buenos Aires, shortly after independence a new term emerged, trigueño (wheat-colored), which was used there to describe a growing, racially ambiguous population. In Buenos Aires, the term trigueño referred to  “light-skinned” castas (mulatos, mestizos, zambos) or  “dark-skinned” Europeans (Sicilians, Andalusians, Portuguese).
 
* Fluent in Spanish, French, English and Benga
* Serious hacker
* _not_ Carter
* Looks vaguely like Mireilla

### Anders Vandewoesteyne

* _Bompa_
* Father of Diederik
* Born in Oostende in 1985
* Lives in Vienna with 
* Minor role but still need a credible backstory. Would it be too cruel and too obvious to have them die when a bridge collapsed?

### Olina Vitovska

* _Bomma_ or grandma
* Mother of Diederik
* Born in Gent in 1990 
* Parents from the region of Moldova that borders on the Ukraine, say Palanca 
* Lives in Vienna
* Minor role but still need a credible backstory


## Timeline

1984 Grandfather of Seumas, Diarmad, born
1985 Grandfather of Diederik, Anders, born 
1986 Grandmother of Seumas, Morgana, born 
1990 Grandmother of Diederik born 
2010 Seumas and Diederik born. Diederik  is West-Flemish but born in Gent 
2025 Seumas and Diederik teenagers
2034 Global Fascism Collapse
2035 Suna and Muir born, children of the following baby boom; Seumas and Diederik 25
2050 D arrested and put in jail for 5 years (effectively); Seumas and Diederik 40, Suna and Muir 15; at this point they live in Glasgow I think, is more convenient. 
2052-2056 Muir studies History, Suna studies Physics
2054 The concrete starts to crumble. 
2055 D is freed, aged 45
2056 D leaves and walks all the way to Japan. Suna and Muir are 21. Muir moves to Wissant and finds a partner. Suna stays with Seònaid for a while.
2056 Suna moves out and lives on her own. She designs and makes furniture in Barassie (spell in Gaelic)
2059 Diarmad dies
2060 The concrete has crumbled almost everywhere. Suna has also found a partner. Meanwhile, Muir has a daughter, Sara.

2070 D returns Seumas and Diederik 60, Suna and Muir 35, Sara is 12. Suna now lives in Barassie. D and Suna meet and he tells the story. In parallel, Seònaid tells the story to Muir (who either is visiting or is being visited. If they still live in Scotland that is easier, but abroad is more interesting. )
At this point, it is possible that Diederik's grandparents are still alive. Let's say they live in Vienna and don't come into the story.

If instead of 2070, I make it say 2062:
then Diederik and Seònaid are 52; Yaya is 76; Yayo has died; Sarah is only 6 


## Key ideas

### Beaches

Of course there is the Stevenston/Irvine/Troon beach. And I need at least one more, I'd say Wissant. But it would be very nice to have Findhorn there as well, with Findhorn Eco Village on piles above the waters.
- Stevenston beach is where Dad finally meets Suna and explains what really happened
- Wissant is where Mom meets Muir and tells her side of the story
- If the parents lived in Findhorn off-grid rather than Acharcle, it would work. But then I would need a scene with the grandmother, who'd better have a name and more of a role. We could have a scene maybe before the arrest.

### The fungus

A fungus appears that turns concrete into sand (but only under influence of light and humidity).
Bridges, buildings, airports and Glasgow pavements crumble and turn to sand. In particular, the huge concrete walls of the Ardeer factory completely turn to sand.

- It should be delayed action. Some kind of exponential would do that: require a certain critical mass. 
- It spreads via the tires of the airplanes so it gets everywhere practically at the same time.
- So all over the world, things start to collapse. It takes a while to realise that they don't collapse in very dry places
- I want it to be very hard to get rid of. And yet, I don't want it to work well in total darkness. Although whether it's light or air or ozone that does it?
Ozone is good: means it will work outside and not inside, but does not need light. 
- The key question is how long the collapse would take, and how long the recovery would take. This is all up to me. What I would say is that after about 20 years, new concrete-free bridges have replaced all old ones, and same for buildings. Airports have had to revert to asphalt but it means they can't run the very heave planes and can't operate when it is too hot. Also, all airline companies have of course gone bust. Same for all long-distance haulage. Rail and water transport flourish. Even car use has decreased dramatically as large motorways have become of limited use.
The problem is of course that this would require investment in railways. I think this would beprivate investment. In Victorian times, it took about 5 years to build a new line, so surely we can do this in a few years. Similar for harbours. There is a lot of concrete there but I think it would be relatively easy to replace it.
- currently, there are about as many runways in asphalt as in concrete. By 2050 almost all would be concrete: 20 year lifetime so need replacing, and by that time, there are two factors: (1) it is too hot for asphalt (2) asphalt has become too expensive. That means roads also are mostly concrete by 2050.
- of course it means that in principle, roads and airports can be redone in asphalt. 
- High Density Concrete (nuclear power plants) and lime concrete 

### The dunes

With the rise in sea level, the water line has retreated but, combined with the crumbling concrete, this has created magnificent dunes. Also, the Irvine and Garnock rivers have moved (or dried up because of the AMOC collapse?) and now there is a stretch of beach all the way from Stevenston to Troon

### The sand pictures

Before he leaves, D leaves an SSD full of pictures of sand. Photographing sand is his hobby, has been for a long time. There are thousands. During his travels, he sends them SSDs with more pictures, and also phone numbers. This is a combination of one-time pads and steganography: the messages are first encrypted with the original sand picture as a one-time pad, and then spread through new sand pictures using stego. In this way, they stay in touch but D can't be traced.

I wonder about the phones. I can see a future where you can't buy a device without registration, so the state knows what you have. This is a bad idea as the real crims will of course simply steal them. Also, I guess as long as we have FPGAs, we can simply sandbox all traffic and so pretend to be another device. Problem with that is that the ID will be cryptographically protected. But I can't really see a global system of key issuing authorities.

### The arrest

Diederik has made sure Seumas is with them and they are off-grid.
(but they aren't really, and the signal is that his phone no longer answers. He destroys the SIM.) 
So the police do a dawn raid but Diederik is already awake. He tells the kids not to worry and to do as the cops say, and that it's him they want. The police is about to break down the door but he just opens it. They arrest all of them, but D confesses right away so they have to let the kids go. 
Seumas and their grandmother are waiting for them, to make sure they are not taken into care. The police arrests Seumas as well but have to let him go soon.

### The trial and aftermath

Turns out D is accused of hacking into the largest megacorp and doing so much damage that the corporation can't recover from it. How that is possible is not clear, as he is no computer expert. S is one but D claims it was all his work and there is no evidence against S at all. So D gets 10 years and is out after 5. In that period, S transitions and becomes Seònaid. When D comes out, he returns to the home only briefly, and leaves an SSD with pictures of sand.

I wonder if the crime could have been committed in 2034, contributing to the collapse (e.g. something about the network state, or the Musk empire); and that by 2050 the tide has swung to the right again, and the old crime is uncovered (intentionally of course)

### The slow disaster

Meanwhile, the first reports of problems with concrete are coming in. A bridge collapses, then another one. Buildings start to crack. Airports turn to sand. Initially it is gradual and it takes a while for the realisation that there must be a common cause. The research teams identify a fungus that breaks down concrete, and it is incredibly tenacious. They manage to trace it back to the beach at Stevenston, but there the trail peters out: there is a lot of eroded concrete and a lot of sand, but no evidence.

### The journey of Diederik

Which I would mainly use to highlight the effects of climate change but mostly the resilience of all the communities he encounters. And what I would really like is that he ends up on the Shonan peninsula, near the drowned Yokohama. 
I think I will allow him to get back by boat, one of those huge sail-driven container ships.

### The explanations

They are simple: Diederik created the fungus, tested it and let it loose. The hack, which was actually done by Seumas and exploited a backdoor in the actual hardware, put in there by a collapsed regime (would almost have to be China), and used it to ruin the megacorp. Call it Mazen. But the purpose was so that it would be discovered and deflect attention from the actual crime. Seumas deleted all evidence and destroyed all storage, and this is acceptable if he wanted to mask his hack. So 

## Background

### Global Fascism Collapse of 2034

Fascism won just about everywhere in the first half of the 2020s. Apart from the typical repression, a common feature was a huge rise in emissions. Also, esp. in the US, a rise in epidemics. Then came another pandemic, again caused indirected by climate change: a species forced to migrate due to destruction of its habitat. And the planet somehow seemed to feel the malice with which those regimes were burning fossil fuels. Or maybe it was our incomplete understanding of the tipping points; in any case, all over the world, severe weather events increased in frequency and severity. The Gulf Stream tarted to slow down noticably in the early 2030s. The global economy nosedived. Protests arose everywhere but were brutally suppressed. 
As long as the fascist regimes had financial and tech support, it was hard to see how they could be overcome. 
Then something unexpected happened: for unknown reasons, the network state collapsed from one day to another. Rumours were rife but nothing was certain except that it weakened the regimes in India and the US so much that they could no longer stem the tide of popular unrest and were overthrown. A domino effect so strong that it would make Johnson turn in his grave saw one regime after another fall. It became know as the Global Fascism Collapse.

We could have a sideplot of Morgana and Diarmad smuggling vaccines from Belgium into Scotland. Not all that important. Maybe Scotland was devolved enough to do that officially.

### Tide turning again 2049

Barely 15 years later, the political pendulum was already swinging to the right again. In a way this was understandable: the green revolution did not stop the disasters, as it came much too late. And that made it easy for people to start questioning it. The rise in climate refugees made the situation worse. And so, but 2049, it was clear the dark times were coming back. Authoritarianism reared its ugly head again; state control, which had never really been away anyway, became much more pronounced. 

### The crime

It was therefore not a complete surprise that Diederik got arrested, although the reason was not clear. Muir knew that dad had been up to something for quite a while, but neither he nor Suna had any idea what it was. But whatever it was, it couldn't be what dad confessed to. Still, apparently the evidence was there, and IP address trail leading right to our house. The IT wizard in our house was papa, not dad. Then again, he'd never have been so careless to leave a trail like that. But papa must have had a watertight alibi, they didn't even charge him. Apparently, this was about some cybre crime from before Muir and Suna were born. But the police had all the records, and it was beyond doubt that papa had been with yaya and yayo at the time, firmly off-grid. How the police could be so sure was a mystery to Muir, but papa told them that in those days, surveillance was at its peak. Still, there was something wrong about the whole thing. Dad was no elite hacker, the best he could do was some scripts for his DNA sequencer. He could never have pulled of such a thing. Papa could have done it, especially with yaya's help. But that was apparently not even on the cards. It was all very odd. But what it apparently amounted to was that dad had single-handedly brought down the network state in the US and was therefore the unsung hero of the anti-fascist revolution. This did not come out officially in the trial. The current regime did not want to create a hero, they wanted a common criminal. So, after a brief trial that was mostly held behind closed doors, dad was sentenced to 10 years in prison, but the lawyer assured us that with good behaviour it would be five years at most.

## Structure 

Alternating between Muir and Suna, and one or two chapters Morgana. I want Suna's chapters to be poetic. Muir maybe more matter of fact. And Morgana more political. I want every chapter to take place during a walk on a beach, but maybe that is not possible. Could I have three walks for Muir and Suna and two for Morgana? 

Lets's try something like that:
-----------------------------------------------------------------------------------------------------------------
Chapter 1. Winter, Muir
[[2070
- what happened in 2054 to 2056 
What we learn: the family; that dad is in jail; that the concrete catastrophe started in 2054; that all this was long ago and that he still misses his dad. Also, that he is in Wissant. And that he now has a daughter of 12. The beach description should probably go in the middle, and something (wind, sea, a bird, a ship, the cliffs of Dover) should trigger him to think about dad who left. 
At the end of this chapter, we know about most of the family and that dad was in jail and then left, but we don't know why]]

The opening line something like:

In the fourth year of dad's prison sentence, the concrete started to crumble. It was what is technically called a catastrophic process, which means there is a sudden transition that can't be predicted. So nobody saw it coming. It was a global catastrophe in the common sense as well: everywhere, bridges started to fall, buildings collapsed and airports turned to sand.

The Erskine Bridge had been the first to fall. One starless night, without any warning it crumbled into the Clyde. The investigation did not find anything that could explain the disaster. It seemed to have been a freak incident. We were living quite nearby at the time and I remember the noise of the bridge collapsing into the water. 
----

--------
Chapter 2. Spring, Suna 
[[2070
- starts with a lyrical description of the beach
- the state of the world:  the consequences of the concrete collapse
- the arrest and Suna's doubts, papa's transition, maybe this links to Morgana
- Suna's job]]

--------
Chapter 3. Summer, Morgana
2070
- what happened in 2050 and what led on to it: the rise of fascism in the 2020s, the collapse in 2035, the swing. It should be clear that Morgana is a very engaged person. Here we can have a pan on the beach at the end and the detail of Findhorn ( Inbhir Èireann)  on stilts.
- this could maybe be with Seònaid. If so, we drop some hints about what they did together
--------
Chapter 4. Autumn, Muir
[[2070
- Diederik's trip I. Muir talks about where dad has been, and how he stays in touch. As before, beach in the middle
- We lead this in with a recent letter and then let Muir remember previous letters over the years. 
]]
--------
Chapter 5. Winter, Suna
2071
- Diederik's trip II Suna focuses on other aspects of dads trip and is more technical about the communication. Beach at the start
- We lead this in with a recent letter and then let Suna remember previous letters over the years. 
- I think it's OK to have Diederik send two letters with only three months in between. In the final one he announces his return.

--------
Chapter 6. Spring, Morgana
2071
- Part of the story
But which part? Maybe some background as to why Morgana is an activist and Diarmad does not come into the picture?
- This could also be a family reunion, or maybe just Morgana and Suna
- Or this could be with Diederik, who's back. If we choose this, it will be mainly about him facing Muir and coming clear. 
--------
Chapter 7. Summer, Muir
2071
- A meeting with Mama, in Wissant
Where she explains that she and Morgana did the cyber job but that dad of course knew of it
I think we'll have Muir's partner and the daughter walking ahead in the distance
- So what we reveal here is who did the cyber job and what it was. This could be rather short but I still want the beach in the middle
--------
Chapter 8. Autumn, Suna
2071
- A meeting with dad
Where he finally explains what he did with the fungus, and why taking the rap for the cyber crime was the safer option; and make it clear that Suna had guessed most of that.
Beginning and ending with some glorious description of the dunes

[[ The problem is that we should really have a meeting between Diederik and Muir.
But then again, I want to end in Ard Dìthreabh, so that is with Suna 


What we could do is add an extra chapter: "Spring, Muir", where he meets Diederik
But then the question is why Diederik would wait half a year to see Suna.


So I guess what we need is a meeting between Muir and Mama before Diederik returns, say Spring. Then the chapter between Morgana and Diederik, Summer or Spring. Then Muir and Diederik, Summer ; then Suna and Diederik, Summer; so there is no chapter where 


]]