#!/usr/bin/env perl
use v5.30;
use warnings;
use strict;

my $md = $ARGV[0];
my $md_base = $md; $md_base =~s/\.md//;
my $md_for_tex = $md_base.'_for_tex';
my $md_for_lyx = $md_base.'_for_lyx';

system("perl ./preprocess_md_for_tex.pl $md");
system("./md_to_tex.sh $md_for_tex");
system("perl ./preprocess_tex_for_lyx.pl $md_for_tex");
