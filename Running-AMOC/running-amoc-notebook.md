# Running AMOC

## Chapter 1. Our travels take us further North 
Present, Stockholm. Intro. Mostly ready
## Chapter 2. There's a lot of opportunities
Recollection. The AMOC paper, the idea, the core consortium. Mostly ready
## Chapter 3. <Media campaign and crowdfunding>
Recollection. The fundraiser that went viral. About half done
## Chapter 4. Accuracy 
Present, Stockholm, then recollection. The results were not good enough. Mostly ready
## Chapter 5. Atomic
Recollection. The submarine link. Mostly ready
## Chapter 6. <Momentum>
Recollection; but maybe return to Stockholm? A walk along the waterfront maybe? Very incomplete
## Chapter 7. Celebrate the day a better world was won
Recollection, but should end in the now; or should we have another chapter. Very incomplete

Text in [[ …  ]] are snippets, drafts
Text in {{ … }} are outlines

## Basic idea

{{
The basic idea is that the protagonist starts and helps organise a movement that does the following:

* better simulation of AMOC to pin down the exact moment it will collapse and how quickly the effects will be noticable.
* a grassroots support movement
* a targeted lobbying campaign with elected politicians etc 
* even, a totally secret military operation, UK (maybe +US), where they use the nuclear subs to sample the AMOC water at various depths and places. This is plausible by the protagonist meeting the wife of a sub captain in a menopause support group in Helensburgh or Rhu. Of course it would mean that the search pattern would be changed, so all of it deeply secret, but no harm done. Could get a nice scene at the narrows in there.
* a press campaign, ideally we'd want the Sun to scream "we'll all freeze!" all the time. Call it "Stop the Freeze!" ?
* of course a social media campaign too. Maybe even Project for Awesome ?

The final situation should be that all over Europe the Greens get to power (and in the UK they abolish fptp and grant the referendum). Degrowth becomes a reality, and I'd leave it open if AMOC will eventually collapse or not, but either way, people are prepared and the govt is on their side.
In the US maybe this will not work, but there should be a *huge* green movement; need to check about Canada too. 

I want to make the point somewhere that China, India and Russia of course prefer to do nothing, because a weakened US and EU is to their advantage; but the scientists in those countries don't necessarily share those views, they just want to do the best science and help humanity.

Other points to make: at the 
- funding agencies won't fund this because they don't react to emergencies. They will simply say "this is currently not a priority" and they will say "but you can always go for an open call" (with a success rate of 4%)
- "world leading" teams are not very likely to help, so this will be teams that are not world leading. 

We definitely need the Green parties on board, but why wouldn't they? 

And I think I should put in Charlotte's grassroots idea of a network of cities that are affected by climate change. Need to ask her for the name, or invent one.
}}

## Structure

{{
I think I will start this with a flashback, starting 2024. The overall timeline is:
by 2028, we know for sure that the collapse is due between 2040 and 2050 unless emissions drop dramatically. With the sub data, in 2029, this gets narrowed down to 2045. We have elections in 2030, 2032 and 2034. By 2035, it's all over, and we're waiting. The protagonist is then 67.

Does this work for the science? Given enough funding, say 2 postdocs per team and about 10 teams, and 3 years, we need about £6M. Make that £10M.

Then, assuming we start in March 2024. 6 months setup. 3x6-monthly fundraising targets. So the first teams could start working March 2025 and might get results end of 2026. Other teams get their results end of 2027. Writing the paper, due diligence and all, and then publication, gets us probably to end of 2028. 
The sub data are in by end of 2026, and we can therefore get them to the teams already in 2027. 
Then a new funding round, and by the end of 2029 we get the updated results. 
It is possible to globally crowdfund £10M but it is a tall order. It would be better to have many smaller ones, with targets of around £100K.
Also, there must be some way to pressurise the institutions into contributing. And of course there is no reason not to go for funding agencies, especially for the 2nd round.
}}



### Ch 1. Our travels take us further North 

*You must be old, so take it easier* (NFADS, "Stockholm")

{{
She's then living with some other people, maybe in in a flat in Nacka Strand (near Stockholm). (She should have moved but somewhere affected by the AMOC collapse and in a flat reachable by train. Could be Finland or Lithuania or Norway as well.)
I imagine the household to consist of a younger woman (40) with a teenage daughter (15), immigrants. But it could very well be a couple with an absent husband, as long as he is a nice guy. It could be Eza. The latter makes sense because the protagonist is Indonesian and that means they would had some affinity 
In any case, this person thinks back to how it started.  I will let her think back to when the results came out, and then back to how it started, and then forward to the sub data, and then back to how that was achieved. 
The challenge is to mix in all the political changes. Maybe the heat pump should be the trigger.
}}

[[ I'm sitting in my rocking chair looking over Saltsjön towards the lights on Blockhusudden, on a late March night. There are still raindrops on the window from the passing showers but the sky is clear and there are even a few stars out. Eza is at work, I hear the intermittent click of her mouse and an occasional bursts of muted keystrokes, echoing the earlier sound of the raindrops hitting the glass. Nur is busy in the kitchen, frying garlic and spices. It smells good already. 

Not long after the final result were published, and long before the dust had settled, Eza told me of her impending move to Sweden. She had been offered a position at the KTH Climate Action Centre. It was a fantastic opportunity but at very short notice, so finding a place in Stockholm would be hard. Also, her husband wouldn't be able to join her for a while.

I had been away there for so long, and my son had recently moved to Australia, so I really didn't have any ties with Sweden any more. But suddenly I had this vision of living with Eza and Nur in a cosy flat in Stockholm. Out of the blue I was struck by a an attack of homesickness so strong that tears sprung into my eyes. I have been living in Scotland for forty years an never before had something like this happened to me.
Eza immediately noticed something was amiss. "What is it, Sigrid? You nook shaken. Do you think it's not a good idea?"
I composed myself a little "Of course it's a goo idea, Eza, and I am very glad you got offered the opportunity. But I was thinking how you would love Stockholm and suddenly I felt so very homesick. I wish I could go back to live in Sweden with you and Nur". Eza's expressive eyebrows went up, her face became a study in surprise, wonder -- and delight. "Would you really like that? What a wonderful idea! But shouldn't you think it over a bit?" There was a slight hint of worry in her voice. I would have been worried if I was her: I'd never been so confused in my life. So I did think it over, carefully, over the course of the next weeks. It didn't change my mind. If anything, my desire to go back grew stronger. So I contacted my son and proposed to buy his flat in Nacka Strand. We quickly agreed a deal. I had a solicitor draw up a contract for shared ownership with Eza, organised my move and canceled my rent. I had never bought a property. I'd always put it down to "too much hassle", but maybe subconsciously it had been a way of resisting stronger ties to Scotland. Anyway, less than half a year later, here I was, back in Sweden after all this time. I could practically see our old home through the window.

The faint hum of the air source heat pump is soothing. It's made by a Belgian co-op started Li-zhen's friend Paul. They found a way to make the cheap, reliable, and easy to repair. I met him once and he had told me the story of how he and Li-Zhen met during their PhD ("One day, I was working in the clean room, babysitting the machines, and someone waved at me through the window. I looked up from the computer and there she was."), and the very unlikely events that had led to him learning the high-tech plumbing skills that he now used in the business ("You wouldn't believe how poor their safety arrangements were. And in my youthful naiveté, I decided that Something Had To Be Done about that. Of course I ended up having to do it, rather than doing my PhD research."). 

{{ Maybe this should be Chapter 3. Or I can keep a sentence as a teaser. 
It was Li-zhen's team, at Canada's CCCma, https://www.canada.ca/en/environment-climate-change/services/climate-change/science-research-data/modeling-projections-analysis/centre-modelling-analysis.html
who had first managed to confirm the results of the notorious 2023 work by the Ditlevsens 
https://www.nature.com/articles/s41467-023-39810-w
and then narrow down their estimate. Soon after, Mirza's team at the APEC Climate Center Busan had followed suit, and then an international team led by Sarah, using the ICON model as their starting point. Rather to my surprise, even the Mofem-based model lead by Glasgow managed to obtain results, and there was complete consensus, a very rare thing. It narrowed the AMOC collapse down to 2040-2050, with >95% certainty, as good as it gets. 
}}

[[
It was Li-zhen's team who had first managed to confirm the results of the notorious 2023 work by the Ditlevsens and then narrow down their estimate. Soon after, Mirza's team had followed suit. It was amazing how well things had worked out.
]]

### Chapter 2. I can't believe the news today

*I can't close my eyes and make it go away* [[[ I'd like a better title for this ]]]

{{ flashback to the actual start. This will be rather long because what we need is:
* How she was moved to act
She read the article, and it came as a bit of a shock. Not that it was a new notion, the possibility had been raised already in the late 90s. But the confidence level on that latest work was really high, and she trusted the authors. 
But it would not be enough to move the community into action. Politicians were bound to assume the best possible case, which was 2095. The public would have forgotten the news tomorrow. 
On the other hand, this could be a once-in-a-lifetime opportunity, because the threat was very real and the consequences would be immediate, widespread and far-reaching. Without action, people would literally freeze to death. Huge areas of land would become to arid to farm. All that was needed was for the public to believe this. Then politicians would have to follow suit, else they would lose out.

* What she planned to do
* How she brought the team together
And then in particular for the team, we need a lot more. The blank spots are the media campaign and the fundraiser.
But in broad outline, the protagonist must create a network of people who will do all these things, and she'll have to rely on her own network to do that.

In any case, the protagonist contacts her friend Elías at the Aurora Foundation, and he gets in touch with <the fundraiser> and/or <the media person>; with some details on the foundation and Elias. Maybe simply, the foundation has a global network, and via that network, we get a network of fundraisers and campaigners. 

We can then focus on the UK situation and refer to EU and US briefly, rather than needing all that detail. So maybe, in that way, we don't need those characters.
But we will need a core, and I think for simplicity it would be the same scientists who do the work and form the core.
}}

[[ 
I remember the day I read the article. I was a day very similar to today, cold early spring weather, or late winter -- in Scotland, winter had a long arm. It was my habit to set a morning aside every week for reading papers, a very enjoyable activity: easy chair, nice cup of coffee, looking out over the rain-swept Gareloch. That morning, I didn't get very far. The Ditlevsen paper shook me. Not that it was a new notion, the possibility of collapse of the thermohaline conductor had been raised already in the late 90s. But the confidence level on this latest work was really high, and I trusted the authors. And yet, it would not be enough to move the international community into action. Politicians were bound to assume the best possible case, which was 2095. The public would have forgotten the news tomorrow. 
I felt strongly that this was a once-in-a-lifetime opportunity. The threat was very real and the consequences would be immediate, widespread and devastating. Without action, people would literally freeze to death. Huge areas of land would become to arid to farm. All that was needed was for the public to believe this. Then politicians would have to follow suit, else they would lose out.

So I got thinking. What was needed most of all was a more precise timing of the AMOC collapse, with a scientific consensus. The way to achieve this was to have this work done by many different teams that would share best practice but keep their own methodologies. The results would be published in a joint paper.
The top teams would likely not be interested, although it wouldn't harm in asking them. But I was confident that using my network, I could build an international consortium of around ten teams that were good enough to do the job.
The main problem was, as always, funding. Research funding agencies were unlikely to fund this because they don't react to emergencies. They would simply say "this is currently not a priority" and add "but you can always go for an open call". With a success rate of 4%, but they didn't have to spell that out. Also, national agencies would usually only provide funding for their own teams, and the risk of not getting funding for one of the collaborators would almost certainly kill any such proposal. So we'd need a different source of funding. And that meant crowdfunding. It would be a very tall order, but it was the only way. Assuming every team had two postdocs and we needed funding for three years (that should be enough for two iterations), that would be about £6M. Hopefully some of the teams would have other sources of funding, but we's still need to raise several millions.
For crowdfunding to work, lots of people would have to be made aware of the AMOC collapse and its consequences, and what we could do about it. We had to spread a message of urgency as well as hope. And it was all a gamble: with the earliest date for the collapse set at 2025 by the Ditlevsen paper, the whole effort might come too late. But chances were that we had a few more decades, so we had to make the most of a very short window of opportunity. I had to get cracking, and soon. 
I realised how poorly I was suited for this kind of project: I, an introverted, low-profile academic, a middle-aged woman of colour, must be one of the most unlikely people to start a successful international large-scale fundraising activity and media campaign. Luckily, I knew at least one person who mas much better suited, and might be up to the task. And he might know others. 
Anyway, I decided to start with in my comfort zone, by building the research consortium. I've always thought that a rather grand term for what is hardly more than arranging some chats with a few friends. For this particular research, there were three scientists I knew well that I thought could do it: Li-Zhen Lai, Mirza Hameed and Eza Shaarani.

Li-Zhen was Chinese, a solid state physicist by training. She had done her PhD in Belgium and I had met her at that time, years ago, at a conference where she presented a paper co-authored with her mother, who was an atmospheric scientist. They had repurposed a semiconductor metallisation simulator to solve the Navier-Stokes equation. She explained how that came about: "Because I was Chinese, they really didn't take my PhD seriously. I had to do way more odd jobs than the other PhD students, and hardly got any supervision. But it also meant they left me alone, so nobody questioned when I used the modelling workstations in the evening to work on mom's project."  On the strength of that work, she got offered a postdoc at the University of Ottawa, and she now led the team at the Canadian Centre for Climate Modelling and Analysis at the University of Victoria.

When I called her, Li-Zhen was her usual force-of-life self, glowing even on Zoom, her strong-boned features softened by strong sideways sunlight, her black hair shining with a reddish halo. She was brimming over with ideas and enthusiasm. It was almost as if she'd been waiting for my call. "As soon as I read that paper, I started thinking of a confirmation study. I have some ideas for other teams to involve; I even think I see my way to get my institution to fund part of the research. It wouldn't be enough though, so getting the funding sorted is the key priority." I explained my idea for a media campaign and a crowd funder. Li-Zhen considered it briefly, nodded and said "I might know a few people who could help, I'll put you in touch with them."

Mirza was an Indian expert in ocean circulation models who had emailed me one day out of the blue with a question about acceleration of  numerical models and, in doing so, had inadvertently switched the direction of my career irreversibly onto the track of efficiency of computational models. That had led me to work with engineers and computing scientists all over the world, collaborations which I found very rewarding even though they had proven hard to fund and publish. Despite all the talk about the importance of interdisciplinary work, funding agencies and journals are rather insular. Still, I had no regrets.
Mirza worked at a small private university in the south of Japan. I had visited him there once for a few months in summer and had fallen in love with Japan forever. He came to Scotland for a return visit later and we had great times hillwalking. Later, he and his family moved to Korea, and he now had a large group at the APEC Climate Center in Busan.

Talking to Mirza was always slightly odd, in a hard to define way. He looked rather gaunt, had an intense stare and was slow to smile. Despite his important position and academic reputation, he was always very careful not to push any agendas of his own, and yet in the course of a conversation we would generate lots of new and often crazy ideas. He carefully assessed the options for my proposal, and said "I suggest it might indeed be good if I could work with other teams, but for practical purposes it might be better to keep the efforts decentralised and loosely  coupled." 
He also pointed out the politics of the situation. "There are many countries, especially in Asia, that think they would benefit from an AMOC collapse, as it would weaken the US and the EU considerably." As a climate scientist, and one who had pointed out the connection between the El Niño Southern Oscillation and the Indian Ocean dipole, he of course knew better. "I think the message about AMOC collapse should have a strong global dimension, making it clear that everybody would lose out." 
"I don't think APEC will fund this work, and I can't commit to it unless there is another source of funding. But I will approach two other teams. There's one in Japan and one in Australia. They might have a more favourable funding situation." 

I had gotten to know Eza while she was doing here PhD at our institute. She was Malay, from Sarawak, so we spoke the same language, and we found that we had a lot more in common besides that. She had a baby during that time. Her mom came over to help her in the first few months, but after that I often helped out. I knew from personal experience how hard it was combine a PhD with looking after a baby. At least her husband was doing his part, but he also had a full-time job. So I became a bit like and adopted aunt, and she became one of my best friends. We had maintained a close bond after she returned to Malaysia. 
She now was a Professor at the Universiti Malaya and lead their Climate Change Network. She specialised in ocean transports around the Maritime Continent. 

Meeting up with Eza always felt very cosy,  maybe because we spoke Iban together. She also had a very warm personality and a knack for making me forget my troubles.  She looked the way I'd always wanted to look, every feature just a little more defined than mine: eyebrows a bit stronger and much more expressive; nose a little straighter, mouth a little wider; and of course ten years younger. 
As always, she was very practical:
"I have already thought  how I could justify working on AMOC as an extension and validation study for the techniques used in my main research on thermohaline structure of ocean transport in the Southeast Asian Seas. It would bear out the claims on their effect on the global climate. Funding is actually not a problem: Malaysia has just received close to a hundred million dollars from the Global Environment Facility, and part of that funding had already been allocated to the Climate Change Network. I have worked with a team in Oman on circulation in the Arabian Gulf. The Sultanate has realised the importance of the ocean for their economy, and the inevitability of climate change, and consequently it is one of their priority areas for funding. So they might have a good change of getting a grant for a follow-on study."
"You're amazing!"


https://agupubs.onlinelibrary.wiley.com/doi/full/10.1002/2015JC011038

https://www.nst.com.my/news/nation/2023/10/967640/malaysia-receive-rm3648-mil-international-funds-fight-climate-change

https://www.metoffice.gov.uk/research/approach/collaboration/wcssp/weather-and-climate-science-for-service-partnership-southeast-asia

https://www.newton-gcrf.org/gcrf/
		
	

]]

### Chapter 3 Accuracy 

*an observer's refrain*

{{ Not sure if this can actually be a chapter. It is the end of the previous flashback. So it should be in the "now". Maybe the narrator has dinner with the family, and continues her recollections while doing the dishes.

The narrator recollects how the results came in, and how they got the media attention and the fundraisers. 
But also the problem with the data and accuracy: the window was still too wide. It was still not enough to spur politicians into action.

}}

[[And there we were, in 2026, barely two years later, with the most accurate results and the best scientific consensus ever. And that was only the beginning. When we got the measurement data to the teams, they did even better, and that's how we got where we are now.

But that had been a tall order. In fact, it had been pure luck. 
]]

### Chapter 4 Atomic

*Your hair looks beautiful, tonight*

{{ next flashback to the submarine subplot. This is a pure-luck thing: the protagonist meets the wife of a nuclear sub commander at the menopause support group in Helensburgh. They get on well (at first of course she doesn't know the husband's job). 
Something in there how these people genuinely believe they are making the world a safer place through those patrols.
So, at some point, she explains about her project and the problem with the data. https://theecologist.org/2021/oct/22/things-fall-apart

We need some backstory on the captain, because a man in his position usually would not go against orders. A possibility is that he has had an argument with his son who works for an environmental NGO (can maybe even hint at XR).  The father accused him of being a treehugger. The son retorted that all his defense was no good against climate change. Something along those lines.

Add to that that as a sub commander he of course was aware of the danger that climate changed poses to the nuclear bases. 

Then she meets the husband (that's when she learns what he does). He has been thinking about this and thinks he can help. His sub can do this. She of course asks, isn't that risky? Is it not against your orders? He says that, sure, there is some risk, but he has carefully let out some feelers and thinks that he could actually get away with it, because it does not compromise national security, on the contrary, it might help the UK be better prepared. He thinks he'll get off with a warning. 
}}

### Chapter 5 Celebrate the day a better world was won

*On the day, the storm has just begun<br>I will still hope that there are better days to come*

[[ So OK, here we are, we have everything. And the rest, as they say, is history: the sub results really were our trump: the deniers questioned the provenance of the data, because we had held back the sources. We let them. By the time they had become entrenched, the public opinion was firmly on our side  and "Stop the Freeze! had become an international demand. Even the rightwing government parties had added it to their manifestoes. And then we disclosed that the data had been obtained by UK and US nuclear submarines during their patrols. The media went berserk, the deniers lost all footing. And the Greens won the elections by a landslide.
]]

## Characters

### Protagonist

Dr Sigrid Blohm

born 1968
moved to Netherlands in 1980 12
moved to Sweden in 1986 18
Started PhD in 1991 23 
Post-doc 1995 27 
2020 Eza 25, Nur born
2024 56 Eza 29
2026 first results

2035 67 Eza 40, Nur 15

* An applied mathematician working in the IAOAS but collaborating with engineers and computing scientists; a low profile academic, not one of those research stars.
* Swedish, but Indonesian, family moved from the Netherlands to Sweden in the 70s (so Dutch from Indonesian, maybe even Moluccan, descent in Sweden). Swedified their name from Bloem to Blohm.
* Lives in (a flat in) Helensburgh or Rhu, overlooking the Gareloch, ends up living in a flat in Nacka Strand (near Stockholm)
* about 55 in 2024
* Single at the start of the narrative, separated (boyfriend moved to Glasgow with her but couldn't get used to Scotland; or maybe it was the baby)
* Did her PhD after and Erasmus, at Glasgow Institute for Advanced Ocean and Atmosphere Studies
[[ Stefan did the gallant thing and moved with me. I should have known then that you can't build a life on gallantry, but I was young and in love. We still exchange Christmas cards.
]]
{{ I would like to have a passage where she mentions how uprooted she sometimes feels:
[[ I was born in Indonesia and we move to the Netherlands when I was twelve. When I was 17 we moved to Sweden, I went to uni there. That's where I met Stefan. In my final year I went to Scotland with Erasmus and I stayed on for a PhD.
When I go back to Indonesia, I don't belong there. When I go back to Holland, I don't belong there. Even when I go back to Sweden,  I don't feel I belong there anymore. But I don't belong in Scotland either, and certainly not after Brexit. A European person of colour, it doesn't get much worse. 
Not that I regret my choices. I am still very glad to have spent most of my live in Scotland. But I now  empathise more with the "somewhere people". This feeling is the price to pay for being an "anywhere person", and on some days it feels like a high price.

https://mikefrost.net/people-somewhere-vs-people-anywhere/


}}


### Aurora Foundation

Elías Mendoza, based in Mexico, but used to live in the UK. Jewish, late 40s, activist for good. From Costa Rica
His main role will be to do the media campaign, and I guess I can assume some contacts with e.g. journalists and maybe politicians. 
It will be a challenge to make him come alive.

### Crowdfunder

* Luzmila Moreno
* in the US or Canada
* Spanish-speaking but of Quechua descent
* Say 30 in 2024, and a woman

At least we need names for the team. The Canadian team is Roy and Stanley; the Scottish team is Sandaidh and Evelyn. 
latest iridium phone is 22 kbit/s upload (88kbit/s down);  1 min 480p = 22MB so 22*1024*8 kbits so 1024*8 s = several hours; uploading a picture of 220kB is 80 s, so small pics and voice it its.

The course of events: 
- the fundraisers gets going: one boat sails from  Bermuda to St-Kilda and back, the other goes the other way round. 
The trip from St-Kilda would be South, then West; the trip from Bermuda would be North, then East. Either way it could take up to a months with doldrums etc. It should be in June.
https://forum.oceancruisingclub.org/Topic4532.aspx
https://www.crewseekers.net/notices/sailing-across-atlantic-classic-blue-water-voyage/
https://www.morganscloud.com/2008/01/18/fall-north-atlantic-crossing-east-to-west/
https://forum.oceancruisingclub.org/Topic4532.aspx
https://astrolabesailing.com/2016/09/07/ocean-currents/
https://www.yachting.com/en-gb/news/ocean-currents-in-atlantic
https://climate.metoffice.cloud/amoc.html

Other routes for a campaign:
- XR
- Green parties
- Charlotte's cities
- Newspapers (as this is international)
- In Scotland, the "Climate Change People's Panel’"

### Lots of scientists:

{{ Let's say to keep things manageable I will have only three teams that are mentioned: Mirza at APEC, Li-Zhen at CCCma and Eza at UMCCRN
I think I can work on the assumption that this is not a centralised effort, but a kind of tree where everyone contacted by the protagonist goes on to contact others. There is then effectively no need for the protagonist to know all the details of the other teams.
}}

	- * Korea APEC Climate Center Busan, Mirza Hameed (m)
	[[ He was an expert in ocean cwould irculation models.	He found me one day out of the blue and set the direction of my career. He was teaching at a private university in the south of Japan. I visited him there once and fell in love with Japan forever. He came to Scotland later and we had good times hillwalking. 	But Mirza and his family moved to Korea, and he now had a large group at APEC]]
	- Malaysia: Eza (Fauzana Mohd) Shaarani (f)
	Who might actually be at some EU unit somewhere, but it would be  better [The Universit Malaya Climate Change Network (UMCCRN)](https://fass.um.edu.my/research-centre-amp-research-network) 
	She is from Sarawak
	Husband Norman, like Norman bin Musa who is from Butterworth in Penang
			
	- * Canada: a native Chinese Li-Zhen Lai (f). [[Did her PhD in Gent ; She at some point introduces the protagonist to a Belgian she did her PhD with and who is now doing a cheap air source heat pump coop]]
 Li-zhen chemo {{neoadjuvant chemotherapy, this is done early, *before* surgery, and surgery should be within a few months; also, surgery 1-2 months after chemo is optimal
but usually it is 4-6 cycles over 6-12 months; it can be shorter if the tumor shrinks enough. So let's say 2 cycles over three months.}}
May June July August September October

- Oman: Sarah Alkhanjari (f)  
- UK  (m) Lukasz?  
	- * Switzerland or Germany or Austria ()

We also need the people who are going to do the media campaign and influencing/lobbying, and the people of the Belgian coop that makes cheap air source heat pumps. 

And we need a small core of people


## Science

AMOC runs indeed *very* deep so using subs would really help
Also, it's just one part of a global system so it is not so strange to have global interest. 
https://en.wikipedia.org/wiki/Atlantic_meridional_overturning_circulation

https://en.wikipedia.org/wiki/Southern_Ocean_overturning_circulation


	"The East Coast of the United States would be one of the regions most affected by rising sea levels if the AMOC shuts down, he explained, because warming waters, which expand and increase sea level, would pile up there instead of flowing northward. Warming coastal oceans can also contribute to extreme heat waves over land and fuel more intense storms and rainfall. "
	"winter sea ice could expand as far south as England, and some regions of Europe would quickly dry out and cool by as much as 1.5 degrees Celsius per decade."
	“A lot of discussion is, how should agriculture prepare for this,” he said. But a collapse of the heat-transporting circulation is a going-out-of-business scenario for European agriculture, he added. “You cannot adapt to this. There’s some studies of what happens to agriculture in Great Britain, and it becomes like trying to grow potatoes in Northern Norway.”
	
	https://insideclimatenews.org/news/09022024/climate-impacts-from-collapse-of-atlantic-meridional-overturning-current-could-be-worse-than-expected/

[Submitted on 17 Jun 2024]
https://arxiv.org/html/2406.11738v1

Probability Estimates of a 21st Century AMOC Collapse

Emma J.V. Smolders, René M. van Westen, Henk A. Dijkstra

The collapse time is estimated between 2037-2064 (10-90% CI) with a mean of 2050 and the probability of an AMOC collapse before the year 2050 is estimated to be 59 +/- 17%. 

Physics-based early warning signal shows that AMOC is on tipping course
René M. van Westen https://orcid.org/0000-0002-8807-7269 , Michael Kliphuis, and Henk A. Dijkstra

https://www.science.org/doi/10.1126/sciadv.adk1189


## Submarines

Sub sensor tech

https://www.navylookout.com/royal-navy-submarines-and-non-acoustic-sensor-technology/


"""
Hunting without sound

Details of how the non-acoustic sensors work are sketchy but in broad terms, infrared or laser light is passed through the water being sampled by the probes. The absorption or refraction of light by tiny particles present in the water can be measured. The science of spectroscopy enables the detection of very low concentrations that may only be a few parts per billion. Small changes in salinity, temperature and density may also be measured. The sensors may take thousands of samples per second which require computers to process the stream of data to distinguish between naturally occurring phenomenon and the distinct signatures left behind by a submarine.

The wake of a submerged submarine naturally spreads out over time which offers a sporting chance of locating it in the first place. The sensors on a single submarine are relatively close together and likely to distinguish only between actually being in or out of the wake. The needs for multiple sensors arranged it different places on the hull is to confirm the wake is being detected and it not just one probe getting a false reading. The different shaped probes are likely optimised to sense different properties of the wake. Just as sonar propagation is affected by environmental conditions in different parts of the ocean, non-acoustic sensors may also be hampered by factors such as salinity, pollutants, strong currents and water disturbed by other vessels, particularly in shallow and busy littoral waters.
"""

And they can dive really deep, typically around 300-400 m "test depth", but that means they could go twice as deep

https://navalpost.com/how-deep-can-a-submarine-dive/

About where they are:

I’ve got to be a bit vague so apologies:

Imagine the oceans are split into 500 nautical mile (1.852 km) square boxes and each box is called a different name. The submarine gets sent to one of these boxes, that is the SSBNs patrol area, there maybe more than one area, a Submarine maybe told before hand to be in patrol box “Ford” by 1st May and in the next patrol box by 31st May. 
The CO won’t be told to patrol an exact point, just a patrol box.

He will be given his orders before hand, so no need for any communication during the patrol unless really necessary. (Communication can compromise a patrol). The orders will be handed down from submarine command and thought of by someone in a bunker in Northwood, using all the available intelligence to position the SSBN best; then as few as people as possible will know where. And again they only know the rough area the submarine will be in. You will find that the the Admiral in charge of Submarines has no idea where the SSBN is located because he doesn’t need to know.

The information of the patrol area is shared with the US Navy, and they share their patrol area with the UK

Onboard the submarine the only people that know which patrol areas are the Commanding O, the Executive O and the Navigator

https://www.quora.com/Who-makes-the-decision-on-where-the-UK-s-nuclear-missile-submarines-patrol-for-each-tour-of-nuclear-deterrence

## Crowdfunding and awareness

- start with an article e.g. in the Guardian; 
- have two small fundraiser events, big freeze and big heat or big flood, going on, one in say Orkney or Mull or Skye and one somewhere East coast of the US, but not NY; Nantucket looks OK, but how about Bermuda? And in Scotland, St-Kilda?
- have this go viral

For that to work it makes more sense if there is a fundraiser over here so have Elias spend half of his time at least here,

Also I need to imagine what these fundraisers would be and why they'd go viral. We could for example have one person on a raft in a lake and another in the ice climbing wall in Kinlochleven, "to illustrate what it would be like". Or it could be just a conventional "climb a hill" thing; or a circumnavigation

Ah: what about a synchronous navigation in two small sail boats, the trajectory spells out the messages in the GPS coordinates? All we need is two islands of the same size, and make a circuit of about 20 km diameter. And with problems with wind and weather this could become quite entertaining.
	
	‘Climate Change People’s Panel’
	https://www.parliament.scot/about/news/news-listing/scottish-government-must-do-more-to-engage-the-public-on-climate-change
	
	https://www.parliament.scot/chamber-and-committees/committees/current-and-previous-committees/session-6-net-zero-energy-and-transport-committee/business-items/climate-change-peoples-panel
	
https://www.theguardian.com/environment/2024/feb/09/atlantic-ocean-circulation-nearing-devastating-tipping-point-study-finds

https://blog.bermudians.com/2024/02/12/bermuda-faces-potential-impact-from-gulf-stream-collapse/


At least we need names for the team. The Canadian team is Roy and Stanley; the Scottish team is Sandaidh and Evelyn. 
latest iridium phone is 22 kbit/s upload (88kbit/s down);  1 min 480p = 22MB so 22*1024*8 kbits so 1024*8 s = several hours; uploading a picture of 220kB is 80 s, so small pics and voice it its.

{{But then she gets the news that Li-Zhen has breast cancer. She decides that her son is old enough to deal with his own problems, and that right now Li-zhen needs her more. She contacts Luzmila, who right away hatches a plan for a support rota for Li-zhen, even though she's at the other side of Canada. Li-zhen initially doesn't want this, saying she doesn't have the energy to meet new people; but Luzmila explaining that she doesn't have to meet them, if she wants it that way they'll be invisible, just do her chores.}}

Then something quite unexpected happens: just when Li-zhen is about to start chemo {{neoadjuvant chemotherapy, this is done early, *before* surgery, and surgery should be within a few months; also, surgery 1-2 months after chemo is optimal
but usually it is 4-6 cycles over 6-12 months; it can be shorter if the tumor shrinks enough. So let's say 2 cycles over three months.}}

{{
Some timing on this: let's say the results came out in March 2026, the discussion with Eilidh is in April 2026, then nothing happens on that front for 6 months. In that time, things go wrong. But that means there is a period between April 2026 and April 2027 where nothing happens. 

- Rob and Laura move in March and get a hard time in April 2027
- Li-zhen gets her diagnosis in early May 2027, her chemo starts early June and finishes after three months, i.e. in August; say surgery in October 2027.
- Stefan dies 14 May, on Himmelsfärds dag 2027. The funeral is 23 May (Sat) and Sigrid stays on a few days to be with Rob and Laura, say she returns on the 27th. Mirza should therefore be in Seattle end of May and be prepared to stay for three months. 

But I want all this to coincide with an uptick in public support. So that can only happen when the paper is out, which is a year later!
So that would mean that Rob moves in March 2027, and the sub has left on patrol 

- Assuming we start in March 2024. 6 months setup. 3x6-monthly fundraising targets. 
- First teams start working March 2025 and might get results early 2026. 
- Sub discussion is April 2026
- Other teams get their results end of 2026. Writing the paper, due diligence and all, say 3 months, so March 2027; and then publication, gets us probably to March/April 2027 for the preprint (end of 2027 for the actual publication). 
- 2nd funding round can start March/April 2027, so by end of 2027 funds are in
- Sub leaves on patrol say end of 2026
- The sub data are in by end of 2027, and we can therefore get them to the teams already by early 2028. 
Then a new funding round, and by the end of 2028 we get the updated results. Then again 3 month writing, preprint in April 2029, all done end of 2029; which means the story "now" is 2030? But we should maybe take into account elections, and make it 2035. So Eza would only move by then.
}}

- 3x6-monthly fundraising targets. 
- First teams start working March 2025 and might get results early 2026. 
- Sub discussion is April 2026
- Other teams get their results end of 2026. Writing the paper, due diligence and all, say 3 months, so March 2027; and then publication, gets us probably to March/April 2027 for the preprint (end of 2027 for the actual publication). 
- 2nd funding round can start March/April 2027, so by end of 2027 funds are in
- Sub leaves on patrol say end of 2026
- The sub data are in by end of 2027, and we can therefore get them to the teams already by early 2028. 
Then a new funding round, and by the end of 2028 we get the updated results. 

It is possible to globally crowdfund £10M but it is a tall order. It would be better to have many smaller ones, with targets of around £100K.
Also, there must be some way to pressurise the institutions into contributing. And of course there is no reason not to go for funding agencies, especially for the 2nd round.
}}


- 3x6-monthly fundraising targets. 
- First teams start working March 2025 and might get results early 2026. 
- Sub discussion is April 2026
- Other teams get their results end of 2026. Writing the paper, due diligence and all, say 3 months, so March 2027; and then publication, gets us probably to March/April 2027 for the preprint (end of 2027 for the actual publication). 
- 2nd funding round can start March/April 2027, so by end of 2027 funds are in
- Sub leaves on patrol say end of 2026
- The sub data are in by end of 2027, and we can therefore get them to the teams already by early 2028. 
Then a new funding round, and by the end of 2028 we get the updated results. 

It is possible to globally crowdfund £10M but it is a tall order. It would be better to have many smaller ones, with targets of around £100K.
Also, there must be some way to pressurise the institutions into contributing. And of course there is no reason not to go for funding agencies, especially for the 2nd round.
}}


{{ so assuming a patrol of a year, by October 2027, the accurate data are available }}
[[ XXX ]]

[[There is the issue of security, because the data will need to have very precise coordinates and dating. So it will be clear where the submarine has been when. But that is hardly a secret, as it is well known that we patrol on boxes of 500 nautical mile square. And the data only needs to be correct inside the AMOC, right? So we could fake the rest to obfuscate the trajectory." ]]

{{
We maybe need some backstory on the captain, because a man in his position usually would not go against orders. A possibility is that he has had an argument with his son who works for an environmental NGO (can maybe even hint at XR).  The father accused him of being a treehugger. The son retorted that all his defense was no good against climate change. Something along those lines.

}}

{{

But how do we make this interesting? There are a few possibilities
(1) A leak: someone has found out about the data and leaked it; but that is rather unlikely: how is anyone to know? 
It could be that someone is suspicious and does an FoI request but I don't think that would fly. Then they might try worse
(2) A break-in: e.g. someone has broken into the computers of one of the teams; or even more sinister, into the house of one of the core team members.
The hacking could be rival teams, that is not too bad; but it could be worse -- but why? We could of course have a narrative of disinformation, seeking to discredit the work. That might work. It could involve trying to claim some wrongdoing by one of the team. 
(3) Complications because the dataset is richer than before and incorporating it in the sims proves more difficult, giving rise to strange errors
(4) Political trouble/Organisational trouble

So we could somehow combine all of the above: 
- a reaction starts, with a disinfo campaign and a smear campaign
- there are FoI requests but they are of course rejected
- people try to hack into the research computers, with little luck -- but not because the data is so secure, more because the research is not very organised.
- then a rumour starts that the data are faked. Luckily, the FoI protection means there is no way Glasgow has to give access to the data or even discuss the origins.
- we could even have one of the team doxxed; it would almost have to be Sigrid herself I think, or Li-Zhen. I can't see it working for Mirza or Eza. But they could get targeted in different ways. For example, scary, could Eza's office be burgled? Same for e.g. the Australian or Chilean team? 
It is possible and would add an extra dimension

In any case, I will have to write about how Sigrid deals with the degree of fame she is getting. Because media attention and interviews means also attention of the gutter press etc. 
Key question is, is she getting doxxed/harassed or not? 
Maybe it's not quite that level of fame, isn't it? 

"Until yesterday," Eza said dryly. More raised eyebrows all round. "I was about the let you all know but you beat met to it with this unexpected meeting. I got an anonymous phonecall at work, somebody harrasing me, although it wasn't clear why or what about. I've alerted University security but of course they couldn't trace the call. From now on I will have all my calls routed through a secretary who'll do some screening."<br>
"I'm very sorry to hear that, Eza, " I said, "I hope that will put a stop to it. It looks like we really have annoyed some people. So what do you all think of this proposal?" <br>

}}
Three years, two iterations
### World, May/June 2024 -- the fundraising action
### Helensburg,  September/October 2024 -- preparations for 1st iteration 
### Glasgow, February 2026 --  first prelim results for 1st iteration
### Glasgow, March-October 2026 --  bugs in the code
### Glasgow, March 2027 -- official results for 1st iteration
### World, March/December 2027, start second round of funding => this would be mostly around the final publication
### World, March/April 2027, preprint publication
### October 2027 Li-Zhen recovered, final paper for 1st iteration published