#!/usr/bin/env perl
use v5.30;
use warnings;
use strict;

my $md = $ARGV[0];
my $md_base = $md; $md_base =~s/\.md//;
my $md_for_tex = $md_base.'_for_tex';
open my $MD, '>', "$md_for_tex.md" or die $!;

open my $IN,'<', $md or die $!;
while(my $line=<$IN>) {
$line=~s/\<br\>\<br\>/ \\bigskip /;
$line=~s/\<br\>/ \\par /;
print $MD $line;
}
close $IN;
close $MD;
