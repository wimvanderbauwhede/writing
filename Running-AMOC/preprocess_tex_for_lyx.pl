#!/usr/bin/env perl
use v5.30;
use warnings;
use strict;

my $tex = $ARGV[0];
my $tex_for_lyx = $tex;
$tex_for_lyx =~s/tex/lyx/;

# Add the preamble
open my $PRE,'<', 'running-amoc-v3-for-preamble.tex' or die "$!";
my $preamble='';
while(my $line=<$PRE>) {
$preamble.=$line;
}
close $PRE;

open my $TEX, '>', "$tex_for_lyx.tex" or die "$!:$tex_for_lyx";
say $TEX $preamble;
my $skip=1;
open my $IN,'<', "$tex.tex" or die "$!:$tex";
while(my $line=<$IN>) {
    $line=~s/\\bigskip/\\\\\n\~\n\n/;
    if ($line=~/\\section\{/) {
        $skip=0;
        say $TEX '\newpage{}'."\n";
    }
    print $TEX $line unless $skip;
}
close $IN;
close $TEX;
