---
title: Running AMOC
author: Wim Vanderbauwhede
---

<!-- # Part I. Spring 2024 &mdash; Spring 2026 -->

# The science

AMOC, short for the Atlantic Meridional Overturning Circulation, is the system of currents in the Atlantic Ocean. It is also known as the thermohaline circulation because it is driven by differences in temperature and salinity of the ocean waters. The Gulf Stream which keeps Northern Europe warm and cools the Eastern seaboard of Northern America is part of it.

With the pack ice melting due to global warming, the salinity of the Atlantic drops, and at some point AMOC will therefore collapse. This will lead to Siberian winters in Europe, and also increased aridity which will drastically reduce crop yields. Large parts of Northern Europe may become uninhabitable.

The AMOC collapse will also lead to a more rapid and stronger rise in sea levels. The East Coast of the United States would be  most affected by this, because warming waters, which expand and increase sea level, would pile up there instead of flowing northward. Warming coastal oceans would also result in more extreme heat waves and fuel more intense storms and rainfall.

But AMOC is a part of a global system of circulation so the effects of its collapse will be global as well.

The most recent estimate for the collapse [1] is some time between 2037 and 2064, so urgent action is needed.
<br><br>
[1] "Probability Estimates of a 21st Century AMOC Collapse", Emma J.V. Smolders, René M. van Westen, Henk A. Dijkstra, https://doi.org/10.48550/arXiv.2406.11738

# Chapter 1. Our travels take us further North

> *You must be old, so take it easier* (NFADS, "Stockholm")
<br><br>
<!-- about 750 words -->
<!-- ### Stockholm, March 2035. The move -->

_Stockholm, March 2035_

I'm sitting in my new favourite rocking chair, looking over Saltsjön towards the lights on Blockhusudden on an early night late in March. The windows are still spattered with raindrops from the shower that just passed through but the sky is clear and there are even a few stars out.

Eza is at work; I hear the intermittent click of her mouse and the occasional bursts of muted keystrokes, echoing the earlier sound of the raindrops hitting the glass. Nur is busy in the kitchen, frying garlic, ginger and spices. Their aromas fill the room. Soon she'll have a delicious meal ready.
<br><br>
Some years after the final results were published, but long before the dust had settled, Eza told me of her impending move to Sweden. She had been offered a position at the KTH Climate Action Centre. It was a fantastic opportunity but at very short notice, so finding a place in Stockholm would be hard. Her husband wouldn't be able to join her for quite a while, but Nur would come with her.

I had been away from there for so long. My parents had been dead for years, Stefan had died as well and my son Rob and his partner had moved to Australia, so I really didn't have any ties to Sweden any more. But suddenly I had this vision of living with Eza and Nur in a cosy flat in Stockholm. Out of the blue I was struck by a violent attack of homesickness, so strong that tears sprung into my eyes. I had been living in Scotland for forty years and never before had something like this happened to me. (No, that wasn't quite right. Once, eight years ago, when I was back in Sweden for Stefan's funeral, I had felt something similar, but I had pushed it away, and now it was back with a vengeance.)

Even though it was a video call, Eza immediately noticed something was amiss. "What is it, Sigrid? You look shaken. Do you think it's not a good idea?"<br>
I composed myself a little "Of course it's a good idea, Eza, it's wonderful and I am very glad you got offered the opportunity. I was just thinking about how you would love Stockholm and suddenly I felt so very homesick. I wish I could go back to live in Sweden, with you and Nur".

Eza's expressive eyebrows went up, her face became a study in surprise, wonder &mdash; and delight. "Would you really like that? What a wonderful idea! But shouldn't you think it over a bit?" There was a slight hint of worry in her voice.
<br><br>
I would have been worried if I was her: this was totally unlike me, but I'd never been so confused in my life. So I did think it over, carefully, over the course of the next weeks. It didn't change my mind. If anything, my desire to go back grew stronger. So next time I spoke to Rob I proposed to buy his flat in Nacka Strand. It would be perfect. Rob surprised me by not being surprised. "You know, mum, ever since dad's funeral, I've been wondering when you'd move back. It was pretty clear you were homesick that day when we went back to Blockhusudden".  <br>
Dear me, I thought, I must be really transparent. I smiled. "And I thought I'd managed to hide it so well... I was never homesick until that day, but apparently it's been with me ever since."<br>
"Well, it's perfectly fine for us. Laura and I like it here Australia, we have no plans of going back, so of course you can have the flat." <br>
We quickly agreed a deal; I had a solicitor draw up a contract for shared ownership with Eza, got the University to give me a year's unpaid leave, organised my move and canceled my rent.<br>
I had never bought a property. I'd always put it down to "too much hassle", but maybe subconsciously it had been a way of resisting stronger ties to Scotland. Anyway, less than half a year later, here I was, back in Sweden after all this time. I could practically see our old home through the window.
<br><br>
<!-- ### Stockholm, March 2035 -- lead-in to how it started -->
The faint hum of the air source heat pump is soothing. It's made by a Belgian co-op started by Li-Zhen's friend Paul. They found a way to make them cheap, reliable, and easy to repair. I met him once and he had told me the story of how he and Li-Zhen met during their PhD ("One day, I was working in the clean room, babysitting the machines, and someone waved and smiled at me through the window. I looked up from the computer and there she was.") and the very unlikely events that had led to him learning the high-tech plumbing skills that he now used in the business. ("You wouldn't believe how poor their safety arrangements were. And in my youthful naiveté, I decided that Something Had To Be Done about that. Of course I ended up having to do it, rather than doing my PhD research.")

It was Li-Zhen's team who had first managed to confirm the results of the groundbreaking 2023 work by the Ditlevsens and then narrow down their estimate. Soon after, Mirza's teams had followed suit, and the others after that. It was amazing how well things had worked out.

But it all started with that article, and I still vividly remember the day I read it.

# Chapter 2. There's a lot of opportunities

> *If there aren't, you can make them* (Pet Shop Boys, "Opportunities")
<br><br>
<!-- about 1,850 words -->

<!-- ### Helensburgh, March 2024 -- how it started; the AMOC paper; the idea -->
_Helensburgh, March 2024_

It was a day very similar to today, cold early spring weather &mdash; or late winter; in Scotland, winter has a long arm. It was my habit to set a morning aside every week for reading papers, a very enjoyable activity: easy chair at the window, nice cup of coffee, looking out over the rain-swept Gareloch. That morning, I didn't get very far. The Ditlevsen paper shook me. Not that it was a new notion, the possibility of collapse of the thermohaline circulation had been raised already in the late nineties. But the confidence levels on the estimates in this latest work were really high, and I trusted the authors. It was a wake-up call.

And yet, it would not be enough to move the world into action. Politicians were bound to assume the best possible case, which was 2095. The public would have forgotten the news tomorrow.

I didn't know what had come over me. I had never felt the need for direct action, feeling that I was doing my part already. But this time, I couldn't just move on. Maybe, getting older,  I had gotten more radical than I realised. I felt strongly that this was a once-in-a-lifetime opportunity. The threat was very real and the consequences would be quasi-immediate, widespread and devastating. Without action, people would literally freeze to death. Huge areas of land would become too dry to farm. All that was needed was for the public to accept what the science told us. Then politicians would have to follow suit, else they would lose out.
So I got thinking about how we could make them listen. I felt it could be done. After all,  they had listened to the scientists in the pandemic, and thanks to decisive actions on vaccination, masking, testing and ubiquitous air filtration and ventilation, the virus was now a thing of the past.

What was needed most of all was a more precise timing of the AMOC collapse, with a scientific consensus. The work would need to be done by many different teams that would share best practice but keep their own models and methodologies. The results would be published in a joint paper.
The top teams would likely not be interested, although it wouldn't harm in asking them. There was always the off-chance that one of the big shots prioritised the greater good over their career. Anyway, I was fairly confident that using my network I could build an international consortium of five to ten teams that were good enough to do the job.

The main problem would, as always, be funding. Research funding agencies were unlikely to fund this project because they don't react to emergencies. They would simply say "this is currently not a priority" and add "but you can always go for an open call"  &mdash; with a success rate of four percent and a processing time of at least a year, but they didn't need to spell that out. Also, national agencies usually only provided grants for their own teams, and the risk of not getting funding for one of the international collaborators would almost certainly kill any such proposal.

So we'd need different sources of funding. That would mean crowdfunding; it would be a very tall order, but I could see no other way. Assuming every team had two postdocs and we needed funding for three years (that should be enough for two iterations), we'd need about six million pounds. Hopefully some of the teams would have other sources of funding, but we's still need to raise several millions.
<br><br>
For crowdfunding to work, lots of people would have to be made aware of the AMOC collapse and its consequences, and what could be done about it. We would have to spread a message of urgency as well as hope. And it was all a gamble: with the earliest date for the collapse set at 2025 by the Ditlevsen paper, the whole effort might come too late. Still, chances were that we had a few more decades, so we had to make the most of a very short window of opportunity. I had to get cracking, and soon.

I realised how poorly I was suited for this kind of project: I, an introverted, low-profile academic, a middle-aged woman of colour, must be one of the most unlikely people to start a successful international large-scale fundraising activity and media campaign. Luckily, I knew at least one person who I thought was much better suited, and might be up to the task. And he might know others.

Anyway, I decided to start within my comfort zone, by building the research consortium. I've always thought that a rather grand term for what is hardly more than arranging some chats with a few friends. For this particular research, there were three scientists I knew well who could do it: Li-Zhen Lai, Mirza Hameed and Eza Shaarani.
<br><br>
<!-- ### Helensburgh, March 2024 -- Li-Zhen, Mirza, Eza -->
Li-Zhen was of Chinese origin, a solid state physicist by training. She had done her PhD in Belgium and I had met her at that time, years ago, at a conference where she presented a paper co-authored with her mother, who was an atmospheric scientist. They had repurposed a semiconductor metallisation simulator to solve the Navier-Stokes equation. It was a _tour de force_. She'd explained how that came about: "Because I am Chinese, they really didn't take my PhD seriously. I had to do way more odd jobs than the other PhD students, and hardly got any supervision. But it also meant they left me alone, so nobody questioned me when I used the modelling workstations in the evening to work on mom's project."  On the strength of that work, she got offered a postdoc at the University of Ottawa, and she now led a team at the Canadian Centre for Climate Modelling and Analysis at the University of Victoria.

When I called her, Li-Zhen was her usual force-of-life self, glowing even on Zoom, her strong-boned features softened by the Canadian morning sun, her black hair shining with a reddish halo. She was brimming over with ideas and enthusiasm. It was almost as if she'd been waiting for my call. "As soon as I read that paper, I started thinking of a confirmation study. I have some ideas for other teams to involve; I even think I see my way to get my institution to fund part of the research. It wouldn't be enough though, so getting the funding sorted is the key priority." <br>
I explained my idea for a media campaign and a crowd funder. She considered it briefly, nodded and said "I might know a few people who could help, I'll put you in touch with them."
<br><br>

Mirza was an Indian expert in ocean circulation models who had emailed me one day out of nowhere with a question about acceleration of  numerical models and, in doing so, had inadvertently switched the direction of my career irreversibly onto the track of efficiency of computational models. That had led me to work with engineers and computing scientists all over the world, collaborations which I found very rewarding even though they had proven hard to fund and publish. Despite all the talk about the importance of interdisciplinary work, funding agencies and journals are rather insular. Still, I had no regrets.

At the time, Mirza had been working at a small private university in the south of Japan. I had visited him there once for a few months in summer and had fallen in love with Japan forever. He in turn came to Scotland and we had great times hillwalking. Later, he and his partner moved to Korea, and he now had a large group at the APEC Climate Center in Busan.

Talking to Mirza was always slightly odd, in a hard to define way. He looked rather gaunt with prematurely greying hair, had an intense stare and was slow to smile. Despite his important position and academic reputation, he was always very careful not to push any agendas of his own, and yet in the course of a conversation we would generate lots of new and often crazy ideas. He carefully assessed the options for my proposal, and said "I suggest it might indeed be good if I could work with other teams, but for practical purposes it might be better to keep the efforts decentralised and loosely coupled."<br>
He also pointed out the politics of the situation. "There are many countries, especially in Asia, that think they would benefit from an AMOC collapse, as it would weaken the US and the EU considerably." As a climate scientist, and one who had pointed out the connection between the El Niño Southern Oscillation and the Indian Ocean dipole, he of course knew better. "I think the message about AMOC collapse should have a strong global dimension, making it clear that everybody would lose out."<br>
"I don't think APEC will fund this work, and I can't commit to it unless there is another source of funding. But I will approach two other teams. There's one in Japan and one in Australia. They might have a more favourable funding situation."
<br><br>

Finally, I called Eza. I had gotten to know her while she was doing her PhD at our Institute. She was Malay, from Sarawak, so we spoke the same language, and we found that we had a lot more in common besides that. She had a baby, wee Nur, during that time. Her mom came over to help her in the first few months, but after that I often helped out. I knew from personal experience how hard it was combine a PhD with looking after a baby. At least her husband was doing his part, but he also had a full-time post-doc job. So I became a bit like an adopted aunt, and she became one of my best friends. We had maintained a close bond after she returned to Malaysia.
She now was a Professor at the Universiti Malaya and lead their Climate Change Network. She specialised in ocean transports around the Maritime Continent.

Meeting up with Eza always felt very cosy,  maybe because we spoke Iban together. She also had a very warm personality and a knack for making me forget my troubles.  She looked the way I'd always wanted to look, every feature just a little more defined than mine: eyebrows a bit stronger and much more expressive; nose a little straighter, mouth a little wider; and of course twenty years younger.

As always, she was very practical: "I have already thought  how I could justify working on AMOC as an extension and validation study for the techniques used in my main research on thermohaline structure of ocean transport in the Southeast Asian Seas. It would bear out the claims on its effect on the global climate. Funding is actually not a problem: Malaysia has just received close to a hundred million dollars from the Global Environment Facility, and part of that funding has already been allocated to the Climate Change Network. I have worked with a team in Oman on circulation in the Arabian Gulf. The Sultanate has realised the importance of the ocean for their economy, and the inevitability of climate change, and consequently it is one of their priority areas for funding. So they might have a good change of getting a grant for a follow-on study."
<br><br>

"You're amazing!"
<br><br>

They all were. I marvelled how lucky I was to have such friends.

# Chapter 3. The wheel is turning

> *We're not sleeping at the wheel* (Ladytron, "Soft Power")
<br><br>
<!-- about 2,300 words -->

<!-- ### Helensburgh/Glasgow, April 2024 -- blog post -->

_Glasgow, April 2024_

Even after all these years, the commute by train from Helensburgh to Glasgow is still one of my favourite journeys. The views over the Clyde estuary are fabulous: the wide water, the rolling hills on the far side topped with wind turbines; the Dumbarton rock, the old dilapidated harbour at Bowling with the carcasses of old boats slowly degenerating. Even the Erskine bridge, that monument to car culture, is an impressive and arguably even elegant structure. The scenery and wildlife change every day with the tide, the weather and the seasons. On the return trip, the views towards Bute are amazing too. And in winter, the sunsets are often spectacular.

But that day, I had no eye for all that beauty. I was busy writing a short article for my long-neglected blog, explaining the need for action on AMOC: that we most likely still had time to stop it, and why in any case we needed more accurate modelling of the collapse. I had started an outline the night before, and the undisturbed half hour on the train was exactly what I needed to get a first draft ready. I walked from Partick station to the Institute on autopilot, mentally making edits, a rare thing for me. Without even bothering to check my mail, I dove straight in and finished the post in another hour. At least now I had something to share with the general public. I made sure it was prominently linked from my University web page and pinged Elías. Then I went to the Common Room for a belated cup of coffee. I decided to go for a latte, mostly because I enjoyed the ritual and found going through the steps of the process relaxing. While steaming the milk I thought back to my chat with Elías and Luzmila yesterday.
<br><br>
<!-- ### Helensburgh, April 2024 -- Elías and Luzmila -->
I had felt quite energised after talking to my friends about getting the project started. Things were looking a lot better than I had ever dreamed.
As expected, we would still need to find a considerable amount of funding, but less than I had expected. Still, although for an international research project a million was a modest budget, for crowdfunding it was a huge sum.
That kind of fundraising was totally out of my line, but luckily I had more friends. Specifically, my old friend Elías Mendoza, a lifelong activist for good and a seemingly inexhaustible source of positivism and energy. I always got a buzz from chatting with him; he made me feel like we could handle anything.
I had met him years ago at an online event, and in fact we had never met in person. But we had hit it off right from the start. He was in his forties then, one of those people on who being bald looked good, with a neat short beard and a charming engaging smile. He was an endless source of great ideas and initiatives but totally humble at the same time. Elías listened carefully to my rough outline of the project, asked a few pertinent questions, and said "I think I could arrange an opinion piece for you in the Guardian."<br>
I was at the same time delighted and dismayed: that might very well be the end of my low profile. But probably only for a short while, the news cycle moves very fast. And it was definitely one of the best ways to get widespread attention. "That would be fantastic. Thank you so much!"<br>
"Not at all. In the meanwhile, you write that blog post and I'll share it with anyone I can think of: the Greens, XR, Greenpeace, all the usual suspects."<br>
This is why I wasn't an activist. As Elías had assumed &mdash; he knew me too well &mdash; I had been thinking about a blog post, but I hadn't written anything, let alone promoted it. Coming from him, promotion would work so much better. It was almost like having an agent.<br>
He continued: "We should also talk to my friend Luzmila Moreno. She's a community organiser and crowd funder from Nova Scotia, that will give us some traction on the other side of the Atlantic. You'll like here. She's Quechua".
<br><br>

<!-- ### Helensburgh/Glasgow, April 2024 -- sailing the gyre -->
Elías  was right, I did like Luzmila right away. She was young enough to be my daughter and with a bit of imagination she could pass as Rob's elder sister. Her enthusiasm was infectuous. She already knew of the AMOC story, and was really  keen to help. "I have this idea for a fundraising activity that would be a great fit for AMOC: it's to sail along the current from Bermuda to Europe in a ten-meter sailboat." <br>
I thought this was a great idea: Bermuda was an important point in many AMOC  studies, and it would indeed get a lot colder when the collapse happened. <br>Elías said, "We should do it in sync, with another boat going in the other direction".<br>
"Great idea!", I nodded enthusiastically. "I would go for St-Kilda as the other point, because the Outer Hebrides will be particularly hard hit by the collapse. One boat would follow the Gulf Stream and the North Atlantic Drift; the other the Canary Current and the North Equatorial Current. It would be perfect. And the aim should be for the teams to finish as close together as possible.	But it will be quite a challenge. Who is going to do this?"<br>
Luzmila replied, "I've already found a couple who want to do this on my side. But I don't have any contacts in Scotland". <br>
"Me neither," Elías admitted. We sat silent for a few moments, trying to think how to tackle this, but no luck. As always, Elías  remained optimistic. "Anyway, we should set up the crowdfunding site first thing. I'm sure we'll find a boat on the Scottish side." <br>
I wasn't so sure, but once again, he was right.
<br><br>

<!-- ### Glasgow, April 2024 -- Esther, the 2nd team -->
I was trying to convince the machine to grind the coffee beans for my latte when Esther came in. She was one of my favourite colleagues and I hadn't seen her in a while.  She also happened to be the Head of the Institute. I generally felt awkward with chitchat, so my usual approach to contact in the Common Room was to keep it to a minimum. Nod, wave, hurry off. Which was a bit of a shame, as it was a nice airy room with white walls, tall windows and comfy seating.<br>
But I always had a chat with Esther,  so I sat down on the semi-circular sofa near the window and waited for her to join me. There is a row of magnificent beeches in front of the Institute, and the bright spring sunlight reflecting on the  new leaves daubed the walls of the room with pale green.

While she was brewing her tea, I suddenly remembered that she had mentioned once that they had a boat at Rhu marina. I hadn't paid much attention at the time as sailing was not my thing, and it had totally slipped my mind, but now I couldn't believe my luck! After the most perfunctory of long-time-no-see-how-have-you-beens, I came straight to the point "Esther, I can't believe it: only yesterday I was talking about not knowing anyone in the sailing community, and here you are! You are in the Helensburgh Sailing Club, aren't you? You could do me a great favour!" and I explained the whole idea.
Esther was of course more than happy to help. Everyone in the Instituted knew about AMOC, and was low-key worried about it.  She agreed to post the idea on their facebook, and within days, things started to move. The post made its way to various sailing forums, and soon a young team got in touch, explaining they'd read my blog post and thought it was something they wanted to help with. They had wanted to do something like this for a while and had even been strengthening their boat to deal with the harsher conditions of the Northern Atlantic.
<br><br>

<!-- ### World, May/June 2024 -- the fundraising action, going viral -->
_The Northern Atlantic, June 2024_

While we were planning the particulars of the action, Elías made good on his promise and got me my opinion piece in The Guardian. I positioned it as a follow-up on their original article on the AMOC collapse but did my best to make it a call to action. Elías and Luzmila were a great help, and I think the final submitted article was really pretty good. And luckily, the editors left it largely intact as well. We even managed to squeeze in a mention to the fundraising event.

Purely by chance, our timing had been fortuitous: the best time to undertake this kind of cross-Atlantic sailing trip is June. The Canadian team, Roy and Stanley, set out North from Bermuda to join the Gulf Stream near the Grand Banks, off Nova Scotia; the Scottish team, Sandaidh and Evelyn, were headed South from St Kilda to join the Portugal Current near the Galician Banks, off the north-western tip of Spain, and from there on to the lower part of the Atlantic Gyre.

<!-- the Canary Current that would take them to the Canary Islands, Cape Verde and then -->

Sandaidh and Evelyn had decided to stay close to land and visit a number of British ports so I went to meet them in Oban, a great excuse for a trip on the beautiful West Highland Line. I was really surprised by the reception: there was a small crowd with colourful banners and the action even made it into the venerable Oban Times.


Luzmila and Elías had done a great job with the crowdfunding site, and the teams themselves already had a small but dedicated following, but we weren't really prepared for what happened next (or at least, I wasn't; I'm sure Elías had been counting on it from the start). The action went viral. From a niche fundraising event followed by a small but dedicated group of enthusiasts, overnight it became a global sensation. The teams became instant celebrities, and a host of volunteers appeared out of nowhere, keen to help with everything from creating a dedicated "STOP THE FREEZE" web site, to moderating the comments on the public log book of the teams. I had thought the Guardian article had given us a lot of exposure, but it was nothing compared to this. In the first week of the fundraiser we had already exceeded our three-month target, and when both journeys along the gyre where finally completed, we had a budget of ten million, an amount I'd never thought we could reach, and which would even allow us to fund part of the second iteration of the project.

Paradoxically, the fact that both teams had very limited bandwidth seemed to help with their popularity. As I'd learned,  even a state-of-the-art Iridium satellite phone only has 22 kbit/s upload speed. That is plenty for voice and plain text, but posting pictures takes longer and video is out of the question. So in the morning, each team had a  voice call session, and in the evening they'd post a few pictures. Somehow, the more frequent small text messages they'd post in between really appealed to the audience. The calls between both teams proved very popular as well.

So they garnered a huge following, but they were still alone and far away from anywhere; and it was abundantly clear to the audience that the ocean was no laughing matter. I think it was this position of isolation and fragility that helped them get the message about the threat posed by the AMOC collapse across so effectively.

I, who before hadn't had the slightest interest in sailing, was instantly hooked. I worried about them when they got into a storm, felt for them in the doldrums, shared their anger at the pollution and delighted with them in the beauty of it all: lush islands and schools of flying fish for Sandaidh and Evelyn; rather more austere with icebergs, seals and whales for Stanley and Roy, and magnificent clouds and gorgeous sunsets all around.
It was a riveting adventure; fortunately, there were no major mishaps  and both teams arrived a their destinations after about a month and within a few days of one another.
<br><br>

<!-- ### Helensburgh,  July/August 2024 -- hosting Stanley and Roy -->
_Helensburgh,  August 2024_

Before sailing on to France where they had friends, Stanley and Roy stayed with me for a few weeks. They were a delightful couple and somehow fitted so seamlessly into my life that I was really sorry to see them go.
It was a decent summer (for Scotland), and we went for long walks together, exploring the hills and moors. The orchids were out in myriads and the cottonsedge was like clouds of candy floss floating over the grasslands. It was idyllic, and I couldn't help but thinking what would happen to this beautiful land when the AMOC collapsed. Luckily, it was impossible to be gloomy in their company. They took me sailing, something I'd never done before, and it was a magical experience.
But after a month had flown by like the sparrow flying through King Edwin's hall, it was time for them to move on, and on a fair morning in early August, the tide was right and they sailed off into the distance. I stood on the pier at Rhu long after their gennaker had disappeared around the Rosneath peninsula.

While walking home along the shore of the Gareloch, I mused that, even if the project would lead to nothing, at least on a personal level I was already much the better for it.
<br><br>

<!-- ### Helensburgh,  September/October 2024 -- Oops -->
_Helensburgh,  October 2024_

A few months later, with the practical organisation of the project in full swing, my friend Elilla sent me a very recent preprint of work by Dijkstra. He and his group at of Universiteit Utrecht specialised in the inter-relations between climate subsystems. The paper was "Probability Estimates of a 21st Century AMOC Collapse" and I admit my heart sank a bit when reading that title: if they had managed to narrow down the estimate considerably, the work of my teams might be obsolete before it even began. And it would be much harder to shift the public opinion with only this preliminary work by one group.
So in a way I was relieved that their estimate only narrowed the Ditlevsen estimate to 2037-2064. It was a very important and useful piece of work, but a lot more research was still needed, and that's where our teams came in &mdash; or so I devoutly hoped.

# Chapter 4. Accuracy

> *an observer's refrain* (The Cure, "Accuracy")
<br><br>
<!-- about 1,050 words -->

<!-- ### Stockholm, March 2035 -- remembering first results -->
_Stockholm, March 2035_

Nur was an excellent cook, even though she was only fifteen. She certainly hadn't learned it from Eza, who didn't enjoy preparing food and usually went for quick and easy meals. But Norman, Eza's husband, simply loved cooking. I liked Norman a lot. He was a civil engineer specialising in urban regeneration projects in Malaysia and so he was still over there, planning to move when his major projects had finished. He was a Malay Chinese from Butterworth in Penang. They had met early in her PhD, when he was doing a postdoc at Strathclyde. They'd married soon after. I had gotten to know him quite well when I helped them looking after baby Nur.
He was a rather atypical husband as he did his share and more of the housework and childcare; and he was a fantastic cook and a dedicated teacher. He had even taught little Nur to speak Penang Hokkien. I knew she missed him a lot.

It was my turn to do the kitchen cleaning up. I loaded the dishwasher and turned it on. As usual, after the initial gurgling sounds, it started to sing, some rather pleasing harmonic resonance. I started on scrubbing the hob. But Nur not only knew how to cook, she was very tidy as well, so it was done in no time and I moved to the washing up of the cutting board and knives. Doing the dishes always makes my mind wander. I thought back to that exciting period when the first results had started to come in.
<br><br>
<!-- ### Glasgow, February 2026 --  first results -->

_Glasgow, February 2026_

It was early in 2026, barely two years since that fateful winter morning in Helensburgh, when Li-Zhen's team at Canada's CCCma managed to confirm the results of the seminal 2023 work by the Ditlevsens and the follow-on work from 2024 by Dijkstra, then narrow down their estimate. Soon after, Mirza's team at the APEC Climate Center Busan followed suit, and then it was like crocuses in spring, everywhere confirmation studies started to crop up: a team led by Eza in Malaysia and Sarah in Oman, using the ICON model as their starting point; teams from Japan and Australia working with Mirza, and from Chile via Li-Zhen, each with their own models. Rather to my surprise, even the Mofem-based model led by Glasgow managed to deliver the goods. Not that it was a bad model or a poor team, on the contrary, but Mofem was not a circulation model in origin. Its use as a climate model started out with a simulation of the fictional planet Arrakis from the Dune universe, and that had stimulated researchers to extend it and try it out for other planetary simulation purposes. What this meant was that the codebase was very different from the other circulation models, so the fact that it returned compatible results greatly increased the confidence in the project.
<br><br>

<!-- ### Glasgow, March-October 2026 --  bugs in the code -->
_Glasgow, October 2026_

By autumn of 2026, there was almost complete consensus. Only the team of Eza and Sarah had a significantly different result, with an earlier collapse but a much larger uncertainty. We discussed what the reason could be. Optimising and parallelising simulation code was my expertise, so I agreed to get hands-on with the codebase, in extended debug sessions with the researchers who wrote and maintained the code. As often with this kind of simulation code, it was built on a core of much older code that had proven reliable and accurate in the past. At first, we left that code alone, as it was more likely that the bug was elsewhere. But we didn't get anywhere: everything else seemed just fine, we couldn't find any bug that could have caused that discrepancy. So after a few weeks, we reluctantly tackled that crufty old code anyway, metaphorically rolling up our sleeves and getting our hands dirty. It was truly a mess  (the term "spaghetti code" didn't come even close) and it took us weeks to just start to understand how it worked. The postdocs started to look tired. I suspected they kept banging their heads against the problem long after I'd gone to bed. I told them to look after themselves, but I knew only too well that sometimes programmers can't find peace until they've cracked the problem. One of the perks of age is that you find it easier to let things go.
But then suddenly, one beautiful day, the problem became obvious. Long ago some forgotten scientist had taken a shortcut to make the code easier to parallelise, and so introduced a potential convergence problem. For years it had gone unnoticed, until it cropped up in our complicated AMOC model. And we only really became suspicious because it did not match the results of the other teams. On its own, the results it produced were plausible enough. We were over the moon when we'd finally figured it out. Fixing the bug was comparatively straightforward, meaning that it took only half as long as finding it. And so a few months later, when the daffodils where just starting to show their petals, the model's result were finally in agreement with those of the other teams.
<br><br>

<!-- ### Glasgow, March 2027 --  results, lack of accuracy -->
_Glasgow, March 2027_

We had narrowed the AMOC collapse down to 2035-2055, with better than 95% certainty, significantly better than any other estimate published so far and with an unprecedented degree of consensus. It was as good as it ever got.
<br><br>
But it wasn't good enough.

To be convincing, the uncertainty would have to be a few years rather than decades. The problem was the accuracy of the observational data. Measuring the AMOC was difficult: the currents flowed really deep in some parts of the ocean, and for accurate modelling, many measurement points of temperature, salinity and density were needed, all across the length and depth of the currents. That required a research ship with a diving bell that could go really deep, and that was hard to come by and very expensive. Plus, the old refrain, it was not a priority so getting funding for it was effectively impossible. It was a serious conundrum.
<br><br>
Eventually we did get the measurements, and that's how we got where we are now: scientific consensus that, unless emissions are cut dramatically, AMOC will collapse in 2045 with a window of just one year on either side and still better than 95% certainty.
<br><br>
That had been more than a tall order. In fact, it had been pure luck.

<!-- # Part II.  Spring 2026 &mdash; Autumn 2027 -->
<!-- chapters 5 and 6;  -->
# Chapter 5. Atomic

> *Your hair looks beautiful, tonight* (Blondie, "Atomic")
<br><br>
<!-- about 2,750 words -->

<!-- ### Sigrid's past ; maybe this is Stockholm 2035? -->
_Stockholm, March 2035_

I was born in Kalimantan, in Indonesia. My father was Indo, my mother Iban. For as long as I could remember, my parents had been set on moving to the Netherlands as soon as they could afford it, and they named me Sigrid, a name that was then common over there, so that I would fit in. I liked to think the choice was prophetic, and that I was destined to end up in Sweden because of my name. I know it wasn't like that though: my mum told me much later that she'd named me after her favourite Dutch actress.
We finally moved to the Netherlands when I was eleven; when I was seventeen we moved to Sweden and my father Swedified his name from Bloem to Blohm.

It was a move born from desperation: the Netherlands had turned out to be a very different place than my parents had imagined. My father was a proud man, and constantly having to swallow that pride to avoid trouble proved too much for him. Partly it was simply bad timing: we arrived in Holland when the Moluccan train hijacking of 1977 was still fresh in the collective memory, and of course people did not distinguish between Moluccans and Indos, let alone Iban. But I think that even without that, the everyday and institutional racism would have gotten to him eventually. My mother was treated even worse, because my father was after all an Indo and a man, but because of her upbringing she never let it show. I got my share at school, although it was many years before I started to recognise it for what it was. In any case, when my father very carefully floated the idea that we might move away from the Netherlands, we were both delighted.

I went to study Applied Mathematics at Stockholm University. That's where I met Stefan. In my final year I went to Scotland with Erasmus. I did my undergraduate thesis at the Institute for Advanced Oceanographic and Atmospheric Science of  Glasgow University, on simulation methods for ocean currents, and I stayed on for a PhD. Stefan did the gallant thing and moved with me. I should have known then that you can't build a life on gallantry, but I was young and in love.

I soon noticed that he wasn't quite happy in Scotland, even though he put on a brave face. For a few years we muddled through. Then I got pregnant, and for a brief while we were happy, but a few months after Rob was born I got postnatal depression,  and that was the last straw. Stefan couldn't take it and went back to Sweden. I can't blame him, and it was probably better because in retrospect he had clearly been depressed as well, but at the time it was a nightmare. If my mother hadn't come over for a while to help, I don't know how I would have managed. But we both got through it in the end, and we still exchange Christmas cards. And I did get my PhD,  even though it took me a year longer.

I loved Scotland so I wanted to stay there. I got a postdoc position, and then a lectureship, at the same Institute where it all started. I found a fabulous flat in Helensburgh and settled into the routine of academic life. I wanted Rob to have a good connection with Sweden so we went to stay with my parents during the holidays as often as possible. After high school he went to study software engineering at KTH in Stockholm. He met a lovely Italian-Swedish lassie called Laura and they moved in together, and I knew then that Sweden would be his home. But he still liked Scotland so they used to visit often, until they moved to Australia. That is a long way away, and we all want to limit our long-haul flights; but I went to see them on a visit to Indonesia.

I love to go back to Indonesia. I love the climate, the light, the food and the people. But I don't feel I really belong there. When I go back to Holland, I don't belong there either. Even during my frequent visits to my parents and son in Sweden, I felt I didn't  really belong there any more. But I don't belong in Scotland either, and certainly not after Brexit. A European person of colour, it doesn't get much worse. Not that I regret my choices. I am still very glad to have spent most of my life in Scotland. But I can now  empathise more with the "somewhere people". This feeling of not belonging is the price I pay for being an "anywhere person", and on some days it feels like a high price.
<br><br>
<!-- ### August 2023, Sigrid in the menopause -->

_Helensburgh, August 2023_

Time slipped by almost unnoticed and before I knew it I was in my fifties, and a proper career academic, if not one of the high flyers. That had never been my ambition anyway (but then a high-flyer would treat that lack of ambition as an excuse for failure). I worked with engineers and computing scientists, focusing on better, faster and more precise simulations; I had a decent record and was happy in my job.
But shortly after my promotion to Reader, I noticed the first telltale signs of the menopause. It hit me hard: I got the signature hot flushes and much more besides. For a while I was really struggling to simply get through the days. Initially, I didn't realise what was happening to me. When I finally caught on, I joined the local support group; with much trial and error my GP and I found an HRT combination that worked, and things gradually got better. By the time the Ditlevsen paper came out, I felt well enough to tackle the project.

The local Menopause Cafe was a godsend. Just meeting other women in the same situation, eating cake and discussing experiences made such a difference. It was there that I met Eilidh. I am usually a bit apprehensive on meeting new people. There is no way to hide that I am a foreigner, and I dread the "where are you from" question, no matter how oblique or well intended. But Eilidh wasn't like that at all. She had wavy auburn hair ("From the bottle", she remarked as soon as she saw me look at it), green-grey eyes and an open countenance, and I thought she looked like a model for a Visit Scotland ad &mdash; if they would use menopausal women as models.
She immediately put me at ease in the most natural way, without seeming to do anything at all. The other women were very nice as well, but she was the reason I actually looked forward to my next visit to the Cafe. We got on really well and soon we saw a lot of one another. Her husband was away for his job for long stretches of time, and her kids had all flown the nest as well, so she had plenty of time, which she filled with all kinds of volunteering activities, and I joined in when I could.
<br><br>

<!-- ### Helensburgh, April 2026, Eilidh -->
_Helensburgh, April 2026_

One lovely spring day, with the cherry trees in bloom all over Helensburgh, Eilidh and I got talking about my AMOC project, and I explained about the new results by Li-Zhen and Mirza, and the problem we had with the data: the lack of precise measurements of the deep currents of the AMOC meant we could not accurately pin down the precise moment it would collapse. I'm not sure how I happened to discuss this because it was very technical  and boring to any outsider and of course Eilidh couldn't do anything about it. But it was heavy on my mind so it was a relief to be able explain it to a patient listener.

Eilidh left in a pensive mood, and the next time we met she said, "Sigrid, there is someone I'd like you to meet. He might be able to help with your problem &mdash; it's my husband, John". <br>
I was really surprised. "Your husband? Is he an academic too? How do you think he could help me?"<br>
"Well," Eilidh hesitated a bit, then made up her mind. "I've never told you what John does for his job, and you've been so good not to ask. But we've discussed this, and we both think this is really important. So here it is: he's a nuclear submarine commander." She looked at me with an apprehensive expression that was very rare for her. <br>
I understood: by now Eilidh knew me well enough to know that I was a pacifist. I was strongly opposed to the doctrine of nuclear deterrence. So she was worried about my reaction. On the other hand, I was now also old enough to realise that there were well-meaning people who genuinely believe it makes the world a safer place. And now I was about to meet one of them. So I smiled at her and said, "I'd love to meet him." She invited me to dinner that weekend at their home.
<br><br>
<!-- ### Helensburgh, April/May 2026, the commander -->
John was a quiet, soft-spoken man, clean-shaven with short grey hair and keen eyes. I had done my stint as Head of the Institute, and as a veteran of too many executive committee meetings at all levels of the University, I immediately recognised him as a person with authority and a true leader.<br>
"When Eilidh told me about your project, and about this danger of the AMOC collapse, it got me thinking. It is the role of a government to keep its population safe, and the military is there to give it the means to do so. But we can't protect people against the dangers of climate change, at least not directly. It may surprise you but you will find very few climate change deniers in the upper echelons. In particular in my job, we've been aware for a while now that in a few decades the nuclear submarine bases will no longer be safe. We also know climate change will cause global instability, which means more war. We have to take it seriously." He paused for a while, taking a sip from his tea. "Anyway, I think our submarines would be ideal to collect detailed data on the Atlantic currents. They can go really deep and have high-sensitivity sensors for salinity, temperature and density. We could sample the AMOC during a routine patrol in the Atlantic. It would take about a year."<br>

I was couldn't believe my ears. It really did sound too good to be true. But if it worked, it would provide us with an unprecedented source of very accurate observations. It would be sensational.<br>
Then again, it seemed incredible that someone in his position would be prepared to risk his entire career for my project. I objected, "But isn't that very risky? Would that not be against your orders?"<br>
He gave me a look that said, I see there is more behind your question, and replied calmly, "There is definitely some risk, but I have already carefully put out some feelers and I think that I can make it work. It does not compromise national security. And I have a lot of leeway as a commander. The top brass would be very annoyed if I didn't inform them upfront, but I have some contacts there too. So I think I'll get off with a warning. If I understand correctly what you are planning, this research will cause quite a storm, and it would look really bad for the MoD if they sanctioned me for doing the right thing."<br>
"How accurate are those sensors?" I asked. This was a crucial point. He smiled "We use lidar Brillouin spectroscopy. I'm told it's pretty accurate." <br>
I nodded and almost smiled at his understatement. I had worked with this technology myself, it was state-of-the-art. But I was still puzzled. "But why?"<br>
John looked at Eilidh, then at me. "Sigrid, I joined the Navy because I believed it helped to keep Britain safe. But if what you and your colleagues say is correct, and I believe it is, then this AMOC collapse is the greatest threat Britain has ever faced, and right now we are not at all prepared to handle it. So I actually see it as my duty to help you, even if my superiors are less likely to see it that way."<br>
I had an inkling there was more to it than that. When I had explained to Eilidh how climate change would make AMOC collapse, and what that would mean for Scotland, I had been surprised that she accepted this right away and also relieved at not having to argue climate catastrophe with my best friend. Although most people outside of my research community would superficially agree climate change was a bad thing, they were not prepared to accept that Britain would get Siberian winters. Then Eilidh told me that some years ago, their son Ruairidh had become an environmental activist. That had led to a lot of strain in the family but she and her husband had worked through their denial and disagreements and eventually come around to his side, stopping short of becoming activists themselves.<br>
I nodded thoughtfully. "I understand. Well, John, I image there is information you need from me. What is it and how can I best get it to you?"<br>
He considered for a while. "We need the most accurate maps of the AMOC current, and a precise idea of how many samples you'd need to get the necessary accuracy for your models.<br>
We use the NATO AML format, and within that the water column data is in netCDF format, which I think is what you use too, isn't it?"<br>
I nodded again.<br>
"It would be ideal if you could provide a netCDF file with coordinates for all the possible sampling points, and a text file explaining how many you'd need, which ones are absolutely crucial, how much deviation you could allow and so on. Just give it to Eilidh on an encrypted memory stick."
<br><br>

I got Eilidh the data a few days later, and that same day I had a call with Li-Zhen, Mirza and Eza to discuss John's proposal. As they all shared my concern about the lack of accuracy, they were entirely in favour.<br>
Mirza counselled caution: "We need a backup plan in case the data does not materialise. So we need to get the most recent satellite and aircraft observations, complemented with shallow-water sampling at key points in the current."<br>
Eza argued that the provenance of the data should only be made public in the final paper. "It's essential from a publicity perspective. The media will go wild when they learn where the data comes from. If it leaks out too early, it will completely destroy the impact of the paper." Little did we realise how momentous this decision would prove to be. We were truly babes in the woods.<br>
Li-Zhen was sceptical. "It will be very hard to keep the provenance secret. Our consortium is too large." <br>
I argued for being open. "I agree, Li-Zhen, but the nature of the data is such that our clever  postdocs and PhD students will quickly realise that they could not have come from an ordinary survey ship. There are hardly any survey vessels that can measure at depths of 300 m of more. If we told them a subterfuge or refused to disclose the origin of the data, some of them would naturally become suspicious and they would feel we didn't trust them." <br>
Mirza supported me. "It's not only a matter of operational secrecy, it's a matter of team coherence. If we lie to them now and have to admit to it later, our teams' trust in us will be forever destroyed. There's hardly anything worse you can do than tell somebody you couldn't trust them." <br>
Li-Zhen nodded, "you're right. I don't want to deceive my team either, they mean a lot to me."<br>
I added two more points. "First, the actual provenance of the data is hardly believable, it's totally unprecedented. That will work to our advantage. Second, I am quite certain that my younger self would not be happy to be working on a project with data from the military, however lofty the cause. I'm sure some of our researchers will feel equally strongly about this. If we tell them now, that gives us and them plenty of time to find another project."<br>
Eza concluded, "well, that means we'll just have to trust our teams. We'll just have to tell them. But I propose we have a plausible cover story that anyone in the team can use in case somebody asks who doesn't really need to know. Sigrid, do you think you can come up with an alternate provenance?" Two years into the project was a little late for coming up with an origin story, but luck was on my side: from the start of the project, my team had provided most of the observational data sets to the other teams, so it would not seem odd that we would provide the final data as well. I saw two options for a blind. Some years ago, we had put in a research proposal for a survey of the Atlantic currents by the RRS Discovery, but it had not been funded. I could just pretend that it had been funded, and that the results had finally come in. Or I could invent some anonymous super-wealthy benefactor who had funded such a project under a very strict Non-Disclosure Agreement. The former was more believable, but would not stand up to any serious scrutiny. The latter seemed rather outré, but in proper truth-stranger-than-fiction fashion, there was a precedent: in 2018, Norwegian billionaire Kjell Inge Røkke had funded just such a research ship. So we decided on that option, also because it would be near-impossible to verify.

I arranged a chat with Esther and put my cards on the table. She wasn't very comfortable with the arrangement, as it meant that we would still be lying to the public about the nature of the data. But she saw that it was on the one hand a necessity, and that on the other hand no harm would be resulting from it: the science would still be the same. So she agreed to back me up.

We organised a few meetings for all researchers in all teams, almost fifty people. I explained to them that the data for the next phase of the project would be provided by the military, who would collect it using submarines in an unprecedented UK-US collaboration.

I also told them that it was important to keep the provenance of the data confidential to maximise the impact of the publication, and that therefore,
we proposed they tell our provenance cover story to anyone who didn't really need to know where the data came from.

For about half a year after that, I heard nothing from John. From Eilidh I learned that he was busy laying his plans, in particular securing support from within Navy Command, and even a high-ranking Sub Navy Board member.

Then one day in early autumn, Eilidh invited me again for a little dinner at their home. I felt like a sleeper spy finally called to action, an odd feeling in this sleepy place.
Walking from my flat on the waterfront to their house higher up the hill, there was a distinct autumn smell in the air. The sun had already set behind the hills on Cowall and it was getting chilly. Here and there on the old walls, some of the pink flowers of rock soapworth were still clinging on, recalling the distant memory of their early-summer glory.

It was very pleasant, the food was delicious and John was skilful at keeping the conversation both engaging and safe. When we had moved to the cosy salon and were sipping our beverages of choice, he finally broached the topic.<br>
He smiled broadly, "All is going well, Sigrid. Very well indeed. We'll leave on patrol in a few days and I have managed to get informal support of key people in my team and of some of the top-level decision makers. Whatever comes of this, I am pretty confident I won't be censured. On the contrary, chances are the Navy will make much of this action. And best of all, the Americans have agreed to do something similar. It's really funny when you think of it: submarines are so secret, you can use that secrecy as a cloak for operations like this."<br>

"I know," I interjected, "Above Top Secret!"<br>

John looked at me quizzically.<br>

I laughed. "I read about it in a novel once. So secret that even with the highest clearance, you're still not in on it."<br>

He nodded, with an amused smile. "Something like that. I don't know how it got arranged, but apparently some other commander at the other side of the pond is willing to do this same thing, and obviously nobody is to know over there either. In any case, I think that is a very clever tactical move: it will give the whole operation a strong air of legitimacy once it comes out and it would be very hard for either side to paint it as a conspiracy. And you will get even better data as a results. A win for all involved."
<br><br>
<!-- ### Helensburgh, October 2026, sub leaves on patrol -->

_Helensburgh, October 2026_

And so it happened that, on a beautiful late afternoon in October, with the soft sun turning the autumn leaves on the hills pale russet, fallow and golden, I stood with Eilidh on the narrow pebble beach at the lighthouse on Rhu Point, waving at the crew on the deck of a nuclear submarine being towed through the narrows. If anyone had told me a year ago that I, a veteran of many Faslane blockades, would someday wave and smile at the military personnel in charge of a missile submarine, I'd have thought them mad. It would have been unthinkable. And yet, here I was, and I was at peace with myself. The machine was an abomination, but if it could help us win over the public on AMOC, it would for once have contributed to something worthwhile. I knew that John and Eilidh looked at it totally differently, and it made me turn to her with a goofy smile. I could see Eilidh was happy as well, despite having to miss her husband for yet another year. "Why are you smiling like that, Sigrid?" she asked, with a hint of mischief in her smile. <br>

I replied "We're the most unlikely of friends, aren't we?"<br>

She knew exactly what I meant. "We are indeed. And I'm glad for it."

# Chapter 6. Surprise, sometimes, will come around

> *I will surprise you sometime, I'll come around, when you're down* (Interpol, "Untitled")
<br><br>
<!-- about 2,000 words -->

<!-- ### Stockholm, March 2035, the troubles -->
_Stockholm, March 2035_

When I had finished the kitchen chores I went out for my usual constitutional along the harbour. It was cold but clear and pleasant. I ambled without purpose, and ended up at the famous "God our father on the rainbow" fountain. There I stood for a while, staring blankly at the place where the sun had set, hours before &mdash; and thinking. I still couldn't understand how it had all worked out in the end. It had been quite a rollercoaster ride.
<br><br>
<!-- ### World, March/December 2027, second round of funding -->

_Glasgow, March 2027_

That illustrious first viral fundraising action was by then nearly three years ago. The media and the public had mostly forgotten about the project; but not entirely. Luzmila and Elías had managed to keep alive the network of supporters and organisations that had arisen at that time. We had made sure that the preliminary results got press releases, so that there'd been a small but continuous trickle of posts, articles, podcasts, videos, interviews and local fundraisers that ensured we were not totally forgotten. Elías called it the background hum.

But now the time had come for a second round of fundraising, and unfortunately we did not get another viral idea. Still, thanks to a carefully managed media campaign (lots of interviews for me and Li-Zhen), our research got above-average attention and public support grew. Because we now had proper peer reviewed scientific results, and our science had, to paraphrase Ken MacLeod, been socially constructed, we got more traction with established green organisations and political parties, and that considerably extended our reach.

Also, for this round we needed less crowdfunding. We still had funds left from the first round, and although the national funding bodies were, as expected,  on the whole too inflexible to react in time, several institutions and some independent funders had read the room and decided it would be good for their PR if they provided our teams with some funding. So once again, we met our targets and &mdash; at least financially and scientifically &mdash; all was well.
<br><br>
<!-- ### World, April 2027, preprint publication -->

In fact, all was very well: the preprint of the paper was published at the end of April, and it did not go unnoticed. Cue an uptick in media attention for the project and the cause, more interviews, raised profiles all around, and even more crowdfunding.
<br><br>

But on a personal level, things weren't going so well at all.
<br><br>
<!-- ### Sydney/Helensburgh, April 2027, Rob & Laura -->

Rob and Laura had moved to Australia in March. We had scheduled regular calls, and at the first one, in April, I immediately sensed that there was something wrong. They both looked stressed and thoroughly unhappy.

Rob came right out with it: "We've been verbally abused and threatened by racists several times in the past weeks. It must be the area where we live. At first we couldn't understand why they were picking on us, but we soon worked it out from the insults: these people think that we're Malaysian immigrants. I looked it up, and there is a lot of racism directed specifically at Malaysian immigrants, probably because most of them are muslim. And to make it worse, the landlord of our flat is one of them too. He is now trying to get rid of us."

I nodded my understanding, and wished I could give both of them a hug. I knew exactly how they felt. Racial abuse was nothing new to me. Fortunately I had been spared the worst of it after leaving the Netherlands, at least until the Brexit referendum. In Sweden and Scotland, people looking like me hadn't been the most common targets, but Britain was swept up on the wave of xenophobia and inevitably some of it was directed at me. Rob takes after me in looks and I have my looks from my mother,  so he could easily be mistaken for Malay. (He takes after his father in temperament, not a good combination under the circumstances.) Laura's ancestors came from Southern Italy, and despite her strawberry blond hair she looks quite exotic. With hindsight, it was not surprising that they had become a target for some ignorant and intolerant Australians.

I wondered if I should travel over there to support them, but I didn't mention that yet. It was a desultory call as there was not much I could do except listen, and they were in a state where they totally regretted their decision to move. All I could suggest was not to do anything rash.
<br><br>
<!-- ### Victoria/Helensburgh, May 2027, Li-Zhen -->

I am not a very social person, and not particularly amiable, but despite that I am somewhat overly sensitive to the moods and emotions of others, even people I don't know very well. With Rob, I had always felt that very strongly. I think, or rather I hope, that my own mother hadn't been like me as I could be a very melancholy child, for no obvious reasons. Buried somewhere deep down in my mind though is a suspicion that she was, but never let it show.

I felt bad for Rob and Laura and the next days I didn't get much done. I kept mulling over their situation. A week later, I had practically made up my mind to travel to Sydney. After all, the team at Sydney University working on the project with Mirza was lead by Gulnur who just happened to be an ex-PhD student of mine. I had a long-standing invitation from her so I could combine it with a research visit.

But then I received another bombshell, this time an email from Li-Zhen: she had just been diagnosed with breast cancer.
I called her right away. She looked the same as ever. I couldn't discern the slightest worry on her face. I guess I must have looked a lot more frazzled.
Li-Zhen was very businesslike about it: "Something showed during the routine screening, and today they did an MRI. It's still small and there is no metastasis yet. It will be fine. I'm lucky, the oncology team here is very good and I can start treatment in a few weeks."

I knew Li-Zhen was single, and that her parents were no longer alive, so I thought that right now she might need me even more than Rob and Laura. Maybe Australia could wait. "I can come over to look after you. I can be there by the time the treatment starts."<br>
"Don't be absurd, Sigrid. There's really no need. Don't even think of it. I will be perfectly all right. They are going for neoadjuvant chemotherapy, and I think I can handle that. Please don't worry, I only let you, Mirza and Eza know because it might impact on the project."<br>
"How you can think about the project at a time like this is beyond me... but I'm glad you think you'll be fine." <br>
Usually we closed the connection with a quick wave and a short "Bye-bye" but this time I felt awkward about it. But Li-Zhen simply said, "I have another meeting now, I have to go now" and that was that.

The thought that I might lose Li-Zhen hit me like a brick. We'd only met once, long ago, but she was one of my best friends and I couldn't imagine a life without her. I felt I had to do something, if only to deal with my fear.

I knew that Li-Zhen seriously underestimated the disruption this would cause to her life. She would definitely need assistance. But maybe it could be purely practical and maybe she was right that she didn't need me there.

So I arranged another meeting with Li-Zhen, together with Luzmila. She showed us what it really meant to be a community organiser: right away she hatched a plan for a support rota for Li-Zhen, relying on a network she just happened to have in Victoria, even though she lived right on the other side of Canada.<br>
Li-Zhen initially didn't want to hear of it. "I don't have the energy to meet new people. And I won't have time to deal with them. The treatment is going to take a lot of my time and I already don't have any time to spare."<br>
She was clearly still in denial about the seriousness of her situation. But Luzmila explained "You don't have to meet any them if you want it that way. They'll be invisible, just do your chores. It'll just make your life easier."<br>
I chipped in "The treatment means you'll have to spend a lot of time at the clinic, so you'll have a lot less time for housework." I knew of course full well that it would be a lot worse. My mother had had cancer, and I remembered very well what an ordeal it had been. Even though treatments had improved, Li-Zhen was in for a very hard time. But I kept those thoughts to myself. Instead, I added "The volunteers can come when you are at the clinic, and have your house cleaned, shopping done and food ready when you return."<br>
Li-Zhen considered this for a while longer, and finally agreed "That makes sense. Thank you very much, Luzmila, this is incredibly generous of you and your friends. But I don't want to take any more of their time than necessary, so wait until I've started my treatment, in a few weeks."

So it was decided, and I was relieved: at least Li-Zhen wouldn't be totally on her own. I could go back to worrying about Rob and Laura, and planning a trip to Australia. I was torn by it: on the one hand I didn't want to fly because of the emissions. But on the other hand I felt I needed to be there. So I just hoped we wouldn't be judged too harshly for the things we did for love.
<br><br>
<!-- ### Stockholm, 14 May 2027, Stefan -->

But fate had other plans in store for Sigrid. On Himmelsfärds dag, like a bolt out of the blue, I got a text message from Stefan's wife Greta, who had never contacted me before. All it said was that Stefan had died of a stroke and could I please tell Rob. I was dumbfounded, it was so utterly unexpected. Stefan and I weren't close any more, especially after he had married, but we still kept in touch on and off and nothing had prepared me for this. When I had regained my composure I arranged an emergency call to tell Rob and Laura the bad news.
They were understandably even more affected than I, especially Rob. He had always been close to his dad, and I could tell he was really distraught, even though he tried hard not to show it. It was heartrending.
And yet, even in such dark times there was a silver lining: Stefan's death took Rob and Laura's minds completely off their personal troubles. They arranged right away to travel back to Stockholm for the funeral. I called Stefan's wife to let her know, and to ask if I could help in any way. Stefan had several elder brothers and I knew Greta's parents and sister were still alive too, so I wasn't surprised when she said everything was being taken care of.
<br><br>

_Stockholm, May 2027_

The funeral was a humanist service followed by the cremation, at the New Crematorium at Skogskyrkogården. Both my parents had been cremated there, ten years ago. It's a peculiar modern building set in a pine forest and seeing it again I felt a strange feeling close to nostalgia. I was suddenly overwhelmed by memories from my years in Sweden with my parents and my time with Stefan. I didn't write anything on the pine casket.

After a traditional funeral meal of sandwiches and Prinsesstårta, I took Rob and Laura on a tour of Blockhusudden, to revisit my old haunts. We had a good time together. But I myself felt very strange. For the first time since my move to Scotland I was homesick for Stockholm, and I couldn't understand it. There ought to be nothing to tie me to this place. But memories are a powerful force, and my defences were down. I thought of Legolas, "Alas for the wailing of the gulls!"

Stefan had always been a meticulous person so it was no surprise to me that his will had been in perfect order. Rob received a considerable sum. I gifted my share to him as well, and with that money they could afford to move to the neighbourhood of Sydney University, an area where racism was much less of an issue. I could see they both felt a lot better now they had a way to handle their situation. Maybe I wouldn't have to go to Australia after all.
<br><br>
<!-- ### Busan/Vancouver/Helensburgh, 21 May 2027, Mirza's offer -->

_Helensburgh, June 2027_

I had been back from Stockholm for a week when once again something totally unexpected happened, but this time in a good way. Li-Zhen, Mirza and I were having a project meeting a few days before Li-Zhen was to start chemo. At the close of the meeting, Mirza casually asked, "by the way Li-Zhen, I'm attending the OceanPredict symposium in Vancouver next week and I'd like to stay on for a few weeks after that. Could you host me? All I need is a place to sleep."<br>
We were both surprised but Li-Zhen didn't hesitate for a moment, she was clearly delighted by the offer. "Of course, Mirza, you can stay at my place. But you know my situation. The next months are not going to be fun and I won't have much time to work with you." <br>
Mirza smiled his enigmatic smile "Of course, I realise that. Nevertheless, I'd be grateful if you could put me up. You won't have to look after me, I promise I won't be any trouble."

Mirza was as hard to read as always, so it was impossible to know why he'd decided to do this, but his being there turned out a godsend. He ended up staying for three months and supported Li-Zhen while she suffered from the side effects of the chemo. Initially, I had been worried he might be falling for Li-Zhen, or the other way round. I knew he had a steady boyfriend in Busan and I liked all three of them a lot. But with hindsight I clearly had been worrying for nothing.

<!-- # Part III. Autumn 2027 &mdash; Autumn 2029 -->

# Chapter 7.  I'm relying on your common decency
<!-- 3,500 words -->

> *It's obvious you hate me, though, I've done nothing wrong* (Depeche Mode, "People Are People")
<br><br>
<!-- ### October 2027 Li-Zhen recovered, final paper for 1st iteration published -->

_Helensburgh, September 2027_

And so the summer rains passed, and the autumn rains began. As I had expected, Li-Zhen had a rough time with the chemo but somehow, much to my surprise, she still managed to keep her research going. To what extent this was thanks to Mirza I never knew. I for my part shepherded the paper on the first phase of the research through the publication process, and so by early October, the official, peer-reviewed version was published in _Nature Communications_. Although the scientific community had for the most part accepted our results based on the preprint, the publication did not go unnoticed. There was a lot of media interest and even mainstream politicians started to pay attention, as some reporters would quiz them about the contingency planning for the AMOC collapse. Which of course they deflected, but it raised the project profile so much that even the tabloids started running AMOC articles.

Li-Zhen had her surgery almost on the same day the paper was published, and to our unending relief it went very well. The neoadjuvant chemotherapy had been very effective, and the medical team was confident the tumour had been completely removed. There would likely be no need for follow-on chemo or radiation therapy.

<!-- Li-Zhen recovered quickly and by the end of the year she was quite looking her old self again. -->

Although we had not publicised Li-Zhen's illness, because of her high profile the media had quickly become aware of it. The result had been an unexpected wave of sympathy, well wishing &mdash; and more crowdfunding.

On the strength of the work in the paper, the team leads had once again approached their funding councils, and some of them displayed some unexpected adaptability by providing follow-on funding.
<br><br>
<!-- ### Helensburgh, October 2027, sub returns from patrol -->

_Helensburgh, October 2027_

It was purely by chance that I saw the submarine returning on a wet morning in October, with horizontal rain streaking over the Gareloch. I was up a bit earlier than usual, and looking blearily out of the window over the loch at the veils of driving rain. I could barely make out the brutal, dark shape gliding through the choppy water with its twin tugs pushing it towards the Faslane base.
While I watched it pass by, a feeling of apprehension and excitement rose up in me. It could of course be a different submarine but given the timing I thought that highly unlikely. So I would soon know if it had brought any data. I resolved to be patient and to wait until I heard from Eilidh. And for the next few days, waiting was all I did. I felt in limbo. I didn't accomplish anything. I couldn't focus at all. When I finally got the call I was so nervous that at first I could hardly pick up the phone. But Eilidh's warm voice brought me right down to earth. "Hi Sigrid, John is finally back, how about meeting up this weekend?"<br>

I had just the presence of mind to reply "That would be wonderful! It's my turn this time, isn't it? How about Saturday night around seven?"

I am not a fancy cook, but there are a few recipes that I learned to make from helping out in the kitchen as a child. I vividly recall the kitchen of our old house in Ketapang, with the intense green light of the tropical sun filtered through the leaves of the almond trees spilling in through the open window, and a delicious smell of frying spices. I could still see myself standing there, barely tall enough to reach the worktop, helping my mum make ayam pansuh. Over the years I had developed my own vegan version of the dish. At least it would be something new and unusual for Eilidh and John.

We had a very pleasant meal and talked about the project and many other things besides. They are both wonderful dinner companions. Of course, not a word about the submarine mission. Almost casually, with just a hint of a wink, John gave me an encrypted memory stick, "Ah, by the way, here's that file I promised you."

The stick contained a single dataset combining the observations from both submarines. The next days I ran a comprehensive array of sanity checks on the data, and everything checked out. When I overlaid it on a map of the AMOC, I saw how very cleverly the operation had been carried out. It looked like lots of rows of points on slightly curved lines, arranged such that they frequently intersected with the current, but reconstructing the trajectory of each submarine was impossible. I also marvelled at the wealth of data. Not just the amount of it, but in particular the depths at which some of the samples had been obtained would be virtually impossible to achieve by any of our survey vessels.
I shared it with a few key people who did their own thorough integrity checks. Once we were all are satisfied, we shared the data with the teams, impressing once more on them that secrecy was of the essence.
<br><br>
<!-- ### World, February 2028, more trouble -->

_Glasgow, February 2028_

We agreed to keep a lower profile than before with few official press releases, and when asked about the work, we stuck to the cover story. It seemed to work for a little while.

So far, although the project had received a lot of attention, more than I had ever thought possible, it had mostly come from friendly corners. Even the tabloids, where they had bothered to take notice, seemed to be broadly on the side of us boffins. But with the official publication of the first results, that happy façade started to crumble.

Obviously, the AMOC collapse was going to cause dramatic changes. Very bad things were going to happen and it was therefore in the interest of some to downplay or even deny it. Apparently we had now crossed the line where shadowy actors came to play, and they were clearly well-organised. A disinformation campaign appeared on social media, as well as an attempt at a smear campaign. Some self-styled journalists had been digging into my past and that of Li-Zhen and Mirza, and likely that of the others as well; vague accusations and blatant lies about us started to surface in various places. It was very upsetting. I had been a boring, diligent and honest academic with nothing to hide for my whole career &mdash; until now, that is &mdash; and yet, to read the stories about me, I was one of the most dishonest opportunists alive, and Li-Zhen was even worse, with hints of embezzlement of funds, and Mirza had backstabbed his way to the top. There was also a huge rise in plain racist, misogynistic and in Mirza's case homophobic posts.
I didn't have much of a social media presence and I could quite easily lock it down even more, or just leave it for a while; but both Li-Zhen and Mirza were quite active on their social media platforms and for them, this sudden and unexpected wave of ad-hominem attacks came as a nasty shock.

On a dark, dreich night when February was at its most depressing, I was having a quiet dinner with John and Eilidh and near the end I mentioned this state of affairs to them. John got a very grave look on his face, quite unusual for him. "I think, Sigrid," he said thoughtfully, "that you should move somewhere else until your paper is published, and your team mates as well."<br>
"But John," I retorted, "it's not as serious as all that, it's just very annoying."<br>
John shook his head. "I can't agree with you there. From what you're saying, this looks orchestrated, and I fear it will get far worse. Are you on the electoral roll?" <br>I nodded. <br>"There you are then, you must assume that any time now, people will be showing up on your doorstep to harass you." He looked at Eilidh, and she nodded. I somehow got the impression that they had discussed all this before. "We'd like to propose that you move in with us a while. And at the very least your friends Li-Zhen and Mirza should move somewhere safe too."<br>
I was really taken aback. John was right, and I realised now that I had known this for a while but refused to acknowledge it. The situation was not simply a minor annoyance. <br>
"But if you are right," I protested, "then I don't want to get both of you involved in all this. Would it not be better if I go somewhere on my own?"<br>
Eilidh wasn't having this. "Don't worry about us. It's you they're after, and your friends. Also, the idea is that nobody will know that you are staying with us. So nobody will come and knock on our door." <br>
I wasn't convinced, "What if I'm being followed?"<br>
 John said, "With a little care, it's easy to get in here unobserved. You may have noticed the lane behind the house. It has a continuous wall running along all the properties in the street, with a separate back gate for each of them. There is no way to hide there, and without seeing exactly which gate you enter, no way to know in which house you are staying. And it's shared with the parallel street below us. I'll arrange with a neighbour that you can go through their garden. Nobody can follow you there." <br>
"Unless of course there are many of them," I objected, but I realised I had now started to treat this as a kind of a game. <br>
John replied, "That is true, if the opponent is powerful enough, they would find you anyway. If things would get that bad, you'd have to apply for police protection. But my assessment is that it's not on that level. These are people attempting to discredit your results, not terrorists or organised criminals."<br>
Still, this was an unprecedented step, and not just for me, so we agreed I would discuss it with the team the next day and let them know. <br>
"Call me tomorrow when you've decided," Eilidh said when I was leaving. I felt thoroughly chastened and shaken as I made my way back home through the dark winter night. With shadows everywhere, the deserted streets where I had loved to walk at night  were all of a sudden no longer friendly, and even the safety of my own home now seemed an illusion. I couldn't help but glance around surreptitiously before opening the front door. But there was nobody there, no lurkers in the shadows, no suspicious cars.

I sat down with a cup of herbal infusion and thought the matter over. Looking at it with a sober eye, it wasn't I who was in any real danger, only the project; and actually, the prospect of living for a while with Eilidh was rather appealing.

It was just after midnight. I didn't think I would be able to sleep before settling this so I set up an emergency meeting for 3am. If nothing else, that should show the others it was something extraordinary. Nothing but an emergency could move me to forego my sleep like that.

And so, a few hours later, we were all there, three slightly worried or puzzled faces, or maybe just bemused. "It's like this. A friend of mine who I respect a lot in such matters has advised that we all move to a safe place until the paper is out. Eza, for you it might be less of an issue as you have been largely left alone until now, haven't you?"<br>
"Mostly, yes," Eza agreed. "The only thing so far is that I got an anonymous phone call at work, somebody harassing me, although it wasn't clear why or what about. I've alerted University security but of course they couldn't trace the call. From now on I will have all my calls routed through a secretary who'll do some screening."<br>
"I'm very sorry to hear that, Eza, " I said, "I hope that will put a stop to it."<br>
"Well, I'm not taking any calls from numbers I don't know on my own phone, and on social media nothing bad has happened yet. Here in Malaysia the whole thing is not much in the news." <br>
"I really hope for you it stays that way. So what do you all think of this proposal?" <br>
Mirza replied without any hesitation, "I agree with it. I was discussing something like that with my partner yesterday. The way this is going, it won't take long before someone doxxes us and that would make our life really hell. We've already been thinking where we could go." <br>
Li-Zhen agreed, "I hadn't really considered it until now but I think what you propose is wise. My social media is awful right now, and I had been worrying where this would lead to. I think I will make this an opportunity to do something I've wanted to do for a long time: to go and live on a boat." <br>
I smiled, "You got the sailing bug from our brave teams?" <br>
Li-Zhen answered quite seriously but with a bright smile, "Not really, I've wanted to do this for ages, but their adventures made me want it a lot more." Her smile faded "Not that I think I'll have time for any actual sailing."<br>
Eza joined in, "At least not until the paper is out, but afterwards you should really do it, Li-Zhen! It sounds wonderful!" <br>
"What about you, Eza?" I asked.<br>
 "I'm not sure, Sigrid," she said. "With Nur in school it's hard to see how we could move, and even if we did, I think it would be easy to trace us. I think I'll wait and see. It's you three who are the real targets, you have become quite high-profile. There's really not much point in discrediting me, I'm not important enough."<br>
After the meeting I went to bed much easier in my mind. Luckily it was winter, and it wouldn't be light for hours yet; and I didn't have to go to the Institute the next day. I slept soundly until 10am, had a slow breakfast and phoned Eilidh. Less than a week later, I was ready to move in with them.
I told my neighbours I would be travelling for a few months and put a "No Vacancies" sign in my window, which would lead most people to believe that this was a B&B closed for the winter and explain why the place was empty.
<br><br>
<!-- ### World, March 2028, hiding -->

As it turned out, John had been right and events soon took a more serious turn. We started receiving multiple Freedom of Information requests about the project, in particular about the observational data. They were all rejected as a matter of course as research in progress is exempt, but it showed we had appeared on somebody's radar. For every refused request, a volley of posts appeared on social media, inveigling that these refusals meant we must have something to hide. That this was true didn't make it any easier for us. What made it even worse was that some of the requests came from proper gutter press journalists, so our project was now duly dragged through the mud in the tabloids, and some of these were the same tabloids that had been on our side barely a few months ago.

<!-- ### World, April 2028, break-ins -->

Early April there was an attempt to hack into the research machines of several of the teams. This appeared to be a concerted attack: several sites across the globe got hacked at roughly the same time. Some of the more cybersecurity-savvy postdocs in our teams had somehow detected the intrusion and alerted the other teams before any of our institutions had noticed. A forensic analysis by the experts clearly showed that several of the machines had been compromised.

Apart from the distraction in having to deal with the fall-out (including talking to the police, as our respective institutions took such cyber attacks very seriously), the break-ins did not cause much harm. It seemed that the intruders had been looking for data, not trying to sabotage our systems. And they had had little luck &mdash; not because our data was so secure, rather because the research was not very organised. Saved by messiness. But it was a real wake-up call: we clearly needed to be more security conscious. Another thing for my checklist. Luckily, the postdocs were already on to it.

Also, it was most disconcerting. Somebody must have gotten suspicious about our data and tried to find some signs of irregularities. And it was a somebody with the wherewithal to carry out a concerted cyber attack. It seemed only a small step from having our homes and offices burgled. I was very glad I had moved, and when travelling to and from the office I tried at least to make sure I wasn't followed. I probably wasn't.

<!-- ### World, April 2028, more rumours -->

As expected, there had been a few leaks of the actual provenance, but they had been vague and, paradoxically, the internet seemed to consider those claims as fabrications. No wonder, because the truth was indeed hardly credible. So speculations about the "real" provenance had started to crop up. And as I had halfway expected, and rather dreaded, soon after, a rumour started circulating that our data was faked. Luckily, the FoI exemption meant the University did not have to give access to the data or even discuss the origins, and the University's data protection team seemed to take some grim relish in fending off the queries. But this was something that could really damage the public perception of the project. I didn't want to dwell on it as there was no point. It was quite simple: people who wanted the research to be wrong would stick with the conviction that it was all a fake, and there was nothing we could do about it. People who actually doubted would be convinced by our evidence. And for our growing number of true believers, it made no difference anyway. They would probably still believe us even if we'd faked the results. I vaguely recalled some rather dubious character in a novel saying that it was OK to believe the right things for the wrong reasons. Not that I liked it but it was what people did.

Meanwhile we continued to stick to our cover story of a survey funded by an anonymous benefactor.  There was of course no evidence of such a survey in the public domain, but that was hardly surprising. What our opponents would need was the evidence that there had not been a survey, which was a much harder problem.
Without knowing the port of departure it was practically impossible to identify the survey ship. The only way to work it out would be to find all private survey ships and trace their activity in the last years, and find out who commissioned it. I was a bit worried that someone might attempt that task, but then again it would likely take longer than we needed to publish the new results: even considering only Yachts For Science, there were over a thousand vessels, and there were many shipping companies where you could lease a survey vessel. At least a few of them were bound to have covered some relevant part of the Atlantic. I estimated that the probability of someone taking the military provenance seriously was actually higher.

It was all very vexing and distracting, not to mention exhausting on a personal and emotional level. We simply weren't used to people being so nasty.
But the bright side was that, assuming we could hold out until publication, the disclosure of the real source of the data would be all the more impactful. And for me, sneaking into Eilidh and John's garden after darkness had quickly become routine, and living with them was, at least for the time being, even more pleasant than living on my own. Meanwhile, another spring had come to Helensburgh, the cherry trees were blooming, the weather was lovely and somehow I couldn't take all that dark dread seriously any more. My mood lifted and I decided to enjoy the Scottish spring and summer as much as I could (if it would let me).

# Chapter 8. Celebrate the day a better world was won

> *On the day, the storm has just begun/I will still hope that there are better days to come* (VNV Nation, "Sentinel")
<br><br>
<!-- about 1800 words and growing -->

<!-- ### Glasgow, September 2028, preprint ready -->
_Glasgow, September 2028_

On a late morning in September, with the sun starting to burn away the mists and the thin layer of cloud and the promise of a beautiful day, I was walking to my office from Hyndland station. It was a slightly longer route, but I was now randomly picking either station and several routes to the department to make it harder to be followed. It was also a much nicer route, with more trees, so I didn't really mind. The leaves on the lindens and horse chestnuts were just starting to turn. I savoured the feeling of autumn in the already warm air.

At the same time, I was thinking over the state of the project. I was feeling very upbeat. Many good things had happened since the early spring, when we had decided to go into hiding.

That had been a good decision, if only for our peace of mind. We had also handed over all public communication to our institutions, who would post bland official statements on social media and ignore the trolls, and only grant interviews to journalists from trusted media. From our postdocs and PhD students we learned that a heated battle had been taking place on the internet, and it looked like our side had gained the upper hand. By the start of summer, the rumours and lies were slowly dwindling. Maybe shills, trolls and bots needed a holiday too.

This phase of the project was effectively a re-run from the previous phase  but with better data, so I had expected everything to go very smoothly. Or at least as smoothly as such things ever go. The new data were richer than the first set, so the simulation codes would have to be adapted to deal with that. But that should have been straightforward. However, although I had anticipated this and communicated the complete netCDF file data structure to the teams when we disclosed the provenance, several of the researchers had simply kept on using their previous formats as it was good enough at the time and more convenient. The best-laid plans and all that. More debugging was needed. But in spite of that, less than half a year later, the first results started to come in, and after some intense writing and editing we now had the final paper, ready for preprint.
<br><br>
Two weeks earlier, after careful internal review, I had passed the draft on to John, because the time had come for him and his US counterpart to spill the beans to their superiors. He had clearly played his cards carefully because after less than a fortnight of nail-bitingly tense waiting on my part (and no outward sign of any nervousness on his), he told me that some top brass would like a quiet talk with me, off the record. Apparently the military was with the times because a video call would do.
<br><br>

_Helensburgh, September 2028_

John's superior, whose name he mentioned (but who I couldn't help but thinking of as "Steve") as well as his title (which I promptly forgot as it meant nothing to me; but apparently he was quite high up), was an altogether unimposing man in a dark blue ("navy", I guess) uniform shirt, sitting in an anonymous office. I decided to be formal and address him as "sir", and he was correct and addressed me as "doctor Blohm", even getting the pronunciation right. I guessed he was actually a little younger than John, and therefore than myself. Secretly I was highly amused about the opposites we presented: a very important middle-aged white English man with a cut-glass posh accent, and an unimportant foreign academic woman of colour. All we seemed to have in common was that I was middle-aged too, and we both still had quite a few dark strands amongst our grey hairs.

"Dr Blohm," he opened, "Cdr Worthington has informed us of some survey work his vessel has undertaken on its most recent patrol, and which had not been officially sanctioned in advance." <br>
Cdr Worthington, ah yes, that was John. This was just stating the facts so I said nothing, although the "not sanctioned" made me very apprehensive.<br>
"He also informed us of the purpose of this survey, and the measures taken to safeguard operational security on publication of the data."<br>
I heaved an inward sigh of relief. I suspected that Steve was trying to play cat and mouse with my emotions. <br>
"We discussed this with our US counterparts, who somehow" &mdash; he smiled thinly &mdash; "have found themselves in a similar predicament." His smile broadened a little. "It seems you are aware of all this, is that not so?" <br>
I nodded,"Yes sir. The survey data will help us narrow down the collapse of the thermohaline circulation to within a few years. We hope that this will lead to mitigating action in Northern Europe and Northern America." <br>
He indicated his understanding with a measured nod. "What the Cdr might not have told you, Dr Blohm, is that the Navy has had its own experts looking for a while at the consequences of climate change for our national defence." <br>
I thought it wise to go along with this "That's right, sir." <br>
He did the thin smile again, and once again it broadened out a little. "The upshot is that from our perspective, having an accurate prediction of the AMOC collapse and its effects is of strategic importance." <br>
Yay, I thought; but the smile faded quickly and he continued quite sternly "I'll be candid. We have been going through your life with a fine comb. I make no apologies for that. We know that, apart from being a foreigner, you are a pacifist, leftist and green activist with a history of protest against our nuclear deterrent."  <br>
I said nothing, what could I say? He was right, even though I'd never considered myself a "green activist".  <br>
He went on, "You have no idea how much your background has complicated things. But our investigation also shows you to be a responsible academic of high integrity and considerable loyalty to both your institution and your adopted country."<br>
 Well, that was one way of looking at it, I supposed. <br>
 "Also, like I said, we are confident that releasing the data will not compromise our national security. So we have decided to go ahead." Another one of those smiles "After all, to be even more candid, doing this will not only be for the greater good, it will also garner a lot of goodwill for the Navy. We're not doing this out of kindness." <br>
 I was internally jubilant, and I didn't care much whether it showed or not. "Thank you, sir. I am very grateful that the Navy has reached this decision. This work is really of global importance" I hoped that sounded formal enough. <br>
 He did that brief nod again. "Are you in a position of authority with respect to the publication, or do you need higher approval?"<br>
 I was a bit nonplussed, but then I understood. "I and my co-authors are entirely responsible for what we publish and any attribution of data. In this case, we'll want to inform our institutions in advance but that is only so they are prepared for the publicity." <br>
 He now smiled quite broadly. "Very good. We will pass on the wording of the attribution, including contact information, to Cdr Worthington. I have to impress on you that it is quite essential that you make no public statements on the matter. From now on, there will be an officially sanctioned story of how the Royal Navy and the US Navy worked together on this critical mission."<br>
"I understand," I said, "you have my word. We also much prefer to have an official story for the origin of our data."

As soon as the call was finished, I flew in search of Eilidh. I dragged her out of her office, grabbed her by the arms and danced her through the living room. We were both so elated. John came home soon after, and I related the whole call to him. "Bill is a good guy," he said. <br>Ah yes, "Steve" was really a William, Bill to his friends. <br>"I've known him for ages. I'm not political enough  to rise through the ranks like him, but we've always remained close. He was in on this from the start. He did a lot of work behind the scenes to get support." He gave me a sharp glance. "He was quite right, you know, your background was really problematic for some of the more hardline officers. Their knee-jerk reaction was to class you as some kind of anarchist, communist or foreign spy. Bill personally ensured that the security service's report on you didn't have any negative biases. He fenced for me too, because merely my association with you was enough to make me suspect as well." <br>
I was dismayed. "Oh no! John, that could have finished your career!"<br>
 He waived his hand as though wiping something of a imaginary table. "Luckily, I know a few more people who were sympathetic to the cause." He looked me in the eye and added, "but to be honest, I was a bit more worried than I let on at the time."<br>
He left me at a loss for words. <br>
"Well, " said Eilidh, "now that you got the good news, let's celebrate!"
<br>"OK, let's," I said, "but first I must give my team mates the good news!"
<br><br>
And celebrate we did, late into the night. And so it came that I was walking to the department close to noon on the next day.
<br><br>
Two weeks later we released the preprint.
<br><br>
It was the best estimate of the AMOC collapse date by a large margin, and the widest consensus ever reported on this kind of work: seven different teams and models, all in accordance. The paper stated plainly that, unless emissions were cut really drastically in the next five years, AMOC would collapse in 2045 with a window of just one year on either side and still better than 95% certainty.
<br><br>
It also contained a very precisely-worded attribution of the provenance of the data. This stated unambiguously that the data used in the study had been obtained by joint survey by  nuclear submarines of the Royal Navy and United States Navy, because of the strategic importance of the work.
<br><br>
It was a blooming revolution. Right after the synchronised publication of the joint press release by all institutions involved, all hell broke loose.

By that time, both sides of the "AMOC debate" had become firmly entrenched, and the claims of the detractors had slid deeply into conspiracy territory.
But public opinion was now overwhelmingly on our side and "Stop the Freeze!" had become an international demand. Even the more right-wing government parties had added it to their manifestoes.

The submarine results really were our trump card: faced with an actual conspiracy way beyond even their wildest imaginings, the conspiracy theorists and deniers lost all footing. Contesting the data or their provenance was now futile; contesting the results based on the data was not in their league.

The media went berserk, and the rest, as they say, is history. At the next elections, Greens everywhere won by a landslide, and radical action to mitigate the AMOC collapse was started.

# Chapter 9. Underneath all currents

> *This is where I'm staying/this is my home* (Björk,"The Anchor Song")
<br><br>
<!-- ### Glasgow, October 2028, aftermath -->

_Glasgow, October 2028_

With the project over and the results finally out, our lives returned to some approximation of normality. I went back to my flat, and realised that I actually missed living with other people, if only a little. There was still much to do because, of course, academically speaking, there was no final peer reviewed publication yet, and considering how momentous this work was, the reviewers were unprecedentedly critical. It took many rounds of reviews, edits and rebuttals to appease them. The community wanted to make sure that this one was absolutely beyond reproach. Fortunately, we had a small army of researchers to attend to this long-drawn-out process, because for several years after that, my life became quite busy. For worse or for better, I had become the figurehead of the "Stop The Freeze" movement, and so I became a public figure. At times I almost felt more like a politician than an academic. I barely had the energy to keep it up. It was the same, to varying extents, with the other team members, in particular with Li-Zhen, who was the face of the project in Northern America just as I was in Northern Europe, but she being an extrovert was just naturally better at this kind of thing.
<br><br>
_World, Spring 2030_

By the time the final paper was published, this time in _Nature_ proper, it was the spring of 2030. The world was now a very different place from 2024. The effects of climate change were more pronounced everywhere. The Royal Navy had announced that they would have to abandon the base at Faslane by 2035 because of the increased flooding risk. Partially thanks to their role in our work, the views of the Navy on climate change were now taken very seriously. Politicians wanted at least to be seen to be concerned about strategic issues, and now climate change was one of them, and therefore no longer easily ignored.
Also, the historical tide was strongly on our side. A green wave had swept the northern hemisphere, and the time for prevarication seemed to have ended for good.
Slowly, demands on my time grew less. I took a sabbatical of a year, and did a once-a-lifetime long trip, going back to  Indonesia and visiting Malaysia, Australia and (a small sin) Japan. After that, my life returned to its old routines, and for a few years I was quite content in Scotland.
<br><br>

_Stockholm, March 2035_

But in the end, a quiet but insistent voice inside me called me back to Stockholm. It's hard for me to imagine now that less than half a year ago I was still enjoying the Scottish autumn in Helensburgh. And yet, I left part of my heart there. While walking back along the pier, with the dark waters of Saltsjön beside me, for a moment I am back there on the walkway boarding the Gareloch, with the autumn sun setting behind the hills. Another Sigrid will always be living there for as long as I am here. The sea joins us: a current will carry parts of her old life up here, another one will take fragments of my new life down there.
